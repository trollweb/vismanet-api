# LedgerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internal_id** | **int** |  | [optional] 
**number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**balance_type** | **string** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**consolidation_source** | **bool** |  | [optional] 
**consol_branch** | [**\Trollweb\VismaNetApi\Model\ConsolBranchDto**](ConsolBranchDto.md) |  | [optional] 
**branch_accounting** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


