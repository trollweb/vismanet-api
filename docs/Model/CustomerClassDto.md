# CustomerClassDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**tax_zone_id** | **string** |  | [optional] 
**required_taxzone_id** | **bool** |  | [optional] 
**payment_method_id** | **string** |  | [optional] 
**attributes** | [**\Trollweb\VismaNetApi\Model\ClassAttributeDto[]**](ClassAttributeDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


