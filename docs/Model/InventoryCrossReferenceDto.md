# InventoryCrossReferenceDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alternate_type** | **string** |  | [optional] 
**b_account** | [**\Trollweb\VismaNetApi\Model\BAccountDescriptionDto**](BAccountDescriptionDto.md) |  | [optional] 
**alternate_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


