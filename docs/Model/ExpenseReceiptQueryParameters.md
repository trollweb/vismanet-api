# ExpenseReceiptQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **string** |  | [optional] 
**date_condition** | **string** |  | [optional] 
**inventory** | **string** |  | [optional] 
**project** | **string** |  | [optional] 
**claimed_by** | **string** |  | [optional] 
**project_task** | **string** |  | [optional] 
**invoiceable** | **bool** |  | [optional] 
**status** | **string** |  | [optional] 
**customer** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


