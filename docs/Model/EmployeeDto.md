# EmployeeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employee_id** | **int** |  | [optional] 
**employee_number** | **string** |  | [optional] 
**employee_name** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**department** | **string** |  | [optional] 
**contact** | [**\Trollweb\VismaNetApi\Model\EmployeeContactDto**](EmployeeContactDto.md) |  | [optional] 
**address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


