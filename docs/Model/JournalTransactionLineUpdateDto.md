# JournalTransactionLineUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** |  | [optional] 
**line_number** | [**\Trollweb\VismaNetApi\Model\DtoValueInt32**](DtoValueInt32.md) |  | [optional] 
**account_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SegmentUpdateDto[]**](SegmentUpdateDto.md) |  | [optional] 
**reference_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**transaction_description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**debit_amount_in_currency** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**credit_amount_in_currency** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**vat_code_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**vat_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


