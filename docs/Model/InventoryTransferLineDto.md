# InventoryTransferLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to_location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**line_number** | **int** |  | [optional] 
**inventory_item** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**quantity** | **double** |  | [optional] 
**uom** | **string** |  | [optional] 
**reason_code** | [**\Trollweb\VismaNetApi\Model\ReasonCodeDto**](ReasonCodeDto.md) |  | [optional] 
**description** | **string** |  | [optional] 
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


