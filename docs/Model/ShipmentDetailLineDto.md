# ShipmentDetailLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | [optional] 
**order_type** | **string** |  | [optional] 
**order_nbr** | **string** |  | [optional] 
**inventory_number** | **string** |  | [optional] 
**free_item** | **bool** |  | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**uom** | **string** |  | [optional] 
**shipped_qty** | **double** |  | [optional] 
**ordered_qty** | **double** |  | [optional] 
**open_qty** | **double** |  | [optional] 
**lot_serial_nbr** | **string** |  | [optional] 
**expiration_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**reason_code** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**allocations** | [**\Trollweb\VismaNetApi\Model\AllocationsDto[]**](AllocationsDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


