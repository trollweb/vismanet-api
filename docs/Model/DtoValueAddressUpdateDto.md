# DtoValueAddressUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**\Trollweb\VismaNetApi\Model\AddressUpdateDto**](AddressUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


