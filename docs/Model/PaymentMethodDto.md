# PaymentMethodDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_method_id** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**means_of_payment** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**use_in_ap** | **bool** |  | [optional] 
**details** | [**\Trollweb\VismaNetApi\Model\PaymentMethodDetailDto[]**](PaymentMethodDetailDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


