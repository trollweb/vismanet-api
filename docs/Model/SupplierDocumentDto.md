# SupplierDocumentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**amount** | **double** |  | [optional] 
**amount_in_currency** | **double** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | Branch is deprecated, please use BranchNumber instead. | [optional] 
**supplier** | [**\Trollweb\VismaNetApi\Model\SupplierNumberDto**](SupplierNumberDto.md) |  | [optional] 
**document_type** | **string** |  | [optional] 
**reference_number** | **string** |  | [optional] 
**post_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format MMYYYY. | [optional] 
**financial_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format YYYYMM. | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**approval_status** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**balance** | **double** |  | [optional] 
**balance_in_currency** | **double** |  | [optional] 
**cash_discount** | **double** |  | [optional] 
**cash_discount_in_currency** | **double** |  | [optional] 
**payment_method** | [**\Trollweb\VismaNetApi\Model\PaymentMethodIdDescriptionDto**](PaymentMethodIdDescriptionDto.md) |  | [optional] 
**supplier_reference** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**created_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**note** | **string** |  | [optional] 
**closed_financial_period** | **string** | Format YYYYMM. | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**vat_total** | **double** |  | [optional] 
**vat_total_in_currency** | **double** |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


