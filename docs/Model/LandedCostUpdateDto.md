# LandedCostUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** |  | [optional] 
**line_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueInt32**](DtoValueInt32.md) |  | [optional] 
**landed_cost_code** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**purchase_invoice_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**supplier_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**ap_bill_date** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) |  | [optional] 
**currency** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**amount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**terms** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**inventory_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**ap_doc_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableSupplierDocumentType**](DtoValueNullableSupplierDocumentType.md) |  | [optional] 
**ap_ref_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**in_doc_type** | [**\Trollweb\VismaNetApi\Model\DtoValueCustomerDocumentTypes**](DtoValueCustomerDocumentTypes.md) |  | [optional] 
**in_ref_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**postpone_purchase_invoice_creation** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


