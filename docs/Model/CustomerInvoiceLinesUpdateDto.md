# CustomerInvoiceLinesUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**discount_code** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**domestic_services_deductible** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**item_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableRutRotItemTypes**](DtoValueNullableRutRotItemTypes.md) |  | [optional] 
**type_of_work** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**operation** | **string** |  | [optional] 
**inventory_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**line_number** | [**\Trollweb\VismaNetApi\Model\DtoValueInt32**](DtoValueInt32.md) |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**quantity** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**unit_price_in_currency** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**manual_amount_in_currency** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**account_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**vat_code_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**uom** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**discount_percent** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**discount_amount_in_currency** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**manual_discount** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SegmentUpdateDto[]**](SegmentUpdateDto.md) |  | [optional] 
**salesperson** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**deferral_schedule** | [**\Trollweb\VismaNetApi\Model\DtoValueInt32**](DtoValueInt32.md) |  | [optional] 
**deferral_code** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**term_start_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**term_end_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**note** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


