# CashTransactionUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**tran_date** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) |  | [optional] 
**finanacial_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format YYYYMM. | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**cash_account** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**entry_type** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**document_ref** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**control_total** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**vat_amount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**vat_zone** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**tax_calculation_mode** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableTaxCalcMode**](DtoValueNullableTaxCalcMode.md) |  | [optional] 
**cash_transaction_details** | [**\Trollweb\VismaNetApi\Model\CashTransactionDetailUpdateDto[]**](CashTransactionDetailUpdateDto.md) |  | [optional] 
**cash_transaction_tax_details** | [**\Trollweb\VismaNetApi\Model\CashTransactionTaxDetailUpdateDto[]**](CashTransactionTaxDetailUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


