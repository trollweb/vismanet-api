# JournalTransactionLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | [optional] 
**account_number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**reference_number** | **string** |  | [optional] 
**debit_amount** | **double** |  | [optional] 
**debit_amount_in_currency** | **double** |  | [optional] 
**credit_amount** | **double** |  | [optional] 
**credit_amount_in_currency** | **double** |  | [optional] 
**transaction_description** | **string** |  | [optional] 
**vat_code** | [**\Trollweb\VismaNetApi\Model\VatCodeDto**](VatCodeDto.md) |  | [optional] 
**vat** | [**\Trollweb\VismaNetApi\Model\VatDto**](VatDto.md) |  | [optional] 
**branch** | **string** |  | [optional] 
**customer_supplier** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


