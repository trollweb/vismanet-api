# InventorySummaryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationIdNameDto**](LocationIdNameDto.md) |  | [optional] 
**available** | **double** |  | [optional] 
**available_for_shipment** | **double** |  | [optional] 
**not_available** | **double** |  | [optional] 
**so_booked** | **double** |  | [optional] 
**so_allocated** | **double** |  | [optional] 
**so_shipped** | **double** |  | [optional] 
**so_back_ordered** | **double** |  | [optional] 
**in_issues** | **double** |  | [optional] 
**in_receipts** | **double** |  | [optional] 
**in_transit** | **double** |  | [optional] 
**in_assembly_demand** | **double** |  | [optional] 
**in_assembly_supply** | **double** |  | [optional] 
**purchase_prepared** | **double** |  | [optional] 
**purchase_orders** | **double** |  | [optional] 
**po_receipts** | **double** |  | [optional] 
**expired** | **double** |  | [optional] 
**on_hand** | **double** |  | [optional] 
**so_to_purchase** | **double** |  | [optional] 
**purchase_for_so** | **double** |  | [optional] 
**purchase_for_so_prepared** | **double** |  | [optional] 
**purchase_for_so_receipts** | **double** |  | [optional] 
**so_to_drop_ship** | **double** |  | [optional] 
**drop_ship_for_so** | **double** |  | [optional] 
**drop_ship_for_so_prepared** | **double** |  | [optional] 
**drop_ship_for_so_receipts** | **double** |  | [optional] 
**base_unit** | **string** |  | [optional] 
**estimated_unit_cost** | **double** |  | [optional] 
**estimated_total_cost** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


