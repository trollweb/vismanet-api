# PackagingTypeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**box_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**box_weight** | **double** |  | [optional] 
**max_weight** | **double** |  | [optional] 
**weight_uo_m** | **string** |  | [optional] 
**max_volume** | **double** |  | [optional] 
**volume_uo_m** | **string** |  | [optional] 
**length** | **int** |  | [optional] 
**width** | **int** |  | [optional] 
**height** | **int** |  | [optional] 
**active_by_default** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


