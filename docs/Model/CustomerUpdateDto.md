# CustomerUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**name** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**status** | [**\Trollweb\VismaNetApi\Model\DtoValueCustomerStatus**](DtoValueCustomerStatus.md) |  | [optional] 
**account_reference** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**parent_record_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**credit_limit** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**credit_days_past_due** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt16**](DtoValueNullableInt16.md) |  | [optional] 
**override_with_class_values** | **bool** |  | [optional] 
**customer_class_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**credit_terms_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**print_invoices** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**accept_auto_invoices** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**send_invoices_by_email** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**print_statements** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**send_statements_by_email** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**print_multi_currency_statements** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**vat_registration_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**corporate_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**vat_zone_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**note** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**main_address** | [**\Trollweb\VismaNetApi\Model\DtoValueAddressUpdateDto**](DtoValueAddressUpdateDto.md) |  | [optional] 
**main_contact** | [**\Trollweb\VismaNetApi\Model\DtoValueContactInfoUpdateDto**](DtoValueContactInfoUpdateDto.md) |  | [optional] 
**credit_verification** | [**\Trollweb\VismaNetApi\Model\DtoValueCreditRule**](DtoValueCreditRule.md) |  | [optional] 
**invoice_address** | [**\Trollweb\VismaNetApi\Model\DtoValueAddressUpdateDto**](DtoValueAddressUpdateDto.md) |  | [optional] 
**invoice_contact** | [**\Trollweb\VismaNetApi\Model\DtoValueContactInfoUpdateDto**](DtoValueContactInfoUpdateDto.md) |  | [optional] 
**statement_type** | [**\Trollweb\VismaNetApi\Model\DtoValueStatementTypes**](DtoValueStatementTypes.md) |  | [optional] 
**delivery_address** | [**\Trollweb\VismaNetApi\Model\DtoValueAddressUpdateDto**](DtoValueAddressUpdateDto.md) |  | [optional] 
**delivery_contact** | [**\Trollweb\VismaNetApi\Model\DtoValueContactInfoUpdateDto**](DtoValueContactInfoUpdateDto.md) |  | [optional] 
**price_class_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**direct_debit_lines** | [**\Trollweb\VismaNetApi\Model\CustomerDirectDebitUpdateDto[]**](CustomerDirectDebitUpdateDto.md) | Update direct debit information for a customer(only for Netherlands) | [optional] 
**attribute_lines** | [**\Trollweb\VismaNetApi\Model\AttributeLineUpdateDto[]**](AttributeLineUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


