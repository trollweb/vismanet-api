# DepartmentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**department_id** | **string** |  | [optional] 
**public_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**expense_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**expense_subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


