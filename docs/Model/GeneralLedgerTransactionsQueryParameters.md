# GeneralLedgerTransactionsQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branch** | **string** |  | [optional] 
**ledger** | **string** | Mandatory | [optional] 
**from_period** | **string** | Mandatory | [optional] 
**to_period** | **string** | Mandatory | [optional] 
**account** | **string** |  | [optional] 
**subaccount_id** | **string** |  | [optional] 
**from_date** | **string** |  | [optional] 
**to_date** | **string** |  | [optional] 
**include_unposted** | **bool** |  | [optional] 
**include_unreleased** | **bool** |  | [optional] 
**skip_records** | **int** |  | [optional] 
**number_to_read** | **int** |  | [optional] 
**last_modified_date_time** | **string** |  | [optional] 
**last_modified_date_time_condition** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


