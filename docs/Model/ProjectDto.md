# ProjectDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internal_id** | **int** |  | [optional] 
**project_id** | **string** |  | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerNumberDto**](CustomerNumberDto.md) |  | [optional] 
**hold** | **bool** |  | [optional] 
**status** | **string** |  | [optional] 
**template** | [**\Trollweb\VismaNetApi\Model\TemplateNumberDescriptionDto**](TemplateNumberDescriptionDto.md) |  | [optional] 
**description** | **string** |  | [optional] 
**assets** | **double** |  | [optional] 
**liability** | **double** |  | [optional] 
**income** | **double** |  | [optional] 
**expenses** | **double** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**project_manager** | [**\Trollweb\VismaNetApi\Model\EmployeeDto**](EmployeeDto.md) |  | [optional] 
**restrict_employees** | **bool** |  | [optional] 
**restrict_equipment** | **bool** |  | [optional] 
**visibility** | [**\Trollweb\VismaNetApi\Model\VisibilityDto**](VisibilityDto.md) |  | [optional] 
**def_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**def_sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**def_accrual_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**def_accrual_sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**billing_period** | **string** |  | [optional] 
**next_billing_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_billing_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**customer_location** | [**\Trollweb\VismaNetApi\Model\LocationIdNameDto**](LocationIdNameDto.md) |  | [optional] 
**allocation_rule** | [**\Trollweb\VismaNetApi\Model\AllocationRuleIdDescriptionDto**](AllocationRuleIdDescriptionDto.md) |  | [optional] 
**billing_rule** | [**\Trollweb\VismaNetApi\Model\BillingRuleIdDescriptionDto**](BillingRuleIdDescriptionDto.md) |  | [optional] 
**rate_table** | [**\Trollweb\VismaNetApi\Model\RateTableIdDescriptionDto**](RateTableIdDescriptionDto.md) |  | [optional] 
**auto_allocate** | **bool** |  | [optional] 
**automatic_release_ar** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**tasks** | [**\Trollweb\VismaNetApi\Model\TaskDto[]**](TaskDto.md) |  | [optional] 
**employees** | [**\Trollweb\VismaNetApi\Model\EmployeeDto[]**](EmployeeDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


