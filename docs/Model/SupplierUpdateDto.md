# SupplierUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**name** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**status** | [**\Trollweb\VismaNetApi\Model\DtoValueSupplierStatus**](DtoValueSupplierStatus.md) |  | [optional] 
**account_reference** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**parent_record_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**supplier_class_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**override_with_class_values** | **bool** |  | [optional] 
**credit_terms_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**document_language** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**payment_method_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**cash_account** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**payment_lead_time** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt16**](DtoValueNullableInt16.md) |  | [optional] 
**payment_ref_display_mask** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**pay_separately** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**vat_registration_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**corporate_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**vat_zone_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**charge_bearer** | [**\Trollweb\VismaNetApi\Model\DtoValueSupplierChargeBearer**](DtoValueSupplierChargeBearer.md) |  | [optional] 
**account_used_for_payment** | [**\Trollweb\VismaNetApi\Model\DtoValueAccountUsedForPayment**](DtoValueAccountUsedForPayment.md) |  | [optional] 
**payment_by** | [**\Trollweb\VismaNetApi\Model\DtoValuePaymentBy**](DtoValuePaymentBy.md) |  | [optional] 
**main_address** | [**\Trollweb\VismaNetApi\Model\DtoValueAddressUpdateDto**](DtoValueAddressUpdateDto.md) |  | [optional] 
**main_contact** | [**\Trollweb\VismaNetApi\Model\DtoValueContactInfoUpdateDto**](DtoValueContactInfoUpdateDto.md) |  | [optional] 
**remit_address** | [**\Trollweb\VismaNetApi\Model\DtoValueAddressUpdateDto**](DtoValueAddressUpdateDto.md) |  | [optional] 
**remit_contact** | [**\Trollweb\VismaNetApi\Model\DtoValueContactInfoUpdateDto**](DtoValueContactInfoUpdateDto.md) |  | [optional] 
**supplier_address** | [**\Trollweb\VismaNetApi\Model\DtoValueAddressUpdateDto**](DtoValueAddressUpdateDto.md) |  | [optional] 
**supplier_contact** | [**\Trollweb\VismaNetApi\Model\DtoValueContactInfoUpdateDto**](DtoValueContactInfoUpdateDto.md) |  | [optional] 
**supplier_payment_method_details** | [**\Trollweb\VismaNetApi\Model\SupplierPaymentMethodDetailUpdateDto[]**](SupplierPaymentMethodDetailUpdateDto.md) |  | [optional] 
**attribute_lines** | [**\Trollweb\VismaNetApi\Model\AttributeLineUpdateDto[]**](AttributeLineUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


