# AccountDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_id** | **int** |  | [optional] 
**account_cd** | **string** |  | [optional] 
**account_class** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**use_default_sub** | **bool** |  | [optional] 
**post_option** | **string** |  | [optional] 
**currency** | **string** |  | [optional] 
**tax_category** | **string** |  | [optional] 
**cash_account** | **bool** |  | [optional] 
**public_code1** | **int** |  | [optional] 
**external_code1** | **string** |  | [optional] 
**external_code2** | **string** |  | [optional] 
**analysis_code** | **string** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


