# SalesOrderBasicDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lines** | [**\Trollweb\VismaNetApi\Model\SalesOrderDocumentLineDto[]**](SalesOrderDocumentLineDto.md) |  | [optional] 
**order_type** | **string** |  | [optional] 
**order_no** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**request_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**customer_order** | **string** |  | [optional] 
**customer_ref_no** | **string** |  | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerDescriptionDto**](CustomerDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**currency** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**order_total** | **double** |  | [optional] 
**vat_taxable_total** | **double** |  | [optional] 
**vat_exempt_total** | **double** |  | [optional] 
**tax_total** | **double** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) | The data containing information about the document attachments | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


