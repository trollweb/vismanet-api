# ExpenseClaimDetailDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**claim_detail_id** | **int** | Identifies the expense claim detail id, necessary when updatind detail information | [optional] 
**line_id** | **string** | The expense claim line id | [optional] 
**date** | [**\DateTime**](\DateTime.md) | The date when the expense was incurred. | [optional] 
**expense_item** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) | The expense identifier, if an expense of this type is listed as an inventory nonstock item. | [optional] 
**description** | **string** | A description of the transaction. | [optional] 
**quantity** | **double** | The quantity of this expense item. | [optional] 
**uom** | **string** | The unit of measure in which the quantity is shown. | [optional] 
**unit_cost** | **double** | The cost of a unit of the item. | [optional] 
**currency** | **string** | The currency of the expense receipt. However, if you enter a claim line directly, the currency value is read-only and matching the claim currency. | [optional] 
**total_amount** | **double** | The total amount paid for the expense item in the specified quantity. | [optional] 
**invoiceable** | **bool** | A check box that, if selected, indicates that the claim amount is invoiceable to the customer (the total amount minus the employee&#39;s part). | [optional] 
**claim_amount** | **double** | The amount claimed by the employee, which is calculated as the total claim amount minus the employee part. | [optional] 
**amount_in_claim_curr** | **double** | The amount claimed by the employee, which is expressed in the currency of the expense claim. | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) | The project or customer contract associated with the expense claim, if the work performed was for a project or contract. Project Task The task associated with the contract or project. | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) | The task associated with the contract or project. | [optional] 
**expense_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The expense account to which the system should record the part of the expense to be paid back to the employee. | [optional] 
**expense_subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) | The corresponding subaccount the system uses to record the part of the expense to be paid back to the employee. | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | The branch that will reimburse the expenses to the employee. | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto**](TaxCategoryNumberDescriptionDto.md) | The tax category associated with the expense item. | [optional] 
**ref_nbr** | **string** | The identifier of the transaction. | [optional] 
**sales_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The sales account to which the system should record the part of the amount to charge the customer for. This is applicable only when a customer has been specified. | [optional] 
**sales_subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) | The corresponding subaccount the system uses to record the amount to charge the customer for. This is applicable only when a customer has been specified. | [optional] 
**employee_part** | **double** | The part of the total amount that will not be paid back to the employee. The percentage depends on the company policy. | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerNumberDto**](CustomerNumberDto.md) | The identifier of the customer associated with the expense. | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) | The identifier of the customer location associated with the expense. | [optional] 
**ar_reference_nbr** | **string** | The reference number of the AR document. | [optional] 
**approval_status** | **string** | The approval status, which indicates whether the detail row requires approval and, if it does, what the current state of approval is. | [optional] 
**approval_status_text** | **string** | The approval status text suitable for display | [optional] 
**approver** | **string** | The identifier of the person authorized to approve the activity, if approval is required. This is either the approver of the project task or, if no approver is assigned to the project task, the project manager. | [optional] 
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) | Expense claim detail line attachtments | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


