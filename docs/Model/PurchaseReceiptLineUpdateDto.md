# PurchaseReceiptLineUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocations** | [**\Trollweb\VismaNetApi\Model\AllocationsUpdateDto[]**](AllocationsUpdateDto.md) |  | [optional] 
**operation** | **string** |  | [optional] 
**line_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) |  | [optional] 
**line_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullablePoLineType**](DtoValueNullablePoLineType.md) |  | [optional] 
**branch_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | BranchId is deprecated, please use BranchNumber instead. | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**inventory_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**warehouse_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**transaction_description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**uom** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**receipt_qty** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**unit_cost** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**ext_cost** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**discount_percent** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**discount_amount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**manual_discount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**discount_code_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**amount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**tax_category_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**account_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**account_description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SegmentUpdateDto[]**](SegmentUpdateDto.md) |  | [optional] 
**actual_account_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**actual_subaccount** | [**\Trollweb\VismaNetApi\Model\SegmentUpdateDto[]**](SegmentUpdateDto.md) |  | [optional] 
**project_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**project_task_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**expiration_date** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) |  | [optional] 
**lot_serial_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**po_order_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullablePurchaseOrderType**](DtoValueNullablePurchaseOrderType.md) | Deprecated, property will be replaced by an action | [optional] 
**po_order_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | Deprecated, property will be replaced by an action | [optional] 
**po_order_line_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | Deprecated, property will be replaced by an action | [optional] 
**transfer_order_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableSalesOrderTypes**](DtoValueNullableSalesOrderTypes.md) |  | [optional] 
**transfer_order_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**transfer_order_line_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) |  | [optional] 
**complete_po_line** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


