# DtoDimension

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**length** | **int** |  | [optional] 
**description** | **string** |  | [optional] 
**segments** | [**\Trollweb\VismaNetApi\Model\DtoSegment[]**](DtoSegment.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


