# LocationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country_id** | **string** |  | [optional] 
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


