# GeneralLedgerBalanceQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**greater_than_value** | **string** |  | [optional] 
**number_to_read** | **int** |  | [optional] 
**skip_records** | **int** |  | [optional] 
**period_id** | **string** |  | [optional] 
**period_id_condition** | **string** |  | [optional] 
**order_by** | **string** |  | [optional] 
**last_modified_date_time** | **string** |  | [optional] 
**last_modified_date_time_condition** | **string** |  | [optional] 
**account_id** | **string** |  | [optional] 
**year_closed** | **int** |  | [optional] 
**balance_type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


