# ContactDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**title** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**position** | **string** |  | [optional] 
**business_account** | **string** |  | [optional] 
**same_as_account** | **bool** |  | [optional] 
**address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**email** | **string** |  | [optional] 
**web** | **string** |  | [optional] 
**phone1** | **string** |  | [optional] 
**phone2** | **string** |  | [optional] 
**phone3** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**contact_method** | **string** |  | [optional] 
**do_not_call** | **bool** |  | [optional] 
**do_not_fax** | **bool** |  | [optional] 
**do_not_email** | **bool** |  | [optional] 
**do_not_mail** | **bool** |  | [optional] 
**no_mass_mail** | **bool** |  | [optional] 
**no_marketing** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**contact_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


