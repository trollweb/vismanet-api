# TimeCardSummaryUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** |  | [optional] 
**line_number** | [**\Trollweb\VismaNetApi\Model\DtoValueInt32**](DtoValueInt32.md) |  | [optional] 
**earning_type** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The type of the work time spent by the employee | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The project that the employee worked on. | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The project task that the employee worked on. | [optional] 
**mon** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Monday, including overtime. | [optional] 
**tue** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Tuesday, including overtime. | [optional] 
**wed** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Wednesday, including overtime. | [optional] 
**thu** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Thursday, including overtime. | [optional] 
**fri** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Friday, including overtime. | [optional] 
**sat** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Saturday, including overtime. | [optional] 
**sun** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) | The work time reported for Sunday, including overtime. | [optional] 
**invoiceable** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) | A check box that you select to indicate that these work hours are invoiceable. | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The description of the reported work hours. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


