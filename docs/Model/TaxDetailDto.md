# TaxDetailDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**record_id** | **int** |  | [optional] 
**vat_id** | [**\Trollweb\VismaNetApi\Model\TaxNumberDescriptionDto**](TaxNumberDescriptionDto.md) |  | [optional] 
**vat_rate** | **double** |  | [optional] 
**taxable_amount** | **double** |  | [optional] 
**vat_amount** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


