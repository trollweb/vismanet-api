# CustomerQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**greater_than_value** | **string** |  | [optional] 
**number_to_read** | **int** |  | [optional] 
**skip_records** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**corporate_id** | **string** |  | [optional] 
**vat_registration_id** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**last_modified_date_time** | **string** |  | [optional] 
**last_modified_date_time_condition** | **string** |  | [optional] 
**created_date_time** | **string** |  | [optional] 
**created_date_time_condition** | **string** |  | [optional] 
**attributes** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


