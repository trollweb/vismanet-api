# SegmentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**segment_id** | **int** |  | [optional] 
**segment_description** | **string** |  | [optional] 
**segment_value** | **string** |  | [optional] 
**segment_value_description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


