# DtoValueSalesOrderAddressUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**\Trollweb\VismaNetApi\Model\SalesOrderAddressUpdateDto**](SalesOrderAddressUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


