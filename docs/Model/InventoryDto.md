# InventoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inventory_id** | **int** | The unique internal identifier of the  item as is stored in the database | [optional] 
**inventory_number** | **string** | The unique alphanumeric identifier of the  item that is used in UI | [optional] 
**status** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**item_class** | [**\Trollweb\VismaNetApi\Model\ItemClassDto**](ItemClassDto.md) | The item class to which the item is assigned. Selecting a class provides default settings for the item. | [optional] 
**posting_class** | [**\Trollweb\VismaNetApi\Model\PostingClassDto**](PostingClassDto.md) | The posting class of the item, which by default is the posting class assigned to the item class. | [optional] 
**vat_code** | [**\Trollweb\VismaNetApi\Model\VatCodeDto**](VatCodeDto.md) | The vat category of the item, which by default is the vat category associated with the item class. | [optional] 
**default_price** | **double** | The price of the item. This price is used as the default price, if there are no other prices | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**cross_references** | [**\Trollweb\VismaNetApi\Model\InventoryCrossReferenceDto[]**](InventoryCrossReferenceDto.md) |  | [optional] 
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) |  | [optional] 
**attributes** | [**\Trollweb\VismaNetApi\Model\AttributeIdValueDto[]**](AttributeIdValueDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


