# ClassAttributeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**required** | **bool** |  | [optional] 
**attribute_type** | **string** |  | [optional] 
**default_value** | **string** |  | [optional] 
**details** | [**\Trollweb\VismaNetApi\Model\ClassAttributeDetailDto[]**](ClassAttributeDetailDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


