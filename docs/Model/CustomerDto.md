# CustomerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internal_id** | **int** |  | [optional] 
**number** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**main_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**main_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**account_reference** | **string** |  | [optional] 
**parent_record** | [**\Trollweb\VismaNetApi\Model\ParentRecordDto**](ParentRecordDto.md) |  | [optional] 
**customer_class** | [**\Trollweb\VismaNetApi\Model\ClassDescriptionDto**](ClassDescriptionDto.md) |  | [optional] 
**credit_terms** | [**\Trollweb\VismaNetApi\Model\CreditTermsDto**](CreditTermsDto.md) |  | [optional] 
**currency_id** | **string** |  | [optional] 
**credit_verification** | **string** |  | [optional] 
**credit_limit** | **double** |  | [optional] 
**credit_days_past_due** | **int** |  | [optional] 
**invoice_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**invoice_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**print_invoices** | **bool** |  | [optional] 
**accept_auto_invoices** | **bool** |  | [optional] 
**send_invoices_by_email** | **bool** |  | [optional] 
**print_statements** | **bool** |  | [optional] 
**send_statements_by_email** | **bool** |  | [optional] 
**print_multi_currency_statements** | **bool** |  | [optional] 
**statement_type** | **string** |  | [optional] 
**delivery_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**delivery_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**vat_registration_id** | **string** |  | [optional] 
**corporate_id** | **string** |  | [optional] 
**vat_zone** | [**\Trollweb\VismaNetApi\Model\VatZoneDto**](VatZoneDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**attributes** | [**\Trollweb\VismaNetApi\Model\AttributeIdValueDto[]**](AttributeIdValueDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**direct_debit_lines** | [**\Trollweb\VismaNetApi\Model\CustomerDirectDebitDto[]**](CustomerDirectDebitDto.md) |  | [optional] 
**price_class** | [**\Trollweb\VismaNetApi\Model\PriceClassDto**](PriceClassDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


