# WarehouseLocationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**incl_qty_avail** | **bool** |  | [optional] 
**is_costed** | **bool** |  | [optional] 
**sales_valid** | **bool** |  | [optional] 
**receipts_valid** | **bool** |  | [optional] 
**transfers_valid** | **bool** |  | [optional] 
**assembly_valid** | **bool** |  | [optional] 
**primary_item_valid** | **string** |  | [optional] 
**primary_item** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**primary_item_class** | [**\Trollweb\VismaNetApi\Model\ItemClassDto**](ItemClassDto.md) |  | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) |  | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


