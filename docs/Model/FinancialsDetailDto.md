# FinancialsDetailDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batch_number** | **string** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**cleared** | **bool** |  | [optional] 
**clear_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**tax_zone** | [**\Trollweb\VismaNetApi\Model\VatZoneDto**](VatZoneDto.md) |  | [optional] 
**tax_calc_mode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


