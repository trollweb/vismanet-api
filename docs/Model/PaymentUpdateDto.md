# PaymentUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullablePaymentTypes**](DtoValueNullablePaymentTypes.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**application_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**application_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**payment_ref** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**payment_method** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**cash_account** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**payment_amount** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**invoice_text** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**orders_to_apply** | [**\Trollweb\VismaNetApi\Model\PaymentOrdersLinesUpdateDto[]**](PaymentOrdersLinesUpdateDto.md) |  | [optional] 
**payment_lines** | [**\Trollweb\VismaNetApi\Model\PaymentLinesUpdateDto[]**](PaymentLinesUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


