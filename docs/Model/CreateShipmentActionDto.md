# CreateShipmentActionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_type** | **string** |  | [optional] 
**return_shipment_dto** | **bool** |  | [optional] 
**shipment_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**shipment_warehouse** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


