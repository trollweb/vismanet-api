# GeneralLedgerTransactionDetailsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | [optional] 
**module** | **string** |  | [optional] 
**batch_number** | **string** |  | [optional] 
**tran_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**period** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**ref_number** | **string** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDescriptionDto**](AccountNumberDescriptionDto.md) |  | [optional] 
**ledger** | [**\Trollweb\VismaNetApi\Model\LedgerDescriptionDto**](LedgerDescriptionDto.md) |  | [optional] 
**subaccount** | **string** |  | [optional] 
**beg_balance** | **double** |  | [optional] 
**debit_amount** | **double** |  | [optional] 
**credit_amount** | **double** |  | [optional] 
**ending_balance** | **double** |  | [optional] 
**currency** | **string** |  | [optional] 
**curr_beg_balance** | **double** |  | [optional] 
**curr_debit_amount** | **double** |  | [optional] 
**curr_credit_amount** | **double** |  | [optional] 
**curr_ending_balance** | **double** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


