# CashTransactionQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number_to_read** | **int** |  | [optional] 
**skip_records** | **int** |  | [optional] 
**last_modified_date_time** | **string** |  | [optional] 
**last_modified_date_time_condition** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


