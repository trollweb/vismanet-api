# ContactInfoDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact_id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**attention** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**web** | **string** |  | [optional] 
**phone1** | **string** |  | [optional] 
**phone2** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


