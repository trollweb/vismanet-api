# NumberingDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numbering_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**manual_numbering** | **bool** |  | [optional] 
**new_number_symbol** | **string** |  | [optional] 
**sequence** | [**\Trollweb\VismaNetApi\Model\NumberingSequenceDto[]**](NumberingSequenceDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


