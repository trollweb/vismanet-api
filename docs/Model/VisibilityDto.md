# VisibilityDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visible_in_gl** | **bool** |  | [optional] 
**visible_in_ap** | **bool** |  | [optional] 
**visible_in_ar** | **bool** |  | [optional] 
**visible_in_so** | **bool** |  | [optional] 
**visible_in_po** | **bool** |  | [optional] 
**visible_in_ep** | **bool** |  | [optional] 
**visible_in_in** | **bool** |  | [optional] 
**visible_in_ca** | **bool** |  | [optional] 
**visible_in_cr** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


