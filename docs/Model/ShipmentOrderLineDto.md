# ShipmentOrderLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_type** | **string** |  | [optional] 
**order_nbr** | **string** |  | [optional] 
**shipped_qty** | **double** |  | [optional] 
**shipped_weight** | **double** |  | [optional] 
**shipped_volume** | **double** |  | [optional] 
**invoice_type** | **string** |  | [optional] 
**invoice_nbr** | **string** |  | [optional] 
**inventory_doc_type** | **string** |  | [optional] 
**inventory_ref_nbr** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


