# SalesOrderDocumentLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_nbr** | **int** |  | [optional] 
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**uom** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**qty_on_shipments** | **double** |  | [optional] 
**open_qty** | **double** |  | [optional] 
**unit_cost** | **double** |  | [optional] 
**unit_price** | **double** |  | [optional] 
**discount_code** | **string** |  | [optional] 
**discount_percent** | **double** |  | [optional] 
**discount_amount** | **double** |  | [optional] 
**manual_discount** | **bool** |  | [optional] 
**disc_unit_price** | **double** |  | [optional] 
**ext_price** | **double** |  | [optional] 
**unbilled_amount** | **double** |  | [optional] 
**line_description** | **string** |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) | The data containing information about the document attachments | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


