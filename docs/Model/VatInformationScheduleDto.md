# VatInformationScheduleDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**vat_rate** | **double** |  | [optional] 
**min_taxable_amt** | **double** |  | [optional] 
**max_taxable_amt** | **double** |  | [optional] 
**reporting_group** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


