# CashTransactionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tran_type** | **string** |  | [optional] 
**reference_nbr** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**tran_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**fin_period** | **string** |  | [optional] 
**cash_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**currency** | **string** |  | [optional] 
**entry_type** | [**\Trollweb\VismaNetApi\Model\EntryTypeIdDescriptionDto**](EntryTypeIdDescriptionDto.md) |  | [optional] 
**disb_receipt** | **string** |  | [optional] 
**document_ref** | **string** |  | [optional] 
**owner** | [**\Trollweb\VismaNetApi\Model\EmployeeNumberNameDto**](EmployeeNumberNameDto.md) |  | [optional] 
**description** | **string** |  | [optional] 
**amount** | **double** |  | [optional] 
**vat_taxable_total** | **double** |  | [optional] 
**vat_exempt_total** | **double** |  | [optional] 
**tax_total** | **double** |  | [optional] 
**control_total** | **double** |  | [optional] 
**tax_amount** | **double** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transaction_details** | [**\Trollweb\VismaNetApi\Model\TransactionDetailDto[]**](TransactionDetailDto.md) |  | [optional] 
**tax_details** | [**\Trollweb\VismaNetApi\Model\CashTranTaxDetailDto[]**](CashTranTaxDetailDto.md) |  | [optional] 
**financials_detail** | [**\Trollweb\VismaNetApi\Model\FinancialsDetailDto**](FinancialsDetailDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


