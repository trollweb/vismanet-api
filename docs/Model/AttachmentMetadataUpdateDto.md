# AttachmentMetadataUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**send_to_autoinvoice** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


