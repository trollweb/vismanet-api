# PackageDetailLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | [optional] 
**confirmed** | **bool** |  | [optional] 
**box_id** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**weight** | **double** |  | [optional] 
**uom** | **string** |  | [optional] 
**declared_value** | **double** |  | [optional] 
**co_d_amount** | **double** |  | [optional] 
**tracking_number** | **string** |  | [optional] 
**custom_ref_nbr1** | **string** |  | [optional] 
**custom_ref_nbr2** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


