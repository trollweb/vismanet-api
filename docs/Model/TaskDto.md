# TaskDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**task_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**planned_start** | [**\DateTime**](\DateTime.md) |  | [optional] 
**planned_end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**rate_table** | [**\Trollweb\VismaNetApi\Model\RateTableIdDescriptionDto**](RateTableIdDescriptionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


