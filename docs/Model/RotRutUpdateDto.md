# RotRutUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distributed_automaticaly** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableRutRotTypes**](DtoValueNullableRutRotTypes.md) |  | [optional] 
**appartment** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**estate** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**organization_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**distribution** | [**\Trollweb\VismaNetApi\Model\RotRutDistributionUpdateDto[]**](RotRutDistributionUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


