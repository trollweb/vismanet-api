# DtoSegmentValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**public_id** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**time_stamp** | **string** |  | [optional] 
**last_modified** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


