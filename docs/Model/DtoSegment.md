# DtoSegment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**segment_id** | **int** |  | [optional] 
**description** | **string** |  | [optional] 
**length** | **int** |  | [optional] 
**public_id** | **string** |  | [optional] 
**time_stamp** | **string** |  | [optional] 
**last_modified** | [**\DateTime**](\DateTime.md) |  | [optional] 
**validate** | **bool** |  | [optional] 
**segment_values** | [**\Trollweb\VismaNetApi\Model\DtoSegmentValue[]**](DtoSegmentValue.md) |  | [optional] 
**is_auto_number** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


