# StocktakeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_nbr** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**summary_status** | **string** |  | [optional] 
**freeze_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**number_of_lines** | **int** |  | [optional] 
**physical_qty** | **double** |  | [optional] 
**variance_qty** | **double** |  | [optional] 
**variance_cost** | **double** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**lines** | [**\Trollweb\VismaNetApi\Model\StocktakeLineDto[]**](StocktakeLineDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


