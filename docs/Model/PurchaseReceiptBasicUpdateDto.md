# PurchaseReceiptBasicUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lines** | [**\Trollweb\VismaNetApi\Model\PurchaseReceiptLineBasicUpdateDto[]**](PurchaseReceiptLineBasicUpdateDto.md) |  | [optional] 
**receipt_type** | [**\Trollweb\VismaNetApi\Model\DtoValuePurchaseReceiptType**](DtoValuePurchaseReceiptType.md) |  | [optional] 
**receipt_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**date** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) |  | [optional] 
**post_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format MMYYYY. | [optional] 
**warehouse_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**supplier_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**create_bill** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**supplier_ref** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**control_qty** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**control_amount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


