# LandedCostDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_nbr** | **int** |  | [optional] 
**landed_cost_code** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**purchase_invoice_nbr** | **string** |  | [optional] 
**supplier** | [**\Trollweb\VismaNetApi\Model\SupplierDescriptionDto**](SupplierDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**ap_bill_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**currency** | **string** |  | [optional] 
**amount** | **double** |  | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto**](TaxCategoryNumberDescriptionDto.md) |  | [optional] 
**terms** | [**\Trollweb\VismaNetApi\Model\CreditTermsDto**](CreditTermsDto.md) |  | [optional] 
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**ap_doc_type** | **string** |  | [optional] 
**ap_ref_nbr** | **string** |  | [optional] 
**in_doc_type** | **string** |  | [optional] 
**in_ref_nbr** | **string** |  | [optional] 
**postpone_purchase_invoice_creation** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


