# SupplierBalanceDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplier** | [**\Trollweb\VismaNetApi\Model\SupplierDescriptionDto**](SupplierDescriptionDto.md) |  | [optional] 
**balance** | **double** |  | [optional] 
**unreleased_purchases_not_in_approval** | [**\Trollweb\VismaNetApi\Model\WithoutWithVatDto**](WithoutWithVatDto.md) |  | [optional] 
**total_sent_for_approval** | [**\Trollweb\VismaNetApi\Model\WithoutWithVatDto**](WithoutWithVatDto.md) |  | [optional] 
**total_purchase_invoice_period** | [**\Trollweb\VismaNetApi\Model\WithoutWithVatDto**](WithoutWithVatDto.md) |  | [optional] 
**total_purchase_invoice_year** | [**\Trollweb\VismaNetApi\Model\WithoutWithVatDto**](WithoutWithVatDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


