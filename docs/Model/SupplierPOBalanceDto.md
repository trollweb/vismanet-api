# SupplierPOBalanceDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supplier** | [**\Trollweb\VismaNetApi\Model\SupplierDescriptionDto**](SupplierDescriptionDto.md) |  | [optional] 
**total_po_on_hold_order_total** | **double** |  | [optional] 
**total_po_on_hold_line_total** | **double** |  | [optional] 
**total_open_po_order_total** | **double** |  | [optional] 
**total_open_po_line_total** | **double** |  | [optional] 
**total_closed_po_order_total** | **double** |  | [optional] 
**total_closed_po_line_total** | **double** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


