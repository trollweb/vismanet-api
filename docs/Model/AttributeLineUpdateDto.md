# AttributeLineUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**attribute_value** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


