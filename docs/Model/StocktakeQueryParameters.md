# StocktakeQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warehouse** | **string** |  | [optional] 
**location** | **string** |  | [optional] 
**inventory** | **string** |  | [optional] 
**lot_serial_number** | **string** |  | [optional] 
**summary_status** | **string** |  | [optional] 
**number_to_read** | **int** |  | [optional] 
**start_with_line** | **int** |  | [optional] 
**end_with_line** | **int** |  | [optional] 
**freeze_date_time** | **string** |  | [optional] 
**freeze_date_time_condition** | **string** |  | [optional] 
**last_modified_date_time** | **string** |  | [optional] 
**last_modified_date_time_condition** | **string** |  | [optional] 
**expiration_date_time** | **string** |  | [optional] 
**expiration_date_time_condition** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**skip_records** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


