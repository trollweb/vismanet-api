# AccountNumberDescriptionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**external_code1** | **string** | ExternalCode1 is deprecated, please use ExternalCode1Info instead. | [optional] 
**external_code2** | **string** | ExternalCode2 is deprecated, please use ExternalCode2Info instead. | [optional] 
**external_code1_info** | [**\Trollweb\VismaNetApi\Model\ExternalCodeNumberDescriptionDto**](ExternalCodeNumberDescriptionDto.md) |  | [optional] 
**external_code2_info** | [**\Trollweb\VismaNetApi\Model\ExternalCodeNumberDescriptionDto**](ExternalCodeNumberDescriptionDto.md) |  | [optional] 
**number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


