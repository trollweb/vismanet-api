# PurchaseOrderLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_nbr** | **int** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**line_type** | **string** |  | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**line_description** | **string** |  | [optional] 
**uom** | **string** |  | [optional] 
**order_qty** | **double** |  | [optional] 
**qty_on_receipts** | **double** |  | [optional] 
**unit_cost** | **double** |  | [optional] 
**ext_cost** | **double** |  | [optional] 
**discount_percent** | **double** |  | [optional] 
**discount_amount** | **double** |  | [optional] 
**manual_discount** | **bool** |  | [optional] 
**discount_code** | [**\Trollweb\VismaNetApi\Model\DiscountCodeNumberDescriptionDto**](DiscountCodeNumberDescriptionDto.md) |  | [optional] 
**amount** | **double** |  | [optional] 
**received_amt** | **double** |  | [optional] 
**alternate_id** | **string** |  | [optional] 
**min_receipt** | **double** |  | [optional] 
**max_receipt** | **double** |  | [optional] 
**complete_on** | **double** |  | [optional] 
**receipt_action** | **string** |  | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto**](TaxCategoryNumberDescriptionDto.md) |  | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) |  | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) |  | [optional] 
**requested** | [**\DateTime**](\DateTime.md) |  | [optional] 
**promised** | [**\DateTime**](\DateTime.md) |  | [optional] 
**completed** | **bool** |  | [optional] 
**canceled** | **bool** |  | [optional] 
**order_type** | **string** |  | [optional] 
**order_number** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


