# NumberingSequenceDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numbering_id** | **string** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**start_nbr** | **string** |  | [optional] 
**end_nbr** | **string** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_nbr** | **string** |  | [optional] 
**warn_nbr** | **string** |  | [optional] 
**nbr_step** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


