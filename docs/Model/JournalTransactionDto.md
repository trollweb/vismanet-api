# JournalTransactionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**module** | **string** |  | [optional] 
**batch_number** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**transaction_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**post_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format MMYYYY. | [optional] 
**financial_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format YYYYMM. | [optional] 
**ledger** | **string** |  | [optional] 
**ledger_description** | **string** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**exchange_rate** | **double** |  | [optional] 
**auto_reversing** | **bool** |  | [optional] 
**reversing_entry** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**original_batch_number** | **string** |  | [optional] 
**debit_total** | **double** |  | [optional] 
**debit_total_in_currency** | **double** |  | [optional] 
**credit_total** | **double** |  | [optional] 
**credit_total_in_currency** | **double** |  | [optional] 
**control_total** | **double** |  | [optional] 
**control_total_in_currency** | **double** |  | [optional] 
**create_vat_transaction** | **bool** |  | [optional] 
**skip_vat_amount_validation** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**transaction_code** | **string** |  | [optional] 
**transaction_code_description** | **string** |  | [optional] 
**branch** | **string** |  | [optional] 
**journal_transaction_lines** | [**\Trollweb\VismaNetApi\Model\JournalTransactionLineDto[]**](JournalTransactionLineDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


