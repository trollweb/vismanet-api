# VatInformationDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_category_id** | **string** | VatCategoryId is deprecated, please use Vat Id instead | [optional] 
**vat_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**deductible_vat** | **bool** |  | [optional] 
**reverse_vat** | **bool** |  | [optional] 
**statistical_vat** | **bool** |  | [optional] 
**pending_vat** | **bool** |  | [optional] 
**includein_vat_exempt_total** | **bool** |  | [optional] 
**includein_vat_total** | **bool** |  | [optional] 
**enter_from_vat_invoice** | **bool** |  | [optional] 
**calculate_on** | **string** |  | [optional] 
**cash_discount** | **string** |  | [optional] 
**vat_agency_id** | [**\Trollweb\VismaNetApi\Model\SupplierNumberDto**](SupplierNumberDto.md) |  | [optional] 
**not_valid_after** | [**\DateTime**](\DateTime.md) |  | [optional] 
**eu_report_code** | **string** |  | [optional] 
**document_text** | **string** |  | [optional] 
**default_non_stock_item** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**visma_xml_vat_type** | **string** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**gl_accounts** | [**\Trollweb\VismaNetApi\Model\VatGlAccountsDto**](VatGlAccountsDto.md) |  | [optional] 
**schedules** | [**\Trollweb\VismaNetApi\Model\VatInformationScheduleDto[]**](VatInformationScheduleDto.md) |  | [optional] 
**categories** | [**\Trollweb\VismaNetApi\Model\VatCategoryDto[]**](VatCategoryDto.md) |  | [optional] 
**zones** | [**\Trollweb\VismaNetApi\Model\VatZoneDto[]**](VatZoneDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


