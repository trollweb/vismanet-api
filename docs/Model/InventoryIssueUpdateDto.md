# InventoryIssueUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**control_amount** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) | The manually entered summary amount for all specified inventory items. Control Amount is available only if the Validate Document Totals on Entry option is selected on the Inventory Preferences form. If the Control Amount and Total Amount values do not match, the system generates a warning message and the issue cannot be saved. | [optional] 
**issue_lines** | [**\Trollweb\VismaNetApi\Model\InventoryIssueLineUpdateDto[]**](InventoryIssueLineUpdateDto.md) | The inventory issue lines | [optional] 
**reference_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The unique reference number of the receipt, which the system automatically assigns according to the numbering sequence selected for receipts on the Inventory Preferences IN.10.10.00) form. | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) | A check box that you select to give the receipt the On Hold status. Clear the check box to save the receipt with the Balanced status. | [optional] 
**date** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) | The date when the receipt was created. All transactions included in this document will have this transaction date. | [optional] 
**post_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format MMYYYY. | [optional] 
**external_reference** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The external reference number of the inventory issue document (for example, the vendor’s reference code). | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | A brief description of the inventory issue or its transactions. | [optional] 
**control_quantity** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) | The manually entered quantity of inventory items. Control Qty. is available only if the Validate Document Totals on Entry option is selected on the Inventory Preferences form. If the Control Qty. and Total Qty.values do not match, the system generates a warning message and the issue cannot be saved. | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The Branch associated | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


