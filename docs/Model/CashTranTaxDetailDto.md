# CashTranTaxDetailDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax** | [**\Trollweb\VismaNetApi\Model\TaxNumberDescriptionDto**](TaxNumberDescriptionDto.md) |  | [optional] 
**tax_rate** | **double** |  | [optional] 
**taxable_amount** | **double** |  | [optional] 
**tax_amount** | **double** |  | [optional] 
**deductible_tax_rate** | **double** |  | [optional] 
**expense_amount** | **double** |  | [optional] 
**include_in_vat_exempt_total** | **bool** |  | [optional] 
**pending_vat** | **bool** |  | [optional] 
**statistical_vat** | **bool** |  | [optional] 
**reverse_vat** | **bool** |  | [optional] 
**tax_type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


