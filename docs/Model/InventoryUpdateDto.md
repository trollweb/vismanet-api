# InventoryUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inventory_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | Mandatory, The unique alphanumeric identifier of the  item that is used in UI. | [optional] 
**status** | [**\Trollweb\VismaNetApi\Model\DtoValueInventoryStatus**](DtoValueInventoryStatus.md) |  | [optional] 
**type** | [**\Trollweb\VismaNetApi\Model\DtoValueInventoryType**](DtoValueInventoryType.md) |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**item_class** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | Mandatory, the item class to which the item is assigned. Selecting a class provides default settings for the item. | [optional] 
**posting_class** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The posting class of the item, which by default is the posting class assigned to the item class. | [optional] 
**vat_code** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The vat category of the item, which by default is the vat category associated with the item class. | [optional] 
**default_price** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) | The price of the item. This price is used as the default price, if there are no other prices set. | [optional] 
**attribute_lines** | [**\Trollweb\VismaNetApi\Model\AttributeLineUpdateDto[]**](AttributeLineUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


