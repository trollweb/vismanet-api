# DiscountDetailDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_nbr** | **int** |  | [optional] 
**skip_discount** | **bool** |  | [optional] 
**discount_code** | **string** |  | [optional] 
**sequance_id** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**manual_discount** | **bool** |  | [optional] 
**discountable_amt** | **double** |  | [optional] 
**discountable_qty** | **double** |  | [optional] 
**discount_amt** | **double** |  | [optional] 
**discount** | **double** |  | [optional] 
**free_item** | **int** |  | [optional] 
**free_item_qty** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


