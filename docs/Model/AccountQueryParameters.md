# AccountQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**greater_than_value** | **string** |  | [optional] 
**public_code** | **int** |  | [optional] 
**external_code1** | **string** |  | [optional] 
**external_code2** | **string** |  | [optional] 
**analysis_code** | **string** |  | [optional] 
**number_to_read** | **int** |  | [optional] 
**skip_records** | **int** |  | [optional] 
**order_by** | **string** |  | [optional] 
**last_modified_date_time** | **string** |  | [optional] 
**last_modified_date_time_condition** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


