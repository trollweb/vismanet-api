# PaymentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**ref_nbr** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**application_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**application_period** | **string** |  | [optional] 
**payment_ref** | **string** |  | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerNumberDto**](CustomerNumberDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**payment_method** | [**\Trollweb\VismaNetApi\Model\PaymentMethodIdDescriptionDto**](PaymentMethodIdDescriptionDto.md) |  | [optional] 
**cash_account** | **string** |  | [optional] 
**currency** | **string** |  | [optional] 
**payment_amount** | **double** |  | [optional] 
**invoice_text** | **string** |  | [optional] 
**applied_to_documents** | **double** |  | [optional] 
**applied_to_orders** | **double** |  | [optional] 
**available_balance** | **double** |  | [optional] 
**write_off_amount** | **double** |  | [optional] 
**finance_charges** | **double** |  | [optional] 
**deducted_charges** | **double** |  | [optional] 
**branch** | **string** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**payment_lines** | [**\Trollweb\VismaNetApi\Model\PaymentLineDto[]**](PaymentLineDto.md) |  | [optional] 
**orders_to_apply** | [**\Trollweb\VismaNetApi\Model\OrderToApplyDto[]**](OrderToApplyDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


