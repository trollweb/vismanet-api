# PurchaseReceiptLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocations** | [**\Trollweb\VismaNetApi\Model\AllocationsDto[]**](AllocationsDto.md) |  | [optional] 
**line_nbr** | **int** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | Branch is deprecated, please use BranchNumber instead. | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**line_type** | **string** |  | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**transaction_description** | **string** |  | [optional] 
**uom** | **string** |  | [optional] 
**order_qty** | **double** |  | [optional] 
**open_qty** | **double** |  | [optional] 
**receipt_qty** | **double** |  | [optional] 
**unit_cost** | **double** |  | [optional] 
**ext_cost** | **double** |  | [optional] 
**discount_percent** | **double** |  | [optional] 
**discount_amount** | **double** |  | [optional] 
**manual_discount** | **bool** |  | [optional] 
**discount_code** | [**\Trollweb\VismaNetApi\Model\DiscountCodeNumberDescriptionDto**](DiscountCodeNumberDescriptionDto.md) |  | [optional] 
**amount** | **double** |  | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto**](TaxCategoryNumberDescriptionDto.md) |  | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**account_description** | **string** |  | [optional] 
**sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**actual_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**actual_sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) |  | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) |  | [optional] 
**expiration_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**lot_serial_number** | **string** |  | [optional] 
**po_order_type** | **string** |  | [optional] 
**po_order_nbr** | **string** |  | [optional] 
**po_order_line_nbr** | **int** |  | [optional] 
**transfer_order_type** | **string** |  | [optional] 
**transfer_order_nbr** | **string** |  | [optional] 
**transfer_order_line_nbr** | **int** |  | [optional] 
**complete_po_line** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


