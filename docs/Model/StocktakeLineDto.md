# StocktakeLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  | [optional] 
**line_nbr** | **int** |  | [optional] 
**tag_nbr** | **int** |  | [optional] 
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryIdNameDto**](InventoryIdNameDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationIdNameDto**](LocationIdNameDto.md) |  | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**lot_serial_nbr** | **string** |  | [optional] 
**expiration_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**book_quantity** | **double** |  | [optional] 
**physical_quantity** | **double** |  | [optional] 
**variance_quantity** | **double** |  | [optional] 
**unit_cost** | **double** |  | [optional] 
**ext_variance_cost** | **double** |  | [optional] 
**reason_code** | **string** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


