# FinancialPeriodAmountUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** |  | [optional] 
**period_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**amount** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


