# EntryTypeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entry_type_id** | **string** | The entry type, selected by its identifier. | [optional] 
**disable_receipt** | **string** | The basic type of cash transaction designated by this entry type: Receipt or Disbursement. | [optional] 
**module** | **string** | The way the entry type is used in the system. | [optional] 
**default_offset_account_branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | The branch to which the default offset account belongs. | [optional] 
**default_offset_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The account that is used by default as the offset account for this entry type. | [optional] 
**default_offset_subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) | The corresponding subaccount. | [optional] 
**reclasification_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The cash account that is used to temporary hold payments that came to the bank account but cannot be entered as valid payments because the customer or vendor is unknown.              The parameter is used together with the Use for Payments Reclassification check box. | [optional] 
**business_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The vendor account, if the entry type is used to record transactions that involve a particular vendor, or the customer account,              if the entry type is used to record transactions that involve a particular customer. | [optional] 
**description** | **string** | A detailed description of the entry type that is used as transaction description by default. | [optional] 
**use_for_payments_reclasification** | **bool** | A check box that you select if this entry type is used to record unknown payments that need to be reclassified later. | [optional] 
**reclasification_account_override** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The account that should be used instead of the reclassification account specified as the default one on the Entry Types. | [optional] 
**offset_account_override** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) | The account that should be used as the offset account for this entry type with this cash account instead of the default offset account specified on the Entry Types.               We recommend that for the disbursement type of transaction, you specify an expense account. For the receipt type of transaction, specify an asset account. | [optional] 
**offset_subaccount_override** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) | The subaccount for this entry type to be used with this cash account instead of the default offset subaccount specified on the Entry Types form. | [optional] 
**offset_account_branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | The branch to which the overriding offset account belongs. | [optional] 
**vat_zone** | [**\Trollweb\VismaNetApi\Model\VatZoneDto**](VatZoneDto.md) | The tax zone to be used by default with this entry type. | [optional] 
**tax_calculation_mode** | **string** | The tax calculation mode to be used by default with this entry type | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


