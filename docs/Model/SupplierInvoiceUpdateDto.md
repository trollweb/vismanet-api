# SupplierInvoiceUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableSupplierInvoiceTypes**](DtoValueNullableSupplierInvoiceTypes.md) |  | [optional] 
**reference_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**post_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format MMYYYY. | [optional] 
**financial_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format YYYYMM. | [optional] 
**supplier_reference** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**supplier_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**payment_ref_no** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**credit_terms_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**due_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**cash_discount_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**note** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**exchange_rate** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**invoice_lines** | [**\Trollweb\VismaNetApi\Model\SupplierInvoiceLineUpdateDto[]**](SupplierInvoiceLineUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


