# SegmentUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**segment_id** | **string** |  | [optional] 
**segment_value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


