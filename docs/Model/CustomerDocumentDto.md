# CustomerDocumentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**document_due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | Branch is deprecated, please use BranchNumber instead. | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerNumberDto**](CustomerNumberDto.md) |  | [optional] 
**document_type** | **string** |  | [optional] 
**reference_number** | **string** |  | [optional] 
**post_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format MMYYYY. | [optional] 
**financial_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format YYYYMM. | [optional] 
**closed_financial_period** | **string** | Format YYYYMM. | [optional] 
**document_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**status** | **string** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**amount** | **double** |  | [optional] 
**amount_in_currency** | **double** |  | [optional] 
**balance** | **double** |  | [optional] 
**balance_in_currency** | **double** |  | [optional] 
**cash_discount** | **double** |  | [optional] 
**cash_discount_in_currency** | **double** |  | [optional] 
**payment_method** | [**\Trollweb\VismaNetApi\Model\PaymentMethodIdDescriptionDto**](PaymentMethodIdDescriptionDto.md) |  | [optional] 
**customer_ref_number** | **string** |  | [optional] 
**invoice_text** | **string** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**created_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**note** | **string** |  | [optional] 
**vat_total** | **double** |  | [optional] 
**vat_total_in_currency** | **double** |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**cash_account** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


