# JournalTransactionUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**batch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**transaction_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**post_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format MMYYYY. | [optional] 
**financial_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format YYYYMM. | [optional] 
**ledger** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**exchange_rate** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**auto_reversing** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**control_total_in_currency** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**create_vat_transaction** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**skip_vat_amount_validation** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**transaction_code** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**journal_transaction_lines** | [**\Trollweb\VismaNetApi\Model\JournalTransactionLineUpdateDto[]**](JournalTransactionLineUpdateDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


