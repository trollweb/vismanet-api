# ExpenseClaimDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref_nbr** | **string** | The unique reference number of the expense claim document. | [optional] 
**status** | **string** | The current status of the expense claim: On Hold/Pending Approval/Approved/Rejected/Released. | [optional] 
**approval_status** | **string** | The approval status of the claim | [optional] 
**date** | [**\DateTime**](\DateTime.md) | The date when the claim was entered. | [optional] 
**description** | **string** | A description of the claim. | [optional] 
**claimed_by** | [**\Trollweb\VismaNetApi\Model\EmployeeNumberNameDto**](EmployeeNumberNameDto.md) | The employee who is claiming the expenses. If the claim is released, an Accounts Payable bill will be generated to this employee. | [optional] 
**claim_total** | **double** | The total amount of the claim. | [optional] 
**vat_taxable_total** | **double** | The document total that is subjected to VAT. | [optional] 
**vat_exempt_total** | **double** | The document total that is exempt from VAT. | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerNumberDto**](CustomerNumberDto.md) | The applicable customer. | [optional] 
**currency** | **string** | The currency of the claim. | [optional] 
**approval_date** | [**\DateTime**](\DateTime.md) | The date when the claim was approved. | [optional] 
**department** | [**\Trollweb\VismaNetApi\Model\DepartmentIdDescriptionDto**](DepartmentIdDescriptionDto.md) | The department associated with the expense claim. | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) | The company location associated with the claim. | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) | The lastest time the expense claim was modified | [optional] 
**details** | [**\Trollweb\VismaNetApi\Model\ExpenseClaimDetailDto[]**](ExpenseClaimDetailDto.md) | Expense Claim detail information | [optional] 
**approval_status_text** | **string** |  | [optional] 
**extras** | [**map[string,\Trollweb\VismaNetApi\Model\Object]**](Object.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


