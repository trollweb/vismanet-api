# SalesCategoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category_id** | **int** |  | [optional] 
**description** | **string** |  | [optional] 
**parent_id** | **int** |  | [optional] 
**sort_order** | **int** |  | [optional] 
**sub_categories** | [**\Trollweb\VismaNetApi\Model\SalesCategoryDto[]**](SalesCategoryDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


