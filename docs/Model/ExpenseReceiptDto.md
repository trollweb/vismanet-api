# ExpenseReceiptDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internal_id** | **int** |  | [optional] 
**receipt_id** | **string** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**currency** | [**\Trollweb\VismaNetApi\Model\CurrencyIdDescriptionDto**](CurrencyIdDescriptionDto.md) |  | [optional] 
**ref_nbr** | **string** |  | [optional] 
**inventory** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**description** | **string** |  | [optional] 
**uom** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**unit_cost** | **double** |  | [optional] 
**total_amount** | **double** |  | [optional] 
**employee_part** | **double** |  | [optional] 
**claim_amount** | **double** |  | [optional] 
**status** | **string** |  | [optional] 
**claimed_by** | [**\Trollweb\VismaNetApi\Model\EmployeeDto**](EmployeeDto.md) |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**expense_claim** | [**\Trollweb\VismaNetApi\Model\ExpenseClaimNumberDescriptionDto**](ExpenseClaimNumberDescriptionDto.md) |  | [optional] 
**invoiceable** | **bool** |  | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) |  | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) |  | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerNumberDto**](CustomerNumberDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) |  | [optional] 
**expense_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**expense_sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**sales_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**sales_sub** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto**](TaxCategoryNumberDescriptionDto.md) |  | [optional] 
**image** | [**\Trollweb\VismaNetApi\Model\AttachmentDto**](AttachmentDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


