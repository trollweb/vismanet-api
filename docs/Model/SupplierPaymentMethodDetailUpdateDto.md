# SupplierPaymentMethodDetailUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_method_detail_description** | **string** |  | [optional] 
**payment_method_detail_value** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


