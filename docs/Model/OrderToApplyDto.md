# OrderToApplyDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_type** | **string** |  | [optional] 
**order_no** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**applied_to_order** | **double** |  | [optional] 
**transferred_to_invoice** | **double** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**cash_discount_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**balance** | **double** |  | [optional] 
**description** | **string** |  | [optional] 
**order_total** | **double** |  | [optional] 
**currency** | **string** |  | [optional] 
**invoice_nbr** | **string** |  | [optional] 
**invoice_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


