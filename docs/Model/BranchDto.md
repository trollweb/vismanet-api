# BranchDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**main_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**main_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**delivery_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**delivery_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**corporate_id** | **string** |  | [optional] 
**vat_registration_id** | **string** |  | [optional] 
**default_country** | [**\Trollweb\VismaNetApi\Model\CountryDto**](CountryDto.md) |  | [optional] 
**industry_code** | [**\Trollweb\VismaNetApi\Model\IndustryCodeDto**](IndustryCodeDto.md) |  | [optional] 
**currency** | [**\Trollweb\VismaNetApi\Model\CurrencyDto**](CurrencyDto.md) |  | [optional] 
**vat_zone** | [**\Trollweb\VismaNetApi\Model\VatZoneDto**](VatZoneDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


