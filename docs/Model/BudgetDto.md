# BudgetDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**financial_year** | **string** |  | [optional] 
**released** | **bool** |  | [optional] 
**released_amount** | **double** |  | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**description** | **string** |  | [optional] 
**amount** | **double** |  | [optional] 
**distributed_amount** | **double** |  | [optional] 
**periods** | [**\Trollweb\VismaNetApi\Model\FinancialPeriodAmountDto[]**](FinancialPeriodAmountDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


