# GeneralLedgerBalanceDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**ledger** | [**\Trollweb\VismaNetApi\Model\LedgerDescriptionDto**](LedgerDescriptionDto.md) |  | [optional] 
**balance_type** | **string** |  | [optional] 
**financial_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format YYYYMM. | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDescriptionDto**](AccountNumberDescriptionDto.md) |  | [optional] 
**subaccount_id** | **int** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**period_to_date_debit** | **double** |  | [optional] 
**period_to_date_credit** | **double** |  | [optional] 
**beginning_balance** | **double** |  | [optional] 
**year_to_date_balance** | **double** |  | [optional] 
**period_to_date_debit_in_currency** | **double** |  | [optional] 
**period_to_date_credit_in_currency** | **double** |  | [optional] 
**beginning_balance_in_currency** | **double** |  | [optional] 
**year_to_date_balance_in_currency** | **double** |  | [optional] 
**year_closed** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


