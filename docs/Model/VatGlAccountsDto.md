# VatGlAccountsDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_payable_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**vat_payable_subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 
**vat_claimable_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**vat_claimable_subccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDescriptionDto**](SubAccountDescriptionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


