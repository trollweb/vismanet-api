# PurchaseOrderBasicDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_type** | **string** |  | [optional] 
**order_nbr** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**status** | **string** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**promised_on** | [**\DateTime**](\DateTime.md) |  | [optional] 
**description** | **string** |  | [optional] 
**supplier** | [**\Trollweb\VismaNetApi\Model\SupplierDescriptionDto**](SupplierDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationIdNameDto**](LocationIdNameDto.md) |  | [optional] 
**owner** | [**\Trollweb\VismaNetApi\Model\UserDescriptionDto**](UserDescriptionDto.md) |  | [optional] 
**currency** | **string** |  | [optional] 
**supplier_ref** | **string** |  | [optional] 
**line_total** | **double** |  | [optional] 
**vat_exempt_total** | **double** |  | [optional] 
**tax_total** | **double** |  | [optional] 
**order_total** | **double** |  | [optional] 
**control_total** | **double** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**lines** | [**\Trollweb\VismaNetApi\Model\PurchaseOrderLineDto[]**](PurchaseOrderLineDto.md) |  | [optional] 
**tax_details** | [**\Trollweb\VismaNetApi\Model\TaxDetailDto[]**](TaxDetailDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


