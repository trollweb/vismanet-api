# VatCategoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_category_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**exclude_listed_taxes** | **bool** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**vat_category_lines** | [**\Trollweb\VismaNetApi\Model\VatCategoryLineDto[]**](VatCategoryLineDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


