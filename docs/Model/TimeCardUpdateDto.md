# TimeCardUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**employee** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**summary** | [**\Trollweb\VismaNetApi\Model\TimeCardSummaryUpdateDto[]**](TimeCardSummaryUpdateDto.md) | Time Card summary information | [optional] 
**extras** | [**map[string,\Trollweb\VismaNetApi\Model\Object]**](Object.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


