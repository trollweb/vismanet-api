# CustomerInvoiceUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_method_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**credit_terms_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**currency_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**customer_ref_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**cash_discount_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**document_due_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**external_reference** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**exchange_rate** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) |  | [optional] 
**domestic_services_deductible_document** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**rot_rut_details** | [**\Trollweb\VismaNetApi\Model\RotRutUpdateDto**](RotRutUpdateDto.md) |  | [optional] 
**invoice_lines** | [**\Trollweb\VismaNetApi\Model\CustomerInvoiceLinesUpdateDto[]**](CustomerInvoiceLinesUpdateDto.md) |  | [optional] 
**reference_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**customer_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**document_date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 
**post_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format MMYYYY. | [optional] 
**financial_period** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The financial period to which the transactions recorded in the document should be posted. Use the format YYYYMM. | [optional] 
**invoice_text** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location_id** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**sales_person_id** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableInt32**](DtoValueNullableInt32.md) |  | [optional] 
**note** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**cash_account** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


