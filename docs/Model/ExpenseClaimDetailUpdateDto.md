# ExpenseClaimDetailUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** |  | [optional] 
**claim_detail_id** | [**\Trollweb\VismaNetApi\Model\DtoValueInt32**](DtoValueInt32.md) | Identifies the expense claim detail line to update | [optional] 
**date** | [**\Trollweb\VismaNetApi\Model\DtoValueDateTime**](DtoValueDateTime.md) | The date when the expense was incurred. | [optional] 
**expense_item** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The expense identifier, if an expense of this type is listed as an inventory nonstock item. | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | A description of the transaction. | [optional] 
**quantity** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) | The quantity of this expense item. | [optional] 
**uom** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The unit of measure in which the quantity is shown. | [optional] 
**unit_cost** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) | The cost of a unit of the item. | [optional] 
**invoiceable** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) | The currency of the expense receipt. However, if you enter a claim line directly, the currency value is read-only and matching the claim currency. | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The project or customer contract associated with the expense claim, if the work performed was for a project or contract. Project Task The task associated with the contract or project. | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The task associated with the contract or project. | [optional] 
**expense_account** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The expense account to which the system should record the part of the expense to be paid back to the employee. | [optional] 
**expense_subaccount** | [**\Trollweb\VismaNetApi\Model\SegmentUpdateDto[]**](SegmentUpdateDto.md) | The corresponding subaccount the system uses to record the part of the expense to be paid back to the employee. | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The branch that will reimburse the expenses to the employee. | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The tax category associated with the expense item. | [optional] 
**ref_nbr** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The identifier of the transaction. | [optional] 
**employee_part** | [**\Trollweb\VismaNetApi\Model\DtoValueDecimal**](DtoValueDecimal.md) | The part of the total amount that will not be paid back to the employee. The percentage depends on the company policy. | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The identifier of the customer associated with the expense. | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) | The identifier of the customer location associated with the expense. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


