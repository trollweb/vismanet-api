# SupplierDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**internal_id** | **int** |  | [optional] 
**number** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**main_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**main_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**account_reference** | **string** |  | [optional] 
**parent_record** | [**\Trollweb\VismaNetApi\Model\ParentRecordDto**](ParentRecordDto.md) |  | [optional] 
**supplier_class** | [**\Trollweb\VismaNetApi\Model\ClassDescriptionDto**](ClassDescriptionDto.md) |  | [optional] 
**credit_terms** | [**\Trollweb\VismaNetApi\Model\CreditTermsDto**](CreditTermsDto.md) |  | [optional] 
**document_language** | **string** |  | [optional] 
**currency_id** | **string** |  | [optional] 
**remit_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**remit_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**payment_method** | [**\Trollweb\VismaNetApi\Model\PaymentMethodIdDescriptionDto**](PaymentMethodIdDescriptionDto.md) |  | [optional] 
**cash_account** | **string** |  | [optional] 
**charge_bearer** | **string** |  | [optional] 
**account_used_for_payment** | **string** |  | [optional] 
**payment_by** | **string** |  | [optional] 
**payment_lead_time** | **int** |  | [optional] 
**payment_ref_display_mask** | **string** |  | [optional] 
**pay_separately** | **bool** |  | [optional] 
**supplier_address** | [**\Trollweb\VismaNetApi\Model\AddressDto**](AddressDto.md) |  | [optional] 
**supplier_contact** | [**\Trollweb\VismaNetApi\Model\ContactInfoDto**](ContactInfoDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**vat_registration_id** | **string** |  | [optional] 
**corporate_id** | **string** |  | [optional] 
**vat_zone** | [**\Trollweb\VismaNetApi\Model\VatZoneDto**](VatZoneDto.md) |  | [optional] 
**attributes** | [**\Trollweb\VismaNetApi\Model\AttributeIdValueDto[]**](AttributeIdValueDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**supplier_payment_method_details** | [**\Trollweb\VismaNetApi\Model\SupplierPaymentMethodDetailDto[]**](SupplierPaymentMethodDetailDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


