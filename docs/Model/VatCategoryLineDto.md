# VatCategoryLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vat_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**calculate_on** | **string** |  | [optional] 
**cash_discount** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


