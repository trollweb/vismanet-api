# PurchaseOrderBasicUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_type** | [**\Trollweb\VismaNetApi\Model\DtoValueNullablePurchaseOrderType**](DtoValueNullablePurchaseOrderType.md) |  | [optional] 
**order_number** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**hold** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean**](DtoValueNullableBoolean.md) |  | [optional] 
**date** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) |  | [optional] 
**promised_on** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime**](DtoValueNullableDateTime.md) |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**supplier** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**owner** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableGuid**](DtoValueNullableGuid.md) |  | [optional] 
**currency** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**supplier_ref** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**control_total** | [**\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal**](DtoValueNullableDecimal.md) |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**lines** | [**\Trollweb\VismaNetApi\Model\PurchaseOrderLineUpdateDto[]**](PurchaseOrderLineUpdateDto.md) |  | [optional] 
**change_dates_on_lines** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


