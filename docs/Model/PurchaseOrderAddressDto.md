# PurchaseOrderAddressDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**override_address** | **bool** |  | [optional] 
**address_id** | **int** |  | [optional] 
**address_line1** | **string** |  | [optional] 
**address_line2** | **string** |  | [optional] 
**address_line3** | **string** |  | [optional] 
**postal_code** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**country** | [**\Trollweb\VismaNetApi\Model\CountryDto**](CountryDto.md) |  | [optional] 
**county** | [**\Trollweb\VismaNetApi\Model\CountyDto**](CountyDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


