# CustomerSalesPriceDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**record_id** | **int** |  | [optional] 
**price_type** | **string** |  | [optional] 
**price_code** | **string** |  | [optional] 
**inventory_id** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**uo_m** | **string** |  | [optional] 
**promotion** | **bool** |  | [optional] 
**break_qty** | **double** |  | [optional] 
**price** | **double** |  | [optional] 
**currency** | **string** |  | [optional] 
**vat** | **string** |  | [optional] 
**effective_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**expiration_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


