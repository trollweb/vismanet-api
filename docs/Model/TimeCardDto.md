# TimeCardDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ref_nbr** | **string** | The unique reference number of the time card, which Acumatica ERP automatically assigns according to the numbering sequence specified as the Time Card Numbering Sequence on the Time and Expenses Preferences (EP.10.10.00) form. | [optional] 
**status** | **string** | The status of the time card | [optional] 
**approval_status** | **string** | The approval status of the time card | [optional] 
**week** | [**\Trollweb\VismaNetApi\Model\WeekNumberDescriptionDto**](WeekNumberDescriptionDto.md) | The week for which the time card has been created. | [optional] 
**employee** | [**\Trollweb\VismaNetApi\Model\EmployeeNumberNameDto**](EmployeeNumberNameDto.md) | The name of the employee whose time card is currently open. | [optional] 
**type** | **string** | The type of the time card. The following options are available:• Normal: Regular time card• Correction: Corrective time card, which is a time card that updates a released time card | [optional] 
**orig_ref_nbr** | **string** | The reference number of the time card being corrected. This box is filled in only when Correction is specified in the Type box | [optional] 
**time_spent** | **int** | The work hours spent by the employee during the week on activities with the Regular Hours earning type. | [optional] 
**invoiceable** | **int** | The invoiceable work hours spent by the employee during the week | [optional] 
**overtime_spent** | **int** | The work hours spent by the employee during the week on activities with the Overtime earning type. | [optional] 
**invoiceable_overtime** | **int** | The invoiceable overtime spent by the employee during the week | [optional] 
**total_time_spent** | **int** | The total working time (regular and overtime) for the week | [optional] 
**invoiceable_total_time** | **int** | The total invoiceable working time (regular and overtime) for the week | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) | The last time the time card line was modified | [optional] 
**summary** | [**\Trollweb\VismaNetApi\Model\TimeCardSummaryDto[]**](TimeCardSummaryDto.md) | Time Card summary information | [optional] 
**approval_status_text** | **string** |  | [optional] 
**extras** | [**map[string,\Trollweb\VismaNetApi\Model\Object]**](Object.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


