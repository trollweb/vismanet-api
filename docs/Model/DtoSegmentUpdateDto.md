# DtoSegmentUpdateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**segment_values** | [**\Trollweb\VismaNetApi\Model\DtoSegmentValueUpdateDtoBase[]**](DtoSegmentValueUpdateDtoBase.md) |  | [optional] 
**dimension_id** | **string** |  | [optional] 
**segement_id** | **int** |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


