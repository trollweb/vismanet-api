# PaymentLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | **string** |  | [optional] 
**ref_nbr** | **string** |  | [optional] 
**amount_paid** | **double** |  | [optional] 
**cash_discount_taken** | **double** |  | [optional] 
**balance_write_off** | **double** |  | [optional] 
**write_off_reason_code** | [**\Trollweb\VismaNetApi\Model\ReasonCodeDto**](ReasonCodeDto.md) |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**due_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**cash_discount_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**balance** | **double** |  | [optional] 
**cash_discount_balance** | **double** |  | [optional] 
**description** | **string** |  | [optional] 
**currency** | **string** |  | [optional] 
**post_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format MMYYYY. | [optional] 
**customer_order** | **string** |  | [optional] 
**cross_rate** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


