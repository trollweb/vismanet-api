# PurchaseReceiptBasicDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**receipt_type** | **string** |  | [optional] 
**receipt_nbr** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**status** | **string** |  | [optional] 
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**post_period** | **string** | The financial period to which the transactions recorded in the document should be posted. Format MMYYYY. | [optional] 
**warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**supplier** | [**\Trollweb\VismaNetApi\Model\SupplierDescriptionDto**](SupplierDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**currency** | **string** |  | [optional] 
**create_bill** | **bool** |  | [optional] 
**supplier_ref** | **string** |  | [optional] 
**total_qty** | **double** |  | [optional] 
**control_qty** | **double** |  | [optional] 
**vat_exempt_total** | **double** |  | [optional] 
**vat_taxable_total** | **double** |  | [optional] 
**total_amt** | **double** |  | [optional] 
**control_total** | **double** |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**lines** | [**\Trollweb\VismaNetApi\Model\PurchaseReceiptLineDto[]**](PurchaseReceiptLineDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


