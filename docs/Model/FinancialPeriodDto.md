# FinancialPeriodDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**year** | **int** |  | [optional] 
**period** | **string** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**description** | **string** |  | [optional] 
**active** | **bool** |  | [optional] 
**closed_in_supplier_ledger** | **bool** |  | [optional] 
**closed_in_customer_ledger** | **bool** |  | [optional] 
**closed_in_inventory_management** | **bool** |  | [optional] 
**closed_in_general_ledger** | **bool** |  | [optional] 
**closed_in_cash_management** | **bool** |  | [optional] 
**closed_in_fixed_assets** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


