# DtoSegmentValueUpdateDtoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operation** | **string** |  | [optional] 
**value** | **string** |  | [optional] 
**description** | [**\Trollweb\VismaNetApi\Model\DtoValueString**](DtoValueString.md) |  | [optional] 
**active** | [**\Trollweb\VismaNetApi\Model\DtoValueBoolean**](DtoValueBoolean.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


