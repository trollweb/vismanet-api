# WarehouseDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warehouse_id** | **string** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**replenishment_class** | [**\Trollweb\VismaNetApi\Model\ReplenishmentClassIdDescriptionDto**](ReplenishmentClassIdDescriptionDto.md) |  | [optional] 
**active** | **bool** |  | [optional] 
**lock_site_pi_count_entry** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**location_entry** | **string** |  | [optional] 
**avg_default_cost** | **string** |  | [optional] 
**fifo_default_cost** | **string** |  | [optional] 
**receipt_location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) |  | [optional] 
**ship_location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) |  | [optional] 
**return_location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) |  | [optional] 
**drop_ship_location** | [**\Trollweb\VismaNetApi\Model\LocationNameDescriptionDto**](LocationNameDescriptionDto.md) |  | [optional] 
**contact** | [**\Trollweb\VismaNetApi\Model\WarehouseContactDto**](WarehouseContactDto.md) |  | [optional] 
**address** | [**\Trollweb\VismaNetApi\Model\WarehouseAddressDto**](WarehouseAddressDto.md) |  | [optional] 
**locations** | [**\Trollweb\VismaNetApi\Model\WarehouseLocationDto[]**](WarehouseLocationDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


