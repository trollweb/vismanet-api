# TimeCardSummaryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** | The time card line number | [optional] 
**line_id** | **string** | The time card line id | [optional] 
**earning_type** | [**\Trollweb\VismaNetApi\Model\EarningTypeDto**](EarningTypeDto.md) | The type of the work time spent by the employee | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) | The project that the employee worked on. | [optional] 
**mon** | **int** | The work time reported for Monday, including overtime. | [optional] 
**tue** | **int** | The work time reported for Tuesday, including overtime. | [optional] 
**wed** | **int** | The work time reported for Wednesday, including overtime. | [optional] 
**thu** | **int** | The work time reported for Thursday, including overtime. | [optional] 
**fri** | **int** | The work time reported for Friday, including overtime. | [optional] 
**sat** | **int** | The work time reported for Saturday, including overtime. | [optional] 
**sun** | **int** | The work time reported for Sunday, including overtime. | [optional] 
**invoiceable** | **bool** | A check box that you select to indicate that these work hours are invoiceable. | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) | The project task that the employee worked on. | [optional] 
**time_spent** | **int** | The work time (regular and overtime) that the employee spent on the project and task during the week. | [optional] 
**description** | **string** | The description of the reported work hours. | [optional] 
**approval_status** | **string** | The approval status, which indicates whether the summary row requires approval and, if it does, what the current state of approval is. | [optional] 
**approver** | **string** | The identifier of the person authorized to approve the activity, if approval is required. This is either the approver of the project task or, if no approver is assigned to the project task, the project manager. | [optional] 
**approval_status_text** | **string** | The approval status text suitable for display | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) | The last time the time card line was modified | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


