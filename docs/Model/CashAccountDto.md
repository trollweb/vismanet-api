# CashAccountDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**entry_types** | [**\Trollweb\VismaNetApi\Model\EntryTypeDto[]**](EntryTypeDto.md) |  | [optional] 
**number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


