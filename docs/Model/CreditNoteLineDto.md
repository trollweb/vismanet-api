# CreditNoteLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) |  | [optional] 
**line_number** | **int** |  | [optional] 
**inventory_number** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**unit_price** | **double** |  | [optional] 
**unit_price_in_currency** | **double** |  | [optional] 
**manual_amount** | **double** |  | [optional] 
**manual_amount_in_currency** | **double** |  | [optional] 
**amount** | **double** |  | [optional] 
**amount_in_currency** | **double** |  | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**vat_code** | [**\Trollweb\VismaNetApi\Model\VatCodeDto**](VatCodeDto.md) |  | [optional] 
**uom** | **string** |  | [optional] 
**discount_percent** | **double** |  | [optional] 
**discount_amount** | **double** |  | [optional] 
**discount_amount_in_currency** | **double** |  | [optional] 
**manual_discount** | **bool** |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**salesperson** | **string** |  | [optional] 
**deferral_schedule** | **int** |  | [optional] 
**deferral_code** | **string** |  | [optional] 
**discount_code** | **string** |  | [optional] 
**note** | **string** |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


