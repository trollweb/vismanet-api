# FinancialPeriodQueryParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**greater_than_value** | **string** |  | [optional] 
**number_to_read** | **int** |  | [optional] 
**skip_records** | **int** |  | [optional] 
**order_by** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


