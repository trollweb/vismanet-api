# SupplierInvoiceLineDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | [optional] 
**inventory_number** | **string** |  | [optional] 
**transaction_description** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**uom** | **string** |  | [optional] 
**unit_cost** | **double** |  | [optional] 
**unit_cost_in_currency** | **double** |  | [optional] 
**cost** | **double** |  | [optional] 
**cost_in_currency** | **double** |  | [optional] 
**account** | [**\Trollweb\VismaNetApi\Model\AccountNumberDto**](AccountNumberDto.md) |  | [optional] 
**subaccount** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**deferral_schedule** | **int** |  | [optional] 
**deferral_code** | **string** |  | [optional] 
**vat_code** | [**\Trollweb\VismaNetApi\Model\VatCodeDto**](VatCodeDto.md) |  | [optional] 
**po_number** | **string** |  | [optional] 
**po_line_nr** | **int** |  | [optional] 
**po_receipt_nbr** | **string** |  | [optional] 
**po_receipt_line_nbr** | **int** |  | [optional] 
**branch_number** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) | Note: The field type has been changed from string to BranchNumberDto, please consider doing changes accordingly. | [optional] 
**note** | **string** |  | [optional] 
**attachments** | [**\Trollweb\VismaNetApi\Model\AttachmentDto[]**](AttachmentDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


