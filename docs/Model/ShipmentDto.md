# ShipmentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipment_number** | **string** |  | [optional] 
**shipment_type** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**hold** | **bool** |  | [optional] 
**operation** | **string** |  | [optional] 
**shipment_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**customer** | [**\Trollweb\VismaNetApi\Model\CustomerDescriptionDto**](CustomerDescriptionDto.md) |  | [optional] 
**location** | [**\Trollweb\VismaNetApi\Model\LocationDto**](LocationDto.md) |  | [optional] 
**from_warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**to_warehouse** | [**\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto**](WarehouseIdDescriptionDto.md) |  | [optional] 
**currency_id** | **string** |  | [optional] 
**owner** | [**\Trollweb\VismaNetApi\Model\UserDescriptionDto**](UserDescriptionDto.md) |  | [optional] 
**shipped_quantity** | **double** |  | [optional] 
**shipped_weight** | **double** |  | [optional] 
**shipped_volume** | **double** |  | [optional] 
**packages** | **double** |  | [optional] 
**packages_weight** | **double** |  | [optional] 
**control_quantity** | **double** |  | [optional] 
**delivery_address** | [**\Trollweb\VismaNetApi\Model\ShipmentAddressDto**](ShipmentAddressDto.md) |  | [optional] 
**delivery_contact** | [**\Trollweb\VismaNetApi\Model\ShipmentContactDto**](ShipmentContactDto.md) |  | [optional] 
**ship_via** | [**\Trollweb\VismaNetApi\Model\CarrierDescriptionDto**](CarrierDescriptionDto.md) |  | [optional] 
**fob_point** | [**\Trollweb\VismaNetApi\Model\FOBPointDescriptionDto**](FOBPointDescriptionDto.md) |  | [optional] 
**shipping_terms** | [**\Trollweb\VismaNetApi\Model\ShippingTermsDescriptionDto**](ShippingTermsDescriptionDto.md) |  | [optional] 
**shipping_zone** | [**\Trollweb\VismaNetApi\Model\ShippingZoneDescriptionDto**](ShippingZoneDescriptionDto.md) |  | [optional] 
**residential_delivery** | **bool** |  | [optional] 
**saturday_delivery** | **bool** |  | [optional] 
**use_customer_account** | **bool** |  | [optional] 
**insurance** | **bool** |  | [optional] 
**freight_cost** | **double** |  | [optional] 
**freight_amt** | **double** |  | [optional] 
**transaction_type** | [**\Trollweb\VismaNetApi\Model\SigmaIntrastatTransactionDescriptionDto**](SigmaIntrastatTransactionDescriptionDto.md) |  | [optional] 
**mode_of_trasport** | [**\Trollweb\VismaNetApi\Model\ModeOfTransportDto**](ModeOfTransportDto.md) |  | [optional] 
**container** | **bool** |  | [optional] 
**shipment_detail_lines** | [**\Trollweb\VismaNetApi\Model\ShipmentDetailLineDto[]**](ShipmentDetailLineDto.md) |  | [optional] 
**shipment_order_lines** | [**\Trollweb\VismaNetApi\Model\ShipmentOrderLineDto[]**](ShipmentOrderLineDto.md) |  | [optional] 
**shipment_package_lines** | [**\Trollweb\VismaNetApi\Model\PackageDetailLineDto[]**](PackageDetailLineDto.md) |  | [optional] 
**last_modified_date_time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


