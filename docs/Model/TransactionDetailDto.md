# TransactionDetailDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line_number** | **int** |  | [optional] 
**branch** | [**\Trollweb\VismaNetApi\Model\BranchNumberDto**](BranchNumberDto.md) |  | [optional] 
**item** | [**\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto**](InventoryNumberDescriptionDto.md) |  | [optional] 
**description** | **string** |  | [optional] 
**quantity** | **double** |  | [optional] 
**uom** | **string** |  | [optional] 
**price** | **double** |  | [optional] 
**amount** | **double** |  | [optional] 
**offset_cash_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**offset_account** | [**\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto**](AccountNumberTypeDescriptionDto.md) |  | [optional] 
**offset_sub_account** | [**\Trollweb\VismaNetApi\Model\SubAccountDto**](SubAccountDto.md) |  | [optional] 
**tax_category** | [**\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto**](TaxCategoryNumberDescriptionDto.md) |  | [optional] 
**non_billable** | **bool** |  | [optional] 
**project** | [**\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto**](ProjectIdDescriptionDto.md) |  | [optional] 
**project_task** | [**\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto**](ProjectTaskIdDescriptionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


