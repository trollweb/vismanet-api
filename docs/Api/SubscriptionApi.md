# Trollweb\VismaNetApi\SubscriptionApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSubscription**](SubscriptionApi.md#createSubscription) | **POST** /resources/v1/subscription | Create a new subscription for the current user
[**deleteSubscription**](SubscriptionApi.md#deleteSubscription) | **DELETE** /resources/v1/subscription/{id} | Delete a specific subscription
[**findSubscriptionById**](SubscriptionApi.md#findSubscriptionById) | **GET** /resources/v1/subscription/{id} | Get a specific subscription
[**getAllSubscriptions**](SubscriptionApi.md#getAllSubscriptions) | **GET** /resources/v1/subscription | Get all subscriptions made by the current user
[**updateSubscription**](SubscriptionApi.md#updateSubscription) | **PUT** /resources/v1/subscription/{id} | Update a specific subscription


# **createSubscription**
> \Trollweb\VismaNetApi\Model\SubscriptionDto createSubscription($subscription)

Create a new subscription for the current user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubscriptionApi();
$subscription = new \Trollweb\VismaNetApi\Model\SubscriptionDto(); // \Trollweb\VismaNetApi\Model\SubscriptionDto | 

try {
    $result = $api_instance->createSubscription($subscription);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApi->createSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription** | [**\Trollweb\VismaNetApi\Model\SubscriptionDto**](../Model/\Trollweb\VismaNetApi\Model\SubscriptionDto.md)|  |

### Return type

[**\Trollweb\VismaNetApi\Model\SubscriptionDto**](../Model/SubscriptionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteSubscription**
> deleteSubscription($id)

Delete a specific subscription



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubscriptionApi();
$id = 789; // int | 

try {
    $api_instance->deleteSubscription($id);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApi->deleteSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

void (empty response body)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **findSubscriptionById**
> \Trollweb\VismaNetApi\Model\SubscriptionDto findSubscriptionById($id)

Get a specific subscription



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubscriptionApi();
$id = 789; // int | 

try {
    $result = $api_instance->findSubscriptionById($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApi->findSubscriptionById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**\Trollweb\VismaNetApi\Model\SubscriptionDto**](../Model/SubscriptionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllSubscriptions**
> \Trollweb\VismaNetApi\Model\SubscriptionDto[] getAllSubscriptions()

Get all subscriptions made by the current user



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubscriptionApi();

try {
    $result = $api_instance->getAllSubscriptions();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApi->getAllSubscriptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\SubscriptionDto[]**](../Model/SubscriptionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSubscription**
> updateSubscription($id, $subscription)

Update a specific subscription



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubscriptionApi();
$id = 789; // int | 
$subscription = new \Trollweb\VismaNetApi\Model\SubscriptionDto(); // \Trollweb\VismaNetApi\Model\SubscriptionDto | 

try {
    $api_instance->updateSubscription($id, $subscription);
} catch (Exception $e) {
    echo 'Exception when calling SubscriptionApi->updateSubscription: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **subscription** | [**\Trollweb\VismaNetApi\Model\SubscriptionDto**](../Model/\Trollweb\VismaNetApi\Model\SubscriptionDto.md)|  |

### Return type

void (empty response body)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

