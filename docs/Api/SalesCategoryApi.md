# Trollweb\VismaNetApi\SalesCategoryApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesCategoryGet**](SalesCategoryApi.md#salesCategoryGet) | **GET** /controller/api/v1/salescategory | Get Sales Categories
[**salesCategoryGetCategoryBycategoryId**](SalesCategoryApi.md#salesCategoryGetCategoryBycategoryId) | **GET** /controller/api/v1/salescategory/{categoryId} | Get a Sales Category
[**salesCategoryGetItemsForCategoryBycategoryId**](SalesCategoryApi.md#salesCategoryGetItemsForCategoryBycategoryId) | **GET** /controller/api/v1/salescategory/{categoryId}/item | Get a range of Inventory Items from a specific Sales Category


# **salesCategoryGet**
> \Trollweb\VismaNetApi\Model\SalesCategoryDto[] salesCategoryGet()

Get Sales Categories

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesCategoryApi();

try {
    $result = $api_instance->salesCategoryGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesCategoryApi->salesCategoryGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\SalesCategoryDto[]**](../Model/SalesCategoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesCategoryGetCategoryBycategoryId**
> \Trollweb\VismaNetApi\Model\SalesCategoryDto salesCategoryGetCategoryBycategoryId($category_id)

Get a Sales Category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesCategoryApi();
$category_id = 56; // int | Identifies the Sales Category

try {
    $result = $api_instance->salesCategoryGetCategoryBycategoryId($category_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesCategoryApi->salesCategoryGetCategoryBycategoryId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **int**| Identifies the Sales Category |

### Return type

[**\Trollweb\VismaNetApi\Model\SalesCategoryDto**](../Model/SalesCategoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesCategoryGetItemsForCategoryBycategoryId**
> \Trollweb\VismaNetApi\Model\InventoryDto[] salesCategoryGetItemsForCategoryBycategoryId($category_id)

Get a range of Inventory Items from a specific Sales Category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesCategoryApi();
$category_id = 56; // int | Identifies the Sales Category

try {
    $result = $api_instance->salesCategoryGetItemsForCategoryBycategoryId($category_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesCategoryApi->salesCategoryGetItemsForCategoryBycategoryId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **int**| Identifies the Sales Category |

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryDto[]**](../Model/InventoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

