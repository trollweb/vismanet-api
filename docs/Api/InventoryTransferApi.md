# Trollweb\VismaNetApi\InventoryTransferApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryTransferGetAll**](InventoryTransferApi.md#inventoryTransferGetAll) | **GET** /controller/api/v1/inventoryTransfer | Get a range of Inventory Transfers
[**inventoryTransferGetByinventoryTransferNumber**](InventoryTransferApi.md#inventoryTransferGetByinventoryTransferNumber) | **GET** /controller/api/v1/inventoryTransfer/{inventoryTransferNumber} | Get a specific Inventory Transfer document
[**inventoryTransferPost**](InventoryTransferApi.md#inventoryTransferPost) | **POST** /controller/api/v1/inventoryTransfer | Create an inventory transfer
[**inventoryTransferPutBytransferRefNumber**](InventoryTransferApi.md#inventoryTransferPutBytransferRefNumber) | **PUT** /controller/api/v1/inventoryTransfer/{transferRefNumber} | Update a specific inventory transfer
[**inventoryTransferReleaseDocumentBytransferNumber**](InventoryTransferApi.md#inventoryTransferReleaseDocumentBytransferNumber) | **POST** /controller/api/v1/inventoryTransfer/{transferNumber}/action/release | Release inventory operation


# **inventoryTransferGetAll**
> \Trollweb\VismaNetApi\Model\InventoryTransferDto[] inventoryTransferGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Inventory Transfers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryTransferApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->inventoryTransferGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryTransferApi->inventoryTransferGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryTransferDto[]**](../Model/InventoryTransferDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryTransferGetByinventoryTransferNumber**
> \Trollweb\VismaNetApi\Model\InventoryTransferDto inventoryTransferGetByinventoryTransferNumber($inventory_transfer_number)

Get a specific Inventory Transfer document

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryTransferApi();
$inventory_transfer_number = "inventory_transfer_number_example"; // string | Identifies the Inventory Transfer document

try {
    $result = $api_instance->inventoryTransferGetByinventoryTransferNumber($inventory_transfer_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryTransferApi->inventoryTransferGetByinventoryTransferNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_transfer_number** | **string**| Identifies the Inventory Transfer document |

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryTransferDto**](../Model/InventoryTransferDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryTransferPost**
> \Trollweb\VismaNetApi\Model\Object inventoryTransferPost($inventory_transfer)

Create an inventory transfer

Response Message has StatusCode Created if POST operation succed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryTransferApi();
$inventory_transfer = new \Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto | Define the data for the inventory transfer to create

try {
    $result = $api_instance->inventoryTransferPost($inventory_transfer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryTransferApi->inventoryTransferPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_transfer** | [**\Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto.md)| Define the data for the inventory transfer to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryTransferPutBytransferRefNumber**
> \Trollweb\VismaNetApi\Model\Object inventoryTransferPutBytransferRefNumber($transfer_ref_number, $transfer)

Update a specific inventory transfer

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryTransferApi();
$transfer_ref_number = "transfer_ref_number_example"; // string | Identifies the inventory transfer to update
$transfer = new \Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto | The data to update for inventory transfer

try {
    $result = $api_instance->inventoryTransferPutBytransferRefNumber($transfer_ref_number, $transfer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryTransferApi->inventoryTransferPutBytransferRefNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transfer_ref_number** | **string**| Identifies the inventory transfer to update |
 **transfer** | [**\Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryTransferUpdateDto.md)| The data to update for inventory transfer |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryTransferReleaseDocumentBytransferNumber**
> \Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto inventoryTransferReleaseDocumentBytransferNumber($transfer_number)

Release inventory operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryTransferApi();
$transfer_number = "transfer_number_example"; // string | Reference number of the released transfer to be released

try {
    $result = $api_instance->inventoryTransferReleaseDocumentBytransferNumber($transfer_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryTransferApi->inventoryTransferReleaseDocumentBytransferNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transfer_number** | **string**| Reference number of the released transfer to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto**](../Model/ReleaseInventoryDocumentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

