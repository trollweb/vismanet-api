# Trollweb\VismaNetApi\CurrencyApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**currencyGetAllCurrencies**](CurrencyApi.md#currencyGetAllCurrencies) | **GET** /controller/api/v1/currency | Gets all available Currencies
[**currencyGetSpecificCurrencyBycuryId**](CurrencyApi.md#currencyGetSpecificCurrencyBycuryId) | **GET** /controller/api/v1/currency/{curyId} | Get a specific Currency


# **currencyGetAllCurrencies**
> \Trollweb\VismaNetApi\Model\CurrencyDto[] currencyGetAllCurrencies()

Gets all available Currencies

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CurrencyApi();

try {
    $result = $api_instance->currencyGetAllCurrencies();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CurrencyApi->currencyGetAllCurrencies: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\CurrencyDto[]**](../Model/CurrencyDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **currencyGetSpecificCurrencyBycuryId**
> \Trollweb\VismaNetApi\Model\CurrencyDto currencyGetSpecificCurrencyBycuryId($cury_id)

Get a specific Currency

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CurrencyApi();
$cury_id = "cury_id_example"; // string | 

try {
    $result = $api_instance->currencyGetSpecificCurrencyBycuryId($cury_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CurrencyApi->currencyGetSpecificCurrencyBycuryId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cury_id** | **string**|  |

### Return type

[**\Trollweb\VismaNetApi\Model\CurrencyDto**](../Model/CurrencyDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

