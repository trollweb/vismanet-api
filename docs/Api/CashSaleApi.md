# Trollweb\VismaNetApi\CashSaleApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cashSaleGetAllCashSales**](CashSaleApi.md#cashSaleGetAllCashSales) | **GET** /controller/api/v1/cashsale | Get a range of Cash Sales
[**cashSaleGetBydocumentNumber**](CashSaleApi.md#cashSaleGetBydocumentNumber) | **GET** /controller/api/v1/cashsale/{documentNumber} | Get a specific Cash Sale
[**cashSalePost**](CashSaleApi.md#cashSalePost) | **POST** /controller/api/v1/cashsale | Create a Cash Sale
[**cashSalePutBydocumentnumber**](CashSaleApi.md#cashSalePutBydocumentnumber) | **PUT** /controller/api/v1/cashsale/{documentnumber} | Update a specific Cash Sale


# **cashSaleGetAllCashSales**
> \Trollweb\VismaNetApi\Model\CashSaleDto[] cashSaleGetAllCashSales($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of Cash Sales

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashSaleApi();
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->cashSaleGetAllCashSales($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashSaleApi->cashSaleGetAllCashSales: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CashSaleDto[]**](../Model/CashSaleDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashSaleGetBydocumentNumber**
> \Trollweb\VismaNetApi\Model\CashSaleDto cashSaleGetBydocumentNumber($document_number)

Get a specific Cash Sale

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashSaleApi();
$document_number = "document_number_example"; // string | Identifies the Cash Sale Document

try {
    $result = $api_instance->cashSaleGetBydocumentNumber($document_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashSaleApi->cashSaleGetBydocumentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_number** | **string**| Identifies the Cash Sale Document |

### Return type

[**\Trollweb\VismaNetApi\Model\CashSaleDto**](../Model/CashSaleDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashSalePost**
> \Trollweb\VismaNetApi\Model\Object cashSalePost($cash_sale_update_dto)

Create a Cash Sale

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashSaleApi();
$cash_sale_update_dto = new \Trollweb\VismaNetApi\Model\CashSaleUpdateDto(); // \Trollweb\VismaNetApi\Model\CashSaleUpdateDto | Defines the data for the Cash Sale to create

try {
    $result = $api_instance->cashSalePost($cash_sale_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashSaleApi->cashSalePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_sale_update_dto** | [**\Trollweb\VismaNetApi\Model\CashSaleUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CashSaleUpdateDto.md)| Defines the data for the Cash Sale to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashSalePutBydocumentnumber**
> \Trollweb\VismaNetApi\Model\Object cashSalePutBydocumentnumber($documentnumber, $cash_sale_update_dto)

Update a specific Cash Sale

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashSaleApi();
$documentnumber = "documentnumber_example"; // string | Identifies the Cash Sale to update
$cash_sale_update_dto = new \Trollweb\VismaNetApi\Model\CashSaleUpdateDto(); // \Trollweb\VismaNetApi\Model\CashSaleUpdateDto | Defines the data for the Cash Sale to update

try {
    $result = $api_instance->cashSalePutBydocumentnumber($documentnumber, $cash_sale_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashSaleApi->cashSalePutBydocumentnumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documentnumber** | **string**| Identifies the Cash Sale to update |
 **cash_sale_update_dto** | [**\Trollweb\VismaNetApi\Model\CashSaleUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CashSaleUpdateDto.md)| Defines the data for the Cash Sale to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

