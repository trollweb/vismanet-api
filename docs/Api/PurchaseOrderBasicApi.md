# Trollweb\VismaNetApi\PurchaseOrderBasicApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseOrderBasicGetAllOrderBasic**](PurchaseOrderBasicApi.md#purchaseOrderBasicGetAllOrderBasic) | **GET** /controller/api/v1/purchaseorderbasic | Get a range of Purchase Order
[**purchaseOrderBasicGetPurchaseOrderBasicByorderNbr**](PurchaseOrderBasicApi.md#purchaseOrderBasicGetPurchaseOrderBasicByorderNbr) | **GET** /controller/api/v1/purchaseorderbasic/{orderNbr} | Get a specific Purchase Order
[**purchaseOrderBasicPost**](PurchaseOrderBasicApi.md#purchaseOrderBasicPost) | **POST** /controller/api/v1/purchaseorderbasic | Create a Purchase Order Basic
[**purchaseOrderBasicPutBypurchaseOrderNumber**](PurchaseOrderBasicApi.md#purchaseOrderBasicPutBypurchaseOrderNumber) | **PUT** /controller/api/v1/purchaseorderbasic/{purchaseOrderNumber} | Update a Purchase Order Basic


# **purchaseOrderBasicGetAllOrderBasic**
> \Trollweb\VismaNetApi\Model\PurchaseOrderBasicDto[] purchaseOrderBasicGetAllOrderBasic($greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition, $order_type, $order_status, $supplier)

Get a range of Purchase Order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderBasicApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$order_type = "order_type_example"; // string | 
$order_status = "order_status_example"; // string | 
$supplier = "supplier_example"; // string | 

try {
    $result = $api_instance->purchaseOrderBasicGetAllOrderBasic($greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition, $order_type, $order_status, $supplier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderBasicApi->purchaseOrderBasicGetAllOrderBasic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **order_type** | **string**|  | [optional]
 **order_status** | **string**|  | [optional]
 **supplier** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseOrderBasicDto[]**](../Model/PurchaseOrderBasicDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseOrderBasicGetPurchaseOrderBasicByorderNbr**
> \Trollweb\VismaNetApi\Model\PurchaseOrderBasicDto purchaseOrderBasicGetPurchaseOrderBasicByorderNbr($order_nbr)

Get a specific Purchase Order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderBasicApi();
$order_nbr = "order_nbr_example"; // string | Identifies the Purchase Order

try {
    $result = $api_instance->purchaseOrderBasicGetPurchaseOrderBasicByorderNbr($order_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderBasicApi->purchaseOrderBasicGetPurchaseOrderBasicByorderNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_nbr** | **string**| Identifies the Purchase Order |

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseOrderBasicDto**](../Model/PurchaseOrderBasicDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseOrderBasicPost**
> \Trollweb\VismaNetApi\Model\Object purchaseOrderBasicPost($purchase_order_basic_update_dto)

Create a Purchase Order Basic

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderBasicApi();
$purchase_order_basic_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto | Defines the data for the Purchase Order to create

try {
    $result = $api_instance->purchaseOrderBasicPost($purchase_order_basic_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderBasicApi->purchaseOrderBasicPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_order_basic_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto.md)| Defines the data for the Purchase Order to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseOrderBasicPutBypurchaseOrderNumber**
> \Trollweb\VismaNetApi\Model\Object purchaseOrderBasicPutBypurchaseOrderNumber($purchase_order_number, $purchase_order_basic_update_dto)

Update a Purchase Order Basic

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderBasicApi();
$purchase_order_number = "purchase_order_number_example"; // string | Identifies the Purchase Order to update
$purchase_order_basic_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto | Defines the data for the Purchase Order to update

try {
    $result = $api_instance->purchaseOrderBasicPutBypurchaseOrderNumber($purchase_order_number, $purchase_order_basic_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderBasicApi->purchaseOrderBasicPutBypurchaseOrderNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_order_number** | **string**| Identifies the Purchase Order to update |
 **purchase_order_basic_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseOrderBasicUpdateDto.md)| Defines the data for the Purchase Order to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

