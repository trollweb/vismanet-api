# Trollweb\VismaNetApi\PurchaseOrderApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseOrderGetAllOrder**](PurchaseOrderApi.md#purchaseOrderGetAllOrder) | **GET** /controller/api/v1/purchaseorder | Get a range of Purchase Order
[**purchaseOrderGetOrderByorderNbr**](PurchaseOrderApi.md#purchaseOrderGetOrderByorderNbr) | **GET** /controller/api/v1/purchaseorder/{orderNbr} | Get a specific Purchase Order
[**purchaseOrderPost**](PurchaseOrderApi.md#purchaseOrderPost) | **POST** /controller/api/v1/purchaseorder | Create a Purchase Order
[**purchaseOrderPutBypurchaseOrderNumber**](PurchaseOrderApi.md#purchaseOrderPutBypurchaseOrderNumber) | **PUT** /controller/api/v1/purchaseorder/{purchaseOrderNumber} | Update a Purchase Order


# **purchaseOrderGetAllOrder**
> \Trollweb\VismaNetApi\Model\PurchaseOrderDto[] purchaseOrderGetAllOrder($greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition, $order_type, $order_status, $supplier)

Get a range of Purchase Order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$order_type = "order_type_example"; // string | 
$order_status = "order_status_example"; // string | 
$supplier = "supplier_example"; // string | 

try {
    $result = $api_instance->purchaseOrderGetAllOrder($greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition, $order_type, $order_status, $supplier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderApi->purchaseOrderGetAllOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **order_type** | **string**|  | [optional]
 **order_status** | **string**|  | [optional]
 **supplier** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseOrderDto[]**](../Model/PurchaseOrderDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseOrderGetOrderByorderNbr**
> \Trollweb\VismaNetApi\Model\PurchaseOrderDto purchaseOrderGetOrderByorderNbr($order_nbr)

Get a specific Purchase Order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderApi();
$order_nbr = "order_nbr_example"; // string | Identifies the Purchase Order

try {
    $result = $api_instance->purchaseOrderGetOrderByorderNbr($order_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderApi->purchaseOrderGetOrderByorderNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_nbr** | **string**| Identifies the Purchase Order |

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseOrderDto**](../Model/PurchaseOrderDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseOrderPost**
> \Trollweb\VismaNetApi\Model\Object purchaseOrderPost($purchase_order_update_dto)

Create a Purchase Order

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderApi();
$purchase_order_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto | Defines the data for the Purchase Order to create

try {
    $result = $api_instance->purchaseOrderPost($purchase_order_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderApi->purchaseOrderPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_order_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto.md)| Defines the data for the Purchase Order to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseOrderPutBypurchaseOrderNumber**
> \Trollweb\VismaNetApi\Model\Object purchaseOrderPutBypurchaseOrderNumber($purchase_order_number, $purchase_order_update_dto)

Update a Purchase Order

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseOrderApi();
$purchase_order_number = "purchase_order_number_example"; // string | Identifies the Purchase Order to update
$purchase_order_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto | Defines the data for the Purchase Order to update

try {
    $result = $api_instance->purchaseOrderPutBypurchaseOrderNumber($purchase_order_number, $purchase_order_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseOrderApi->purchaseOrderPutBypurchaseOrderNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **purchase_order_number** | **string**| Identifies the Purchase Order to update |
 **purchase_order_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseOrderUpdateDto.md)| Defines the data for the Purchase Order to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

