# Trollweb\VismaNetApi\ContactApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactCreateContact**](ContactApi.md#contactCreateContact) | **POST** /controller/api/v1/contact | Create a Contact
[**contactGetAllContacts**](ContactApi.md#contactGetAllContacts) | **GET** /controller/api/v1/contact | Get a range of Contacts
[**contactGetBycontactId**](ContactApi.md#contactGetBycontactId) | **GET** /controller/api/v1/contact/{contactId} | Get a specific Contact
[**contactPutBycontactId**](ContactApi.md#contactPutBycontactId) | **PUT** /controller/api/v1/contact/{contactId} | Update a specific Contact


# **contactCreateContact**
> \Trollweb\VismaNetApi\Model\Object contactCreateContact($contact)

Create a Contact

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ContactApi();
$contact = new \Trollweb\VismaNetApi\Model\ContactUpdateDto(); // \Trollweb\VismaNetApi\Model\ContactUpdateDto | Defines the data for the Contact to create

try {
    $result = $api_instance->contactCreateContact($contact);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactApi->contactCreateContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact** | [**\Trollweb\VismaNetApi\Model\ContactUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ContactUpdateDto.md)| Defines the data for the Contact to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactGetAllContacts**
> \Trollweb\VismaNetApi\Model\ContactDto[] contactGetAllContacts($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Contacts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ContactApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->contactGetAllContacts($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactApi->contactGetAllContacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ContactDto[]**](../Model/ContactDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactGetBycontactId**
> \Trollweb\VismaNetApi\Model\ContactDto contactGetBycontactId($contact_id)

Get a specific Contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ContactApi();
$contact_id = 56; // int | Identifies the Contact

try {
    $result = $api_instance->contactGetBycontactId($contact_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactApi->contactGetBycontactId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **int**| Identifies the Contact |

### Return type

[**\Trollweb\VismaNetApi\Model\ContactDto**](../Model/ContactDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contactPutBycontactId**
> \Trollweb\VismaNetApi\Model\Object contactPutBycontactId($contact_id, $contact)

Update a specific Contact

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ContactApi();
$contact_id = 56; // int | Identifies the Contact to update
$contact = new \Trollweb\VismaNetApi\Model\ContactUpdateDto(); // \Trollweb\VismaNetApi\Model\ContactUpdateDto | Defines the data for the Contact to update

try {
    $result = $api_instance->contactPutBycontactId($contact_id, $contact);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactApi->contactPutBycontactId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contact_id** | **int**| Identifies the Contact to update |
 **contact** | [**\Trollweb\VismaNetApi\Model\ContactUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ContactUpdateDto.md)| Defines the data for the Contact to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

