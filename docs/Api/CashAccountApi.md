# Trollweb\VismaNetApi\CashAccountApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cashAccountGetAll**](CashAccountApi.md#cashAccountGetAll) | **GET** /controller/api/v1/cashaccount | Get all Cash Accounts
[**cashAccountGetByaccountNumber**](CashAccountApi.md#cashAccountGetByaccountNumber) | **GET** /controller/api/v1/cashaccount/{accountNumber} | Get a specific cash account


# **cashAccountGetAll**
> \Trollweb\VismaNetApi\Model\CashAccountDto[] cashAccountGetAll($greater_than_value, $number_to_read, $skip_records)

Get all Cash Accounts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashAccountApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 

try {
    $result = $api_instance->cashAccountGetAll($greater_than_value, $number_to_read, $skip_records);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashAccountApi->cashAccountGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CashAccountDto[]**](../Model/CashAccountDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashAccountGetByaccountNumber**
> \Trollweb\VismaNetApi\Model\CashAccountDto cashAccountGetByaccountNumber($account_number)

Get a specific cash account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashAccountApi();
$account_number = "account_number_example"; // string | Identifies the cash account

try {
    $result = $api_instance->cashAccountGetByaccountNumber($account_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashAccountApi->cashAccountGetByaccountNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_number** | **string**| Identifies the cash account |

### Return type

[**\Trollweb\VismaNetApi\Model\CashAccountDto**](../Model/CashAccountDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

