# Trollweb\VismaNetApi\AccountApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountGetAll**](AccountApi.md#accountGetAll) | **GET** /controller/api/v1/account | Gets a range of Accounts
[**accountGetByaccountCd**](AccountApi.md#accountGetByaccountCd) | **GET** /controller/api/v1/account/{accountCd} | Get specific Account


# **accountGetAll**
> \Trollweb\VismaNetApi\Model\AccountDto[] accountGetAll($greater_than_value, $public_code, $external_code1, $external_code2, $analysis_code, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Gets a range of Accounts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\AccountApi();
$greater_than_value = "greater_than_value_example"; // string | 
$public_code = 56; // int | 
$external_code1 = "external_code1_example"; // string | 
$external_code2 = "external_code2_example"; // string | 
$analysis_code = "analysis_code_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->accountGetAll($greater_than_value, $public_code, $external_code1, $external_code2, $analysis_code, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **public_code** | **int**|  | [optional]
 **external_code1** | **string**|  | [optional]
 **external_code2** | **string**|  | [optional]
 **analysis_code** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\AccountDto[]**](../Model/AccountDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **accountGetByaccountCd**
> \Trollweb\VismaNetApi\Model\AccountDto accountGetByaccountCd($account_cd)

Get specific Account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\AccountApi();
$account_cd = "account_cd_example"; // string | Identifies the account

try {
    $result = $api_instance->accountGetByaccountCd($account_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->accountGetByaccountCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_cd** | **string**| Identifies the account |

### Return type

[**\Trollweb\VismaNetApi\Model\AccountDto**](../Model/AccountDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

