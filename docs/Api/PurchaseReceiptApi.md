# Trollweb\VismaNetApi\PurchaseReceiptApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseReceiptGetAllReceiptBasic**](PurchaseReceiptApi.md#purchaseReceiptGetAllReceiptBasic) | **GET** /controller/api/v1/PurchaseReceipt | 
[**purchaseReceiptGetPurchaseReceiptBasicByreceiptNbr**](PurchaseReceiptApi.md#purchaseReceiptGetPurchaseReceiptBasicByreceiptNbr) | **GET** /controller/api/v1/PurchaseReceipt/{receiptNbr} | Get a specific Purchase Receipt
[**purchaseReceiptPost**](PurchaseReceiptApi.md#purchaseReceiptPost) | **POST** /controller/api/v1/PurchaseReceipt | Create a Purchase Receipt
[**purchaseReceiptPutBypoReceiptNumber**](PurchaseReceiptApi.md#purchaseReceiptPutBypoReceiptNumber) | **PUT** /controller/api/v1/PurchaseReceipt/{poReceiptNumber} | Update a specific Purchase Receipt
[**purchaseReceiptReleaseInvoiceByreceiptNumber**](PurchaseReceiptApi.md#purchaseReceiptReleaseInvoiceByreceiptNumber) | **POST** /controller/api/v1/PurchaseReceipt/{receiptNumber}/action/release | Release purchase receipt operation


# **purchaseReceiptGetAllReceiptBasic**
> \Trollweb\VismaNetApi\Model\PurchaseReceiptDto[] purchaseReceiptGetAllReceiptBasic($receipt_type, $greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptApi();
$receipt_type = "receipt_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->purchaseReceiptGetAllReceiptBasic($receipt_type, $greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptApi->purchaseReceiptGetAllReceiptBasic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseReceiptDto[]**](../Model/PurchaseReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptGetPurchaseReceiptBasicByreceiptNbr**
> \Trollweb\VismaNetApi\Model\PurchaseReceiptDto purchaseReceiptGetPurchaseReceiptBasicByreceiptNbr($receipt_nbr)

Get a specific Purchase Receipt

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptApi();
$receipt_nbr = "receipt_nbr_example"; // string | Identifies the Purchase Receipt

try {
    $result = $api_instance->purchaseReceiptGetPurchaseReceiptBasicByreceiptNbr($receipt_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptApi->purchaseReceiptGetPurchaseReceiptBasicByreceiptNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_nbr** | **string**| Identifies the Purchase Receipt |

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseReceiptDto**](../Model/PurchaseReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptPost**
> \Trollweb\VismaNetApi\Model\Object purchaseReceiptPost($po_receipt_update_dto)

Create a Purchase Receipt

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptApi();
$po_receipt_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto | Defines the data for the  Purchase Receipt to create

try {
    $result = $api_instance->purchaseReceiptPost($po_receipt_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptApi->purchaseReceiptPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **po_receipt_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto.md)| Defines the data for the  Purchase Receipt to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptPutBypoReceiptNumber**
> \Trollweb\VismaNetApi\Model\Object purchaseReceiptPutBypoReceiptNumber($po_receipt_number, $po_receipt_update_dto)

Update a specific Purchase Receipt

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptApi();
$po_receipt_number = "po_receipt_number_example"; // string | Identifies the  Purchase Receipt  to update
$po_receipt_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto | Defines the data for the  Purchase Receipt  to update

try {
    $result = $api_instance->purchaseReceiptPutBypoReceiptNumber($po_receipt_number, $po_receipt_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptApi->purchaseReceiptPutBypoReceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **po_receipt_number** | **string**| Identifies the  Purchase Receipt  to update |
 **po_receipt_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseReceiptUpdateDto.md)| Defines the data for the  Purchase Receipt  to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptReleaseInvoiceByreceiptNumber**
> \Trollweb\VismaNetApi\Model\ReleasePurchaseReceiptActionResultDto purchaseReceiptReleaseInvoiceByreceiptNumber($receipt_number)

Release purchase receipt operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptApi();
$receipt_number = "receipt_number_example"; // string | Reference number of the receipt to be released

try {
    $result = $api_instance->purchaseReceiptReleaseInvoiceByreceiptNumber($receipt_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptApi->purchaseReceiptReleaseInvoiceByreceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_number** | **string**| Reference number of the receipt to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleasePurchaseReceiptActionResultDto**](../Model/ReleasePurchaseReceiptActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

