# Trollweb\VismaNetApi\TokenResourceApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**requestToken**](TokenResourceApi.md#requestToken) | **POST** /security/api/v2/token | Obtain access token


# **requestToken**
> \Trollweb\VismaNetApi\Model\VniToken requestToken($grant_type, $username, $password, $authorization, $client_id, $client_secret)

Obtain access token



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Trollweb\VismaNetApi\Api\TokenResourceApi();
$grant_type = "grant_type_example"; // string | Grant type
$username = "username_example"; // string | Visma.net user email
$password = "password_example"; // string | Visma.net password
$authorization = "authorization_example"; // string | HTTP Basic authentication, the preffered way to authenticate clients
$client_id = "client_id_example"; // string | Client identifier. Mandatory to be set when no other HTTP authentication was used. For instance, a client can also use Authorization header to pass HTTP Basic authentication details
$client_secret = "client_secret_example"; // string | Client secret. Mandatory to be set only when no other HTTP authentication was set.

try {
    $result = $api_instance->requestToken($grant_type, $username, $password, $authorization, $client_id, $client_secret);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenResourceApi->requestToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grant_type** | **string**| Grant type |
 **username** | **string**| Visma.net user email |
 **password** | **string**| Visma.net password |
 **authorization** | **string**| HTTP Basic authentication, the preffered way to authenticate clients | [optional]
 **client_id** | **string**| Client identifier. Mandatory to be set when no other HTTP authentication was used. For instance, a client can also use Authorization header to pass HTTP Basic authentication details | [optional]
 **client_secret** | **string**| Client secret. Mandatory to be set only when no other HTTP authentication was set. | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\VniToken**](../Model/VniToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

