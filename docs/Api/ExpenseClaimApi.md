# Trollweb\VismaNetApi\ExpenseClaimApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**expenseClaimCreateExpenseClaim**](ExpenseClaimApi.md#expenseClaimCreateExpenseClaim) | **POST** /controller/api/v1/expenseClaim | Create an ExpenseClaim
[**expenseClaimGetAll**](ExpenseClaimApi.md#expenseClaimGetAll) | **GET** /controller/api/v1/expenseClaim | Get a range of Expense Claims, a filter needs to be specified
[**expenseClaimGetExpenseClaimByexpenseClaimCd**](ExpenseClaimApi.md#expenseClaimGetExpenseClaimByexpenseClaimCd) | **GET** /controller/api/v1/expenseClaim/{expenseClaimCd} | Get a specific Expense Claim
[**expenseClaimPutByexpenseClaimNbr**](ExpenseClaimApi.md#expenseClaimPutByexpenseClaimNbr) | **PUT** /controller/api/v1/expenseClaim/{expenseClaimNbr} | Update a specific ExpenseClaim


# **expenseClaimCreateExpenseClaim**
> \Trollweb\VismaNetApi\Model\Object expenseClaimCreateExpenseClaim($expense_claim)

Create an ExpenseClaim

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseClaimApi();
$expense_claim = new \Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto(); // \Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto | Defines the data for the ExpenseClaim to create

try {
    $result = $api_instance->expenseClaimCreateExpenseClaim($expense_claim);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseClaimApi->expenseClaimCreateExpenseClaim: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expense_claim** | [**\Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto.md)| Defines the data for the ExpenseClaim to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expenseClaimGetAll**
> \Trollweb\VismaNetApi\Model\ExpenseClaimDto[] expenseClaimGetAll($status, $date, $customer, $department_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Expense Claims, a filter needs to be specified

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseClaimApi();
$status = "status_example"; // string | 
$date = new \DateTime(); // \DateTime | 
$customer = "customer_example"; // string | 
$department_id = "department_id_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->expenseClaimGetAll($status, $date, $customer, $department_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseClaimApi->expenseClaimGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **string**|  | [optional]
 **date** | **\DateTime**|  | [optional]
 **customer** | **string**|  | [optional]
 **department_id** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ExpenseClaimDto[]**](../Model/ExpenseClaimDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expenseClaimGetExpenseClaimByexpenseClaimCd**
> \Trollweb\VismaNetApi\Model\ExpenseClaimDto expenseClaimGetExpenseClaimByexpenseClaimCd($expense_claim_cd)

Get a specific Expense Claim

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseClaimApi();
$expense_claim_cd = "expense_claim_cd_example"; // string | Identifies the expense claim

try {
    $result = $api_instance->expenseClaimGetExpenseClaimByexpenseClaimCd($expense_claim_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseClaimApi->expenseClaimGetExpenseClaimByexpenseClaimCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expense_claim_cd** | **string**| Identifies the expense claim |

### Return type

[**\Trollweb\VismaNetApi\Model\ExpenseClaimDto**](../Model/ExpenseClaimDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expenseClaimPutByexpenseClaimNbr**
> \Trollweb\VismaNetApi\Model\Object expenseClaimPutByexpenseClaimNbr($expense_claim_nbr, $expense_claim)

Update a specific ExpenseClaim

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseClaimApi();
$expense_claim_nbr = "expense_claim_nbr_example"; // string | Identifies the ExpenseClaim to update
$expense_claim = new \Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto(); // \Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto | Defines the data for the ExpenseClaim to update

try {
    $result = $api_instance->expenseClaimPutByexpenseClaimNbr($expense_claim_nbr, $expense_claim);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseClaimApi->expenseClaimPutByexpenseClaimNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expense_claim_nbr** | **string**| Identifies the ExpenseClaim to update |
 **expense_claim** | [**\Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ExpenseClaimUpdateDto.md)| Defines the data for the ExpenseClaim to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

