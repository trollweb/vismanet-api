# Trollweb\VismaNetApi\InventoryReceiptApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryReceiptGetAll**](InventoryReceiptApi.md#inventoryReceiptGetAll) | **GET** /controller/api/v1/inventoryReceipt | Get a range of Inventory Receipts
[**inventoryReceiptGetByinventoryReceiptNumber**](InventoryReceiptApi.md#inventoryReceiptGetByinventoryReceiptNumber) | **GET** /controller/api/v1/inventoryReceipt/{inventoryReceiptNumber} | Get a specific Inventory Receipt document
[**inventoryReceiptPost**](InventoryReceiptApi.md#inventoryReceiptPost) | **POST** /controller/api/v1/inventoryReceipt | Create an inventory item
[**inventoryReceiptPutByreceiptRefNumber**](InventoryReceiptApi.md#inventoryReceiptPutByreceiptRefNumber) | **PUT** /controller/api/v1/inventoryReceipt/{receiptRefNumber} | Update a specific inventory item
[**inventoryReceiptReleaseDocumentByinvoiceNumber**](InventoryReceiptApi.md#inventoryReceiptReleaseDocumentByinvoiceNumber) | **POST** /controller/api/v1/inventoryReceipt/{invoiceNumber}/action/release | Release inventory operation


# **inventoryReceiptGetAll**
> \Trollweb\VismaNetApi\Model\InventoryReceiptDto[] inventoryReceiptGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Inventory Receipts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryReceiptApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->inventoryReceiptGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryReceiptApi->inventoryReceiptGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryReceiptDto[]**](../Model/InventoryReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryReceiptGetByinventoryReceiptNumber**
> \Trollweb\VismaNetApi\Model\InventoryReceiptDto inventoryReceiptGetByinventoryReceiptNumber($inventory_receipt_number)

Get a specific Inventory Receipt document

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryReceiptApi();
$inventory_receipt_number = "inventory_receipt_number_example"; // string | Identifies the Inventory Issue document

try {
    $result = $api_instance->inventoryReceiptGetByinventoryReceiptNumber($inventory_receipt_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryReceiptApi->inventoryReceiptGetByinventoryReceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_receipt_number** | **string**| Identifies the Inventory Issue document |

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryReceiptDto**](../Model/InventoryReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryReceiptPost**
> \Trollweb\VismaNetApi\Model\Object inventoryReceiptPost($inventory_receipt)

Create an inventory item

Response Message has StatusCode Created if POST operation succed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryReceiptApi();
$inventory_receipt = new \Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto | Define the data for the inventory issue to create

try {
    $result = $api_instance->inventoryReceiptPost($inventory_receipt);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryReceiptApi->inventoryReceiptPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_receipt** | [**\Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto.md)| Define the data for the inventory issue to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryReceiptPutByreceiptRefNumber**
> \Trollweb\VismaNetApi\Model\Object inventoryReceiptPutByreceiptRefNumber($receipt_ref_number, $receipt)

Update a specific inventory item

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryReceiptApi();
$receipt_ref_number = "receipt_ref_number_example"; // string | Identifies the inventory receipt to update
$receipt = new \Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto | The data to update for inventory receipt

try {
    $result = $api_instance->inventoryReceiptPutByreceiptRefNumber($receipt_ref_number, $receipt);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryReceiptApi->inventoryReceiptPutByreceiptRefNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_ref_number** | **string**| Identifies the inventory receipt to update |
 **receipt** | [**\Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryReceiptUpdateDto.md)| The data to update for inventory receipt |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryReceiptReleaseDocumentByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto inventoryReceiptReleaseDocumentByinvoiceNumber($invoice_number)

Release inventory operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryReceiptApi();
$invoice_number = "invoice_number_example"; // string | Reference number of the released issue to be released

try {
    $result = $api_instance->inventoryReceiptReleaseDocumentByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryReceiptApi->inventoryReceiptReleaseDocumentByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Reference number of the released issue to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto**](../Model/ReleaseInventoryDocumentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

