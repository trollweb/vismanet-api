# Trollweb\VismaNetApi\CustomerDocumentApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerDocumentGetAllDocuments**](CustomerDocumentApi.md#customerDocumentGetAllDocuments) | **GET** /controller/api/v1/customerdocument | Gets a range of Customer Documents


# **customerDocumentGetAllDocuments**
> \Trollweb\VismaNetApi\Model\CustomerDocumentDto[] customerDocumentGetAllDocuments($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Gets a range of Customer Documents

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerDocumentApi();
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerDocumentGetAllDocuments($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerDocumentApi->customerDocumentGetAllDocuments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerDocumentDto[]**](../Model/CustomerDocumentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

