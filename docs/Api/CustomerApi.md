# Trollweb\VismaNetApi\CustomerApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerCreateDunningLetterActionBycustomer**](CustomerApi.md#customerCreateDunningLetterActionBycustomer) | **POST** /controller/api/v1/customer/{customer}/action/createDunningLetter | Creates dunning letters for a specific customer
[**customerGetAll**](CustomerApi.md#customerGetAll) | **GET** /controller/api/v1/customer | Get a range of customers
[**customerGetAllCashSalesForCustomerBycustomerNumber**](CustomerApi.md#customerGetAllCashSalesForCustomerBycustomerNumber) | **GET** /controller/api/v1/customer/{customerNumber}/cashSale | Get a range of cash sales for a specific customer
[**customerGetAllContactsForCustomerBycustomerCd**](CustomerApi.md#customerGetAllContactsForCustomerBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd}/contact | Get a range of Contacts of a specific customer
[**customerGetAllCustomerBalance**](CustomerApi.md#customerGetAllCustomerBalance) | **GET** /controller/api/v1/customer/balance | Get the balance for a range of customers
[**customerGetAllDocumentsForCustomerBycustomerNumber**](CustomerApi.md#customerGetAllDocumentsForCustomerBycustomerNumber) | **GET** /controller/api/v1/customer/{customerNumber}/document | Gets a range of documents for a specific customer
[**customerGetAllInvoicesForCustomerBycustomerNumber**](CustomerApi.md#customerGetAllInvoicesForCustomerBycustomerNumber) | **GET** /controller/api/v1/customer/{customerNumber}/invoice | Get a range of invoices for a specific customer
[**customerGetAllOrderForCustomerBycustomerCd**](CustomerApi.md#customerGetAllOrderForCustomerBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd}/salesorder | Get a range of SO Orders of a specific customer
[**customerGetAllSalesOrderBasicForCustomerBycustomerCd**](CustomerApi.md#customerGetAllSalesOrderBasicForCustomerBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd}/salesorderbasic | Get a range of SO Orders Basic of a specific customer
[**customerGetBycustomerCd**](CustomerApi.md#customerGetBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd} | Get a specific customer
[**customerGetCustomerBalanceBycustomerCd**](CustomerApi.md#customerGetCustomerBalanceBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd}/balance | Get a specific customer&#39;s balance
[**customerGetCustomerClasses**](CustomerApi.md#customerGetCustomerClasses) | **GET** /controller/api/v1/customer/customerClass | Get Customer Classes
[**customerGetCustomerDirectDebitBycustomerCd**](CustomerApi.md#customerGetCustomerDirectDebitBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd}/directdebit | Get direct debit information for a specific customer(only for Netherlands)
[**customerGetCustomerNoteBycustomerCd**](CustomerApi.md#customerGetCustomerNoteBycustomerCd) | **GET** /controller/api/v1/customer/{customerCd}/note | Get a specific customer&#39;s note
[**customerGetSpecificCustomerClassBycustomerClassId**](CustomerApi.md#customerGetSpecificCustomerClassBycustomerClassId) | **GET** /controller/api/v1/customer/customerClass/{customerClassId} | Get a specific customer class
[**customerPost**](CustomerApi.md#customerPost) | **POST** /controller/api/v1/customer | Creates a customer
[**customerPutBycustomerCd**](CustomerApi.md#customerPutBycustomerCd) | **PUT** /controller/api/v1/customer/{customerCd} | Updates a specific customer


# **customerCreateDunningLetterActionBycustomer**
> \Trollweb\VismaNetApi\Model\CreateDunningLetterActionResultDto customerCreateDunningLetterActionBycustomer($customer, $create_dunning_letter_action_dto)

Creates dunning letters for a specific customer

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer = "customer_example"; // string | Reference number of the customer for which the dunning letters will be created
$create_dunning_letter_action_dto = new \Trollweb\VismaNetApi\Model\CreateDunningLetterActionDto(); // \Trollweb\VismaNetApi\Model\CreateDunningLetterActionDto | Defines the data for the dunning letters to be created

try {
    $result = $api_instance->customerCreateDunningLetterActionBycustomer($customer, $create_dunning_letter_action_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerCreateDunningLetterActionBycustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | **string**| Reference number of the customer for which the dunning letters will be created |
 **create_dunning_letter_action_dto** | [**\Trollweb\VismaNetApi\Model\CreateDunningLetterActionDto**](../Model/\Trollweb\VismaNetApi\Model\CreateDunningLetterActionDto.md)| Defines the data for the dunning letters to be created |

### Return type

[**\Trollweb\VismaNetApi\Model\CreateDunningLetterActionResultDto**](../Model/CreateDunningLetterActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAll**
> \Trollweb\VismaNetApi\Model\CustomerDto[] customerGetAll($greater_than_value, $number_to_read, $skip_records, $name, $status, $corporate_id, $vat_registration_id, $email, $phone, $last_modified_date_time, $last_modified_date_time_condition, $created_date_time, $created_date_time_condition, $attributes)

Get a range of customers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$name = "name_example"; // string | 
$status = "status_example"; // string | 
$corporate_id = "corporate_id_example"; // string | 
$vat_registration_id = "vat_registration_id_example"; // string | 
$email = "email_example"; // string | 
$phone = "phone_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$created_date_time = "created_date_time_example"; // string | 
$created_date_time_condition = "created_date_time_condition_example"; // string | 
$attributes = "attributes_example"; // string | 

try {
    $result = $api_instance->customerGetAll($greater_than_value, $number_to_read, $skip_records, $name, $status, $corporate_id, $vat_registration_id, $email, $phone, $last_modified_date_time, $last_modified_date_time_condition, $created_date_time, $created_date_time_condition, $attributes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **name** | **string**|  | [optional]
 **status** | **string**|  | [optional]
 **corporate_id** | **string**|  | [optional]
 **vat_registration_id** | **string**|  | [optional]
 **email** | **string**|  | [optional]
 **phone** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **created_date_time** | **string**|  | [optional]
 **created_date_time_condition** | **string**|  | [optional]
 **attributes** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerDto[]**](../Model/CustomerDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllCashSalesForCustomerBycustomerNumber**
> \Trollweb\VismaNetApi\Model\CashSaleDto[] customerGetAllCashSalesForCustomerBycustomerNumber($customer_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of cash sales for a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_number = "customer_number_example"; // string | Identifies the customer for which to return data
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllCashSalesForCustomerBycustomerNumber($customer_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllCashSalesForCustomerBycustomerNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_number** | **string**| Identifies the customer for which to return data |
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CashSaleDto[]**](../Model/CashSaleDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllContactsForCustomerBycustomerCd**
> \Trollweb\VismaNetApi\Model\ContactDto[] customerGetAllContactsForCustomerBycustomerCd($customer_cd, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Contacts of a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllContactsForCustomerBycustomerCd($customer_cd, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllContactsForCustomerBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**|  |
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ContactDto[]**](../Model/ContactDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllCustomerBalance**
> \Trollweb\VismaNetApi\Model\CustomerBalanceDto[] customerGetAllCustomerBalance($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get the balance for a range of customers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllCustomerBalance($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllCustomerBalance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerBalanceDto[]**](../Model/CustomerBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllDocumentsForCustomerBycustomerNumber**
> \Trollweb\VismaNetApi\Model\CustomerDocumentDto[] customerGetAllDocumentsForCustomerBycustomerNumber($customer_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Gets a range of documents for a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_number = "customer_number_example"; // string | Identifies the customer for which to return data
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllDocumentsForCustomerBycustomerNumber($customer_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllDocumentsForCustomerBycustomerNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_number** | **string**| Identifies the customer for which to return data |
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerDocumentDto[]**](../Model/CustomerDocumentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllInvoicesForCustomerBycustomerNumber**
> \Trollweb\VismaNetApi\Model\CustomerInvoiceDto[] customerGetAllInvoicesForCustomerBycustomerNumber($customer_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of invoices for a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_number = "customer_number_example"; // string | Identifies the customer for which to return data
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllInvoicesForCustomerBycustomerNumber($customer_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllInvoicesForCustomerBycustomerNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_number** | **string**| Identifies the customer for which to return data |
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerInvoiceDto[]**](../Model/CustomerInvoiceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllOrderForCustomerBycustomerCd**
> \Trollweb\VismaNetApi\Model\SalesOrderDto[] customerGetAllOrderForCustomerBycustomerCd($customer_cd, $order_type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of SO Orders of a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | 
$order_type = "order_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllOrderForCustomerBycustomerCd($customer_cd, $order_type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllOrderForCustomerBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**|  |
 **order_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SalesOrderDto[]**](../Model/SalesOrderDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetAllSalesOrderBasicForCustomerBycustomerCd**
> \Trollweb\VismaNetApi\Model\SalesOrderBasicDto[] customerGetAllSalesOrderBasicForCustomerBycustomerCd($customer_cd, $order_type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of SO Orders Basic of a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | 
$order_type = "order_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerGetAllSalesOrderBasicForCustomerBycustomerCd($customer_cd, $order_type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetAllSalesOrderBasicForCustomerBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**|  |
 **order_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SalesOrderBasicDto[]**](../Model/SalesOrderBasicDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetBycustomerCd**
> \Trollweb\VismaNetApi\Model\CustomerDto customerGetBycustomerCd($customer_cd)

Get a specific customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | Identifies the customer

try {
    $result = $api_instance->customerGetBycustomerCd($customer_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**| Identifies the customer |

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerDto**](../Model/CustomerDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetCustomerBalanceBycustomerCd**
> \Trollweb\VismaNetApi\Model\CustomerBalanceDto customerGetCustomerBalanceBycustomerCd($customer_cd)

Get a specific customer's balance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | Identifies the customer for which to return data

try {
    $result = $api_instance->customerGetCustomerBalanceBycustomerCd($customer_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetCustomerBalanceBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**| Identifies the customer for which to return data |

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerBalanceDto**](../Model/CustomerBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetCustomerClasses**
> \Trollweb\VismaNetApi\Model\CustomerClassDto[] customerGetCustomerClasses()

Get Customer Classes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();

try {
    $result = $api_instance->customerGetCustomerClasses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetCustomerClasses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerClassDto[]**](../Model/CustomerClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetCustomerDirectDebitBycustomerCd**
> \Trollweb\VismaNetApi\Model\CustomerDirectDebitDto[] customerGetCustomerDirectDebitBycustomerCd($customer_cd)

Get direct debit information for a specific customer(only for Netherlands)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | Identifies the customer for which to return data

try {
    $result = $api_instance->customerGetCustomerDirectDebitBycustomerCd($customer_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetCustomerDirectDebitBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**| Identifies the customer for which to return data |

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerDirectDebitDto[]**](../Model/CustomerDirectDebitDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetCustomerNoteBycustomerCd**
> \Trollweb\VismaNetApi\Model\NoteDto customerGetCustomerNoteBycustomerCd($customer_cd)

Get a specific customer's note

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | Identifies the customer for which to return data

try {
    $result = $api_instance->customerGetCustomerNoteBycustomerCd($customer_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetCustomerNoteBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**| Identifies the customer for which to return data |

### Return type

[**\Trollweb\VismaNetApi\Model\NoteDto**](../Model/NoteDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerGetSpecificCustomerClassBycustomerClassId**
> \Trollweb\VismaNetApi\Model\CustomerClassDto customerGetSpecificCustomerClassBycustomerClassId($customer_class_id)

Get a specific customer class

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_class_id = "customer_class_id_example"; // string | Identifies the customer class

try {
    $result = $api_instance->customerGetSpecificCustomerClassBycustomerClassId($customer_class_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerGetSpecificCustomerClassBycustomerClassId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_class_id** | **string**| Identifies the customer class |

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerClassDto**](../Model/CustomerClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerPost**
> \Trollweb\VismaNetApi\Model\Object customerPost($customer)

Creates a customer

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer = new \Trollweb\VismaNetApi\Model\CustomerUpdateDto(); // \Trollweb\VismaNetApi\Model\CustomerUpdateDto | Defines the data for the customer to create

try {
    $result = $api_instance->customerPost($customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer** | [**\Trollweb\VismaNetApi\Model\CustomerUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CustomerUpdateDto.md)| Defines the data for the customer to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerPutBycustomerCd**
> \Trollweb\VismaNetApi\Model\Object customerPutBycustomerCd($customer_cd, $customer)

Updates a specific customer

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerApi();
$customer_cd = "customer_cd_example"; // string | Identifies the customer to update
$customer = new \Trollweb\VismaNetApi\Model\CustomerUpdateDto(); // \Trollweb\VismaNetApi\Model\CustomerUpdateDto | The data to update for the customer

try {
    $result = $api_instance->customerPutBycustomerCd($customer_cd, $customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerApi->customerPutBycustomerCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **customer_cd** | **string**| Identifies the customer to update |
 **customer** | [**\Trollweb\VismaNetApi\Model\CustomerUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CustomerUpdateDto.md)| The data to update for the customer |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

