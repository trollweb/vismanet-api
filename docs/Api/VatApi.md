# Trollweb\VismaNetApi\VatApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatGetAllVats**](VatApi.md#vatGetAllVats) | **GET** /controller/api/v1/vat | Get a range of Vats
[**vatGetVatByvatId**](VatApi.md#vatGetVatByvatId) | **GET** /controller/api/v1/vat/{vatId} | Get a specific Vat


# **vatGetAllVats**
> \Trollweb\VismaNetApi\Model\VatInformationDto[] vatGetAllVats($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Vats

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\VatApi();
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->vatGetAllVats($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatApi->vatGetAllVats: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\VatInformationDto[]**](../Model/VatInformationDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **vatGetVatByvatId**
> \Trollweb\VismaNetApi\Model\VatInformationDto vatGetVatByvatId($vat_id)

Get a specific Vat

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\VatApi();
$vat_id = "vat_id_example"; // string | Identifies the Vat

try {
    $result = $api_instance->vatGetVatByvatId($vat_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatApi->vatGetVatByvatId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vat_id** | **string**| Identifies the Vat |

### Return type

[**\Trollweb\VismaNetApi\Model\VatInformationDto**](../Model/VatInformationDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

