# Trollweb\VismaNetApi\ExpenseReceiptApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**expenseReceiptGetAll**](ExpenseReceiptApi.md#expenseReceiptGetAll) | **GET** /controller/api/v1/expenseReceipt | Get a range of Expense Receipts, a filter needs to be specified
[**expenseReceiptGetByreceiptNumber**](ExpenseReceiptApi.md#expenseReceiptGetByreceiptNumber) | **GET** /controller/api/v1/expenseReceipt/{receiptNumber} | Get a specific ExpenseReceipt
[**expenseReceiptPost**](ExpenseReceiptApi.md#expenseReceiptPost) | **POST** /controller/api/v1/expenseReceipt | Create a Expense Receipt
[**expenseReceiptPutByreceiptNumber**](ExpenseReceiptApi.md#expenseReceiptPutByreceiptNumber) | **PUT** /controller/api/v1/expenseReceipt/{receiptNumber} | Update a Expense Receipt


# **expenseReceiptGetAll**
> \Trollweb\VismaNetApi\Model\ExpenseReceiptDto[] expenseReceiptGetAll($date, $date_condition, $inventory, $project, $claimed_by, $project_task, $invoiceable, $status, $customer)

Get a range of Expense Receipts, a filter needs to be specified

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseReceiptApi();
$date = "date_example"; // string | 
$date_condition = "date_condition_example"; // string | 
$inventory = "inventory_example"; // string | 
$project = "project_example"; // string | 
$claimed_by = "claimed_by_example"; // string | 
$project_task = "project_task_example"; // string | 
$invoiceable = true; // bool | 
$status = "status_example"; // string | 
$customer = "customer_example"; // string | 

try {
    $result = $api_instance->expenseReceiptGetAll($date, $date_condition, $inventory, $project, $claimed_by, $project_task, $invoiceable, $status, $customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseReceiptApi->expenseReceiptGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **string**|  | [optional]
 **date_condition** | **string**|  | [optional]
 **inventory** | **string**|  | [optional]
 **project** | **string**|  | [optional]
 **claimed_by** | **string**|  | [optional]
 **project_task** | **string**|  | [optional]
 **invoiceable** | **bool**|  | [optional]
 **status** | **string**|  | [optional]
 **customer** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ExpenseReceiptDto[]**](../Model/ExpenseReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expenseReceiptGetByreceiptNumber**
> \Trollweb\VismaNetApi\Model\ExpenseReceiptDto expenseReceiptGetByreceiptNumber($receipt_number)

Get a specific ExpenseReceipt

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseReceiptApi();
$receipt_number = "receipt_number_example"; // string | Identifies the ExpenseReceipt

try {
    $result = $api_instance->expenseReceiptGetByreceiptNumber($receipt_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseReceiptApi->expenseReceiptGetByreceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_number** | **string**| Identifies the ExpenseReceipt |

### Return type

[**\Trollweb\VismaNetApi\Model\ExpenseReceiptDto**](../Model/ExpenseReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expenseReceiptPost**
> \Trollweb\VismaNetApi\Model\Object expenseReceiptPost($expense_receipt_update_dto)

Create a Expense Receipt

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseReceiptApi();
$expense_receipt_update_dto = new \Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto(); // \Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto | Defines the data for the Expense Receipt to create

try {
    $result = $api_instance->expenseReceiptPost($expense_receipt_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseReceiptApi->expenseReceiptPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **expense_receipt_update_dto** | [**\Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto.md)| Defines the data for the Expense Receipt to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **expenseReceiptPutByreceiptNumber**
> \Trollweb\VismaNetApi\Model\Object expenseReceiptPutByreceiptNumber($receipt_number, $expense_receipt_update_dto)

Update a Expense Receipt

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ExpenseReceiptApi();
$receipt_number = "receipt_number_example"; // string | 
$expense_receipt_update_dto = new \Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto(); // \Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto | 

try {
    $result = $api_instance->expenseReceiptPutByreceiptNumber($receipt_number, $expense_receipt_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExpenseReceiptApi->expenseReceiptPutByreceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_number** | **string**|  |
 **expense_receipt_update_dto** | [**\Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ExpenseReceiptUpdateDto.md)|  |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

