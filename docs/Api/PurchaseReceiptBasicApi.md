# Trollweb\VismaNetApi\PurchaseReceiptBasicApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**purchaseReceiptBasicGetAllReceiptBasic**](PurchaseReceiptBasicApi.md#purchaseReceiptBasicGetAllReceiptBasic) | **GET** /controller/api/v1/PurchaseReceiptBasic | 
[**purchaseReceiptBasicGetPurchaseReceiptBasicByreceiptNbr**](PurchaseReceiptBasicApi.md#purchaseReceiptBasicGetPurchaseReceiptBasicByreceiptNbr) | **GET** /controller/api/v1/PurchaseReceiptBasic/{receiptNbr} | Get a specific Purchase Receipt
[**purchaseReceiptBasicPost**](PurchaseReceiptBasicApi.md#purchaseReceiptBasicPost) | **POST** /controller/api/v1/PurchaseReceiptBasic | Create a Purchase Receipt
[**purchaseReceiptBasicPutBypoReceiptNumber**](PurchaseReceiptBasicApi.md#purchaseReceiptBasicPutBypoReceiptNumber) | **PUT** /controller/api/v1/PurchaseReceiptBasic/{poReceiptNumber} | Update a specific Purchase Receipt
[**purchaseReceiptBasicReleaseInvoiceByreceiptNumber**](PurchaseReceiptBasicApi.md#purchaseReceiptBasicReleaseInvoiceByreceiptNumber) | **POST** /controller/api/v1/PurchaseReceiptBasic/{receiptNumber}/action/release | Release purchase receipt operation


# **purchaseReceiptBasicGetAllReceiptBasic**
> \Trollweb\VismaNetApi\Model\PurchaseReceiptBasicDto[] purchaseReceiptBasicGetAllReceiptBasic($receipt_type, $greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptBasicApi();
$receipt_type = "receipt_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->purchaseReceiptBasicGetAllReceiptBasic($receipt_type, $greater_than_value, $number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptBasicApi->purchaseReceiptBasicGetAllReceiptBasic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseReceiptBasicDto[]**](../Model/PurchaseReceiptBasicDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptBasicGetPurchaseReceiptBasicByreceiptNbr**
> \Trollweb\VismaNetApi\Model\PurchaseReceiptBasicDto purchaseReceiptBasicGetPurchaseReceiptBasicByreceiptNbr($receipt_nbr)

Get a specific Purchase Receipt

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptBasicApi();
$receipt_nbr = "receipt_nbr_example"; // string | Identifies the Purchase Receipt

try {
    $result = $api_instance->purchaseReceiptBasicGetPurchaseReceiptBasicByreceiptNbr($receipt_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptBasicApi->purchaseReceiptBasicGetPurchaseReceiptBasicByreceiptNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_nbr** | **string**| Identifies the Purchase Receipt |

### Return type

[**\Trollweb\VismaNetApi\Model\PurchaseReceiptBasicDto**](../Model/PurchaseReceiptBasicDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptBasicPost**
> \Trollweb\VismaNetApi\Model\Object purchaseReceiptBasicPost($po_receipt_update_dto)

Create a Purchase Receipt

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptBasicApi();
$po_receipt_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto | Defines the data for the  Purchase Receipt to create

try {
    $result = $api_instance->purchaseReceiptBasicPost($po_receipt_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptBasicApi->purchaseReceiptBasicPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **po_receipt_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto.md)| Defines the data for the  Purchase Receipt to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptBasicPutBypoReceiptNumber**
> \Trollweb\VismaNetApi\Model\Object purchaseReceiptBasicPutBypoReceiptNumber($po_receipt_number, $po_receipt_update_dto)

Update a specific Purchase Receipt

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptBasicApi();
$po_receipt_number = "po_receipt_number_example"; // string | Identifies the  Purchase Receipt  to update
$po_receipt_update_dto = new \Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto(); // \Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto | Defines the data for the  Purchase Receipt  to update

try {
    $result = $api_instance->purchaseReceiptBasicPutBypoReceiptNumber($po_receipt_number, $po_receipt_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptBasicApi->purchaseReceiptBasicPutBypoReceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **po_receipt_number** | **string**| Identifies the  Purchase Receipt  to update |
 **po_receipt_update_dto** | [**\Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PurchaseReceiptBasicUpdateDto.md)| Defines the data for the  Purchase Receipt  to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **purchaseReceiptBasicReleaseInvoiceByreceiptNumber**
> \Trollweb\VismaNetApi\Model\ReleasePurchaseReceiptActionResultDto purchaseReceiptBasicReleaseInvoiceByreceiptNumber($receipt_number)

Release purchase receipt operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PurchaseReceiptBasicApi();
$receipt_number = "receipt_number_example"; // string | Reference number of the receipt to be released

try {
    $result = $api_instance->purchaseReceiptBasicReleaseInvoiceByreceiptNumber($receipt_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PurchaseReceiptBasicApi->purchaseReceiptBasicReleaseInvoiceByreceiptNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **receipt_number** | **string**| Reference number of the receipt to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleasePurchaseReceiptActionResultDto**](../Model/ReleasePurchaseReceiptActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

