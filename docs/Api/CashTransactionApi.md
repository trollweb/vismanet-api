# Trollweb\VismaNetApi\CashTransactionApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cashTransactionCreateCashTransaction**](CashTransactionApi.md#cashTransactionCreateCashTransaction) | **POST** /controller/api/v1/cashTransaction | Create a Cash Transaction
[**cashTransactionGetAll**](CashTransactionApi.md#cashTransactionGetAll) | **GET** /controller/api/v1/cashTransaction | Get all Transaction
[**cashTransactionGetByreferenceNumber**](CashTransactionApi.md#cashTransactionGetByreferenceNumber) | **GET** /controller/api/v1/cashTransaction/{referenceNumber} | Get a specific Transaction
[**cashTransactionPutBycashTransactionNumber**](CashTransactionApi.md#cashTransactionPutBycashTransactionNumber) | **PUT** /controller/api/v1/cashTransaction/{cashTransactionNumber} | Update a specific CashTransaction


# **cashTransactionCreateCashTransaction**
> \Trollweb\VismaNetApi\Model\Object cashTransactionCreateCashTransaction($cash_transaction)

Create a Cash Transaction

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashTransactionApi();
$cash_transaction = new \Trollweb\VismaNetApi\Model\CashTransactionUpdateDto(); // \Trollweb\VismaNetApi\Model\CashTransactionUpdateDto | Defines the data for the Cash Transaction to create

try {
    $result = $api_instance->cashTransactionCreateCashTransaction($cash_transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashTransactionApi->cashTransactionCreateCashTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_transaction** | [**\Trollweb\VismaNetApi\Model\CashTransactionUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CashTransactionUpdateDto.md)| Defines the data for the Cash Transaction to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashTransactionGetAll**
> \Trollweb\VismaNetApi\Model\CashTransactionDto[] cashTransactionGetAll($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition)

Get all Transaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashTransactionApi();
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->cashTransactionGetAll($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashTransactionApi->cashTransactionGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CashTransactionDto[]**](../Model/CashTransactionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashTransactionGetByreferenceNumber**
> \Trollweb\VismaNetApi\Model\CashTransactionDto cashTransactionGetByreferenceNumber($reference_number)

Get a specific Transaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashTransactionApi();
$reference_number = "reference_number_example"; // string | Identifies the Transaction

try {
    $result = $api_instance->cashTransactionGetByreferenceNumber($reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashTransactionApi->cashTransactionGetByreferenceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reference_number** | **string**| Identifies the Transaction |

### Return type

[**\Trollweb\VismaNetApi\Model\CashTransactionDto**](../Model/CashTransactionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cashTransactionPutBycashTransactionNumber**
> \Trollweb\VismaNetApi\Model\Object cashTransactionPutBycashTransactionNumber($cash_transaction_number, $cash_transaction)

Update a specific CashTransaction

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CashTransactionApi();
$cash_transaction_number = "cash_transaction_number_example"; // string | Identifies the Cash Transaction to update
$cash_transaction = new \Trollweb\VismaNetApi\Model\CashTransactionUpdateDto(); // \Trollweb\VismaNetApi\Model\CashTransactionUpdateDto | Defines the data for the Cash Transaction to update

try {
    $result = $api_instance->cashTransactionPutBycashTransactionNumber($cash_transaction_number, $cash_transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CashTransactionApi->cashTransactionPutBycashTransactionNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cash_transaction_number** | **string**| Identifies the Cash Transaction to update |
 **cash_transaction** | [**\Trollweb\VismaNetApi\Model\CashTransactionUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CashTransactionUpdateDto.md)| Defines the data for the Cash Transaction to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

