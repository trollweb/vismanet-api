# Trollweb\VismaNetApi\TimeCardApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**timeCardCreateTimeCard**](TimeCardApi.md#timeCardCreateTimeCard) | **POST** /controller/api/v1/timeCard | Creates a time card
[**timeCardGetAllEmployeeTimeCards**](TimeCardApi.md#timeCardGetAllEmployeeTimeCards) | **GET** /controller/api/v1/timeCard | Get all employee time cards, a filter needs to be specified
[**timeCardGetEmployeeTimeCardBytimeCardCd**](TimeCardApi.md#timeCardGetEmployeeTimeCardBytimeCardCd) | **GET** /controller/api/v1/timeCard/{timeCardCd} | Get a specific employee time cards
[**timeCardUpdateTimeCardByreferenceNumber**](TimeCardApi.md#timeCardUpdateTimeCardByreferenceNumber) | **PUT** /controller/api/v1/timeCard/{referenceNumber} | Updates a specific time card


# **timeCardCreateTimeCard**
> \Trollweb\VismaNetApi\Model\Object timeCardCreateTimeCard($time_card_update_dto)

Creates a time card

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\TimeCardApi();
$time_card_update_dto = new \Trollweb\VismaNetApi\Model\TimeCardUpdateDto(); // \Trollweb\VismaNetApi\Model\TimeCardUpdateDto | Defines the data for the time card to create

try {
    $result = $api_instance->timeCardCreateTimeCard($time_card_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimeCardApi->timeCardCreateTimeCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **time_card_update_dto** | [**\Trollweb\VismaNetApi\Model\TimeCardUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\TimeCardUpdateDto.md)| Defines the data for the time card to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **timeCardGetAllEmployeeTimeCards**
> \Trollweb\VismaNetApi\Model\TimeCardDto[] timeCardGetAllEmployeeTimeCards($status, $week, $type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get all employee time cards, a filter needs to be specified

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\TimeCardApi();
$status = "status_example"; // string | 
$week = "week_example"; // string | 
$type = "type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->timeCardGetAllEmployeeTimeCards($status, $week, $type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimeCardApi->timeCardGetAllEmployeeTimeCards: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **string**|  | [optional]
 **week** | **string**|  | [optional]
 **type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\TimeCardDto[]**](../Model/TimeCardDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **timeCardGetEmployeeTimeCardBytimeCardCd**
> \Trollweb\VismaNetApi\Model\TimeCardDto timeCardGetEmployeeTimeCardBytimeCardCd($time_card_cd)

Get a specific employee time cards

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\TimeCardApi();
$time_card_cd = "time_card_cd_example"; // string | Identifies the time card

try {
    $result = $api_instance->timeCardGetEmployeeTimeCardBytimeCardCd($time_card_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimeCardApi->timeCardGetEmployeeTimeCardBytimeCardCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **time_card_cd** | **string**| Identifies the time card |

### Return type

[**\Trollweb\VismaNetApi\Model\TimeCardDto**](../Model/TimeCardDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **timeCardUpdateTimeCardByreferenceNumber**
> \Trollweb\VismaNetApi\Model\Object timeCardUpdateTimeCardByreferenceNumber($reference_number, $time_card_update_dto)

Updates a specific time card

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\TimeCardApi();
$reference_number = "reference_number_example"; // string | Identifies the time card to update
$time_card_update_dto = new \Trollweb\VismaNetApi\Model\TimeCardUpdateDto(); // \Trollweb\VismaNetApi\Model\TimeCardUpdateDto | The data to update the time card with

try {
    $result = $api_instance->timeCardUpdateTimeCardByreferenceNumber($reference_number, $time_card_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TimeCardApi->timeCardUpdateTimeCardByreferenceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reference_number** | **string**| Identifies the time card to update |
 **time_card_update_dto** | [**\Trollweb\VismaNetApi\Model\TimeCardUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\TimeCardUpdateDto.md)| The data to update the time card with |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

