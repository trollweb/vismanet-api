# Trollweb\VismaNetApi\InventoryApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryGetAll**](InventoryApi.md#inventoryGetAll) | **GET** /controller/api/v1/inventory | Get a range of Inventory items
[**inventoryGetByinventoryNumber**](InventoryApi.md#inventoryGetByinventoryNumber) | **GET** /controller/api/v1/inventory/{inventoryNumber} | Get a specific Inventory item
[**inventoryGetItemClasses**](InventoryApi.md#inventoryGetItemClasses) | **GET** /controller/api/v1/inventory/itemClass | Get Inventory Item Classes
[**inventoryGetItemPostClasses**](InventoryApi.md#inventoryGetItemPostClasses) | **GET** /controller/api/v1/inventory/itemPostClass | Get Inventory Item Post Classes
[**inventoryGetSpecificItemClassByitemClassNumber**](InventoryApi.md#inventoryGetSpecificItemClassByitemClassNumber) | **GET** /controller/api/v1/inventory/itemclass/{itemClassNumber} | Get Specific Inventory Item Class
[**inventoryPost**](InventoryApi.md#inventoryPost) | **POST** /controller/api/v1/inventory | Create an inventory item
[**inventoryPutByinventoryCd**](InventoryApi.md#inventoryPutByinventoryCd) | **PUT** /controller/api/v1/inventory/{inventoryCd} | Update a specific inventory item


# **inventoryGetAll**
> \Trollweb\VismaNetApi\Model\InventoryDto[] inventoryGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $alternate_id, $sales_category, $attributes)

Get a range of Inventory items

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$alternate_id = "alternate_id_example"; // string | 
$sales_category = 56; // int | 
$attributes = "attributes_example"; // string | 

try {
    $result = $api_instance->inventoryGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $alternate_id, $sales_category, $attributes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **alternate_id** | **string**|  | [optional]
 **sales_category** | **int**|  | [optional]
 **attributes** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryDto[]**](../Model/InventoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryGetByinventoryNumber**
> \Trollweb\VismaNetApi\Model\InventoryDto inventoryGetByinventoryNumber($inventory_number)

Get a specific Inventory item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();
$inventory_number = "inventory_number_example"; // string | Identifies the Inventory item

try {
    $result = $api_instance->inventoryGetByinventoryNumber($inventory_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryGetByinventoryNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_number** | **string**| Identifies the Inventory item |

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryDto**](../Model/InventoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryGetItemClasses**
> \Trollweb\VismaNetApi\Model\ItemClassDto[] inventoryGetItemClasses()

Get Inventory Item Classes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();

try {
    $result = $api_instance->inventoryGetItemClasses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryGetItemClasses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\ItemClassDto[]**](../Model/ItemClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryGetItemPostClasses**
> \Trollweb\VismaNetApi\Model\PostingClassDto[] inventoryGetItemPostClasses()

Get Inventory Item Post Classes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();

try {
    $result = $api_instance->inventoryGetItemPostClasses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryGetItemPostClasses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\PostingClassDto[]**](../Model/PostingClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryGetSpecificItemClassByitemClassNumber**
> \Trollweb\VismaNetApi\Model\ItemClassDto inventoryGetSpecificItemClassByitemClassNumber($item_class_number)

Get Specific Inventory Item Class

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();
$item_class_number = "item_class_number_example"; // string | 

try {
    $result = $api_instance->inventoryGetSpecificItemClassByitemClassNumber($item_class_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryGetSpecificItemClassByitemClassNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_class_number** | **string**|  |

### Return type

[**\Trollweb\VismaNetApi\Model\ItemClassDto**](../Model/ItemClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryPost**
> \Trollweb\VismaNetApi\Model\Object inventoryPost($inventory)

Create an inventory item

Response Message has StatusCode Created if POST operation succed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();
$inventory = new \Trollweb\VismaNetApi\Model\InventoryUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryUpdateDto | Define the data for the inventory item to create

try {
    $result = $api_instance->inventoryPost($inventory);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory** | [**\Trollweb\VismaNetApi\Model\InventoryUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryUpdateDto.md)| Define the data for the inventory item to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryPutByinventoryCd**
> \Trollweb\VismaNetApi\Model\Object inventoryPutByinventoryCd($inventory_cd, $inventory)

Update a specific inventory item

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryApi();
$inventory_cd = "inventory_cd_example"; // string | Identifies the inventory item to update
$inventory = new \Trollweb\VismaNetApi\Model\InventoryUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryUpdateDto | The data to update for inventory item

try {
    $result = $api_instance->inventoryPutByinventoryCd($inventory_cd, $inventory);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryApi->inventoryPutByinventoryCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_cd** | **string**| Identifies the inventory item to update |
 **inventory** | [**\Trollweb\VismaNetApi\Model\InventoryUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryUpdateDto.md)| The data to update for inventory item |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

