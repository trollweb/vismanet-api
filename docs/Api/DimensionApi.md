# Trollweb\VismaNetApi\DimensionApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dimensionGetDimensionBydimensionId**](DimensionApi.md#dimensionGetDimensionBydimensionId) | **GET** /controller/api/v1/dimension/{dimensionId} | Get a specific Dimension
[**dimensionGetDimensionList**](DimensionApi.md#dimensionGetDimensionList) | **GET** /controller/api/v1/dimension | Get a list of all Dimension names/IDs
[**dimensionGetSegmentBydimensionIdsegmentId**](DimensionApi.md#dimensionGetSegmentBydimensionIdsegmentId) | **GET** /controller/api/v1/dimension/{dimensionId}/{segmentId} | Get a specific Segment for a specific Dimension
[**dimensionGetSegmentValueBydimensionIdsegmentIdpublicId**](DimensionApi.md#dimensionGetSegmentValueBydimensionIdsegmentIdpublicId) | **GET** /controller/api/v1/dimension/{dimensionId}/{segmentId}/publicid/{publicId} | Get a specific SegmentValue for a specific Segment for a specific Dimension
[**dimensionGetSegmentValueBydimensionIdsegmentIdvalueId**](DimensionApi.md#dimensionGetSegmentValueBydimensionIdsegmentIdvalueId) | **GET** /controller/api/v1/dimension/{dimensionId}/{segmentId}/{valueId} | Get a specific SegmentValue for a specific Segment for a specific Dimension
[**dimensionUpdateSegmentBydimensionIdsegmentId**](DimensionApi.md#dimensionUpdateSegmentBydimensionIdsegmentId) | **PUT** /controller/api/v1/dimension/{dimensionId}/{segmentId} | Update a Segment


# **dimensionGetDimensionBydimensionId**
> \Trollweb\VismaNetApi\Model\DtoDimension dimensionGetDimensionBydimensionId($dimension_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a specific Dimension

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DimensionApi();
$dimension_id = "dimension_id_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->dimensionGetDimensionBydimensionId($dimension_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DimensionApi->dimensionGetDimensionBydimensionId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dimension_id** | **string**|  |
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\DtoDimension**](../Model/DtoDimension.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dimensionGetDimensionList**
> string[] dimensionGetDimensionList()

Get a list of all Dimension names/IDs

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DimensionApi();

try {
    $result = $api_instance->dimensionGetDimensionList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DimensionApi->dimensionGetDimensionList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string[]**

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dimensionGetSegmentBydimensionIdsegmentId**
> \Trollweb\VismaNetApi\Model\DtoSegment dimensionGetSegmentBydimensionIdsegmentId($dimension_id, $segment_id)

Get a specific Segment for a specific Dimension

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DimensionApi();
$dimension_id = "dimension_id_example"; // string | Identifies the Dimension
$segment_id = 56; // int | Identifies the Segment

try {
    $result = $api_instance->dimensionGetSegmentBydimensionIdsegmentId($dimension_id, $segment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DimensionApi->dimensionGetSegmentBydimensionIdsegmentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dimension_id** | **string**| Identifies the Dimension |
 **segment_id** | **int**| Identifies the Segment |

### Return type

[**\Trollweb\VismaNetApi\Model\DtoSegment**](../Model/DtoSegment.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dimensionGetSegmentValueBydimensionIdsegmentIdpublicId**
> \Trollweb\VismaNetApi\Model\DtoSegmentValue dimensionGetSegmentValueBydimensionIdsegmentIdpublicId($dimension_id, $segment_id, $public_id)

Get a specific SegmentValue for a specific Segment for a specific Dimension

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DimensionApi();
$dimension_id = "dimension_id_example"; // string | Identifies the Dimension
$segment_id = 56; // int | Identifies the Segment
$public_id = "public_id_example"; // string | Identifies the SegmentValue by its publicId

try {
    $result = $api_instance->dimensionGetSegmentValueBydimensionIdsegmentIdpublicId($dimension_id, $segment_id, $public_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DimensionApi->dimensionGetSegmentValueBydimensionIdsegmentIdpublicId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dimension_id** | **string**| Identifies the Dimension |
 **segment_id** | **int**| Identifies the Segment |
 **public_id** | **string**| Identifies the SegmentValue by its publicId |

### Return type

[**\Trollweb\VismaNetApi\Model\DtoSegmentValue**](../Model/DtoSegmentValue.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dimensionGetSegmentValueBydimensionIdsegmentIdvalueId**
> \Trollweb\VismaNetApi\Model\DtoSegmentValue dimensionGetSegmentValueBydimensionIdsegmentIdvalueId($dimension_id, $segment_id, $value_id)

Get a specific SegmentValue for a specific Segment for a specific Dimension

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DimensionApi();
$dimension_id = "dimension_id_example"; // string | Identifies the Dimension
$segment_id = 56; // int | Identifies the Segment
$value_id = "value_id_example"; // string | Identifies the SegmentValue by Id. Must be Base64 encoded.

try {
    $result = $api_instance->dimensionGetSegmentValueBydimensionIdsegmentIdvalueId($dimension_id, $segment_id, $value_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DimensionApi->dimensionGetSegmentValueBydimensionIdsegmentIdvalueId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dimension_id** | **string**| Identifies the Dimension |
 **segment_id** | **int**| Identifies the Segment |
 **value_id** | **string**| Identifies the SegmentValue by Id. Must be Base64 encoded. |

### Return type

[**\Trollweb\VismaNetApi\Model\DtoSegmentValue**](../Model/DtoSegmentValue.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **dimensionUpdateSegmentBydimensionIdsegmentId**
> \Trollweb\VismaNetApi\Model\Object dimensionUpdateSegmentBydimensionIdsegmentId($dimension_id, $segment_id, $update)

Update a Segment

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DimensionApi();
$dimension_id = "dimension_id_example"; // string | Identifies the Dimension for which the Segment is defined
$segment_id = 56; // int | Identifies the Segment
$update = new \Trollweb\VismaNetApi\Model\DtoSegmentUpdateDto(); // \Trollweb\VismaNetApi\Model\DtoSegmentUpdateDto | Defines the data for the Segment to update

try {
    $result = $api_instance->dimensionUpdateSegmentBydimensionIdsegmentId($dimension_id, $segment_id, $update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DimensionApi->dimensionUpdateSegmentBydimensionIdsegmentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dimension_id** | **string**| Identifies the Dimension for which the Segment is defined |
 **segment_id** | **int**| Identifies the Segment |
 **update** | [**\Trollweb\VismaNetApi\Model\DtoSegmentUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\DtoSegmentUpdateDto.md)| Defines the data for the Segment to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

