# Trollweb\VismaNetApi\CustomerSalesPriceApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerSalesPriceGetCustomerSalesPrices**](CustomerSalesPriceApi.md#customerSalesPriceGetCustomerSalesPrices) | **GET** /controller/api/v1/customerSalesPrice | Get a range of Customer Sales Prices


# **customerSalesPriceGetCustomerSalesPrices**
> \Trollweb\VismaNetApi\Model\CustomerSalesPriceDto[] customerSalesPriceGetCustomerSalesPrices($greater_than_value, $number_to_read, $skip_records, $price_type, $price_code, $inventory_id, $effective_as_of, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Customer Sales Prices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerSalesPriceApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$price_type = "price_type_example"; // string | 
$price_code = "price_code_example"; // string | 
$inventory_id = "inventory_id_example"; // string | 
$effective_as_of = new \DateTime(); // \DateTime | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerSalesPriceGetCustomerSalesPrices($greater_than_value, $number_to_read, $skip_records, $price_type, $price_code, $inventory_id, $effective_as_of, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerSalesPriceApi->customerSalesPriceGetCustomerSalesPrices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **price_type** | **string**|  | [optional]
 **price_code** | **string**|  | [optional]
 **inventory_id** | **string**|  | [optional]
 **effective_as_of** | **\DateTime**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerSalesPriceDto[]**](../Model/CustomerSalesPriceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

