# Trollweb\VismaNetApi\AttachmentApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attachmentGetByattachmentId**](AttachmentApi.md#attachmentGetByattachmentId) | **GET** /controller/api/v1/attachment/{attachmentId} | Get latest revision of a specific Attachment
[**attachmentPutByattachmentId**](AttachmentApi.md#attachmentPutByattachmentId) | **PUT** /controller/api/v1/attachment/{attachmentId} | Update a specific Attachment


# **attachmentGetByattachmentId**
> \Trollweb\VismaNetApi\Model\Object attachmentGetByattachmentId($attachment_id)

Get latest revision of a specific Attachment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\AttachmentApi();
$attachment_id = "attachment_id_example"; // string | Identifies the attachment

try {
    $result = $api_instance->attachmentGetByattachmentId($attachment_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentApi->attachmentGetByattachmentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_id** | **string**| Identifies the attachment |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **attachmentPutByattachmentId**
> \Trollweb\VismaNetApi\Model\Object attachmentPutByattachmentId($attachment_id, $metadata_dto)

Update a specific Attachment

The the Response Message has StatusCode NoContent if Put operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\AttachmentApi();
$attachment_id = "attachment_id_example"; // string | Identifies the Attachment to update
$metadata_dto = new \Trollweb\VismaNetApi\Model\AttachmentMetadataUpdateDto(); // \Trollweb\VismaNetApi\Model\AttachmentMetadataUpdateDto | Defines the data for the Attachment to update

try {
    $result = $api_instance->attachmentPutByattachmentId($attachment_id, $metadata_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentApi->attachmentPutByattachmentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachment_id** | **string**| Identifies the Attachment to update |
 **metadata_dto** | [**\Trollweb\VismaNetApi\Model\AttachmentMetadataUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\AttachmentMetadataUpdateDto.md)| Defines the data for the Attachment to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

