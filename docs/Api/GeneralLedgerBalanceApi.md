# Trollweb\VismaNetApi\GeneralLedgerBalanceApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generalLedgerBalanceGetAll**](GeneralLedgerBalanceApi.md#generalLedgerBalanceGetAll) | **GET** /controller/api/v1/generalLedgerBalance | Get a range of General Ledger Balances


# **generalLedgerBalanceGetAll**
> \Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[] generalLedgerBalanceGetAll($greater_than_value, $number_to_read, $skip_records, $period_id, $period_id_condition, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $account_id, $year_closed, $balance_type)

Get a range of General Ledger Balances

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\GeneralLedgerBalanceApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$period_id = "period_id_example"; // string | 
$period_id_condition = "period_id_condition_example"; // string | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$account_id = "account_id_example"; // string | 
$year_closed = 56; // int | 
$balance_type = "balance_type_example"; // string | 

try {
    $result = $api_instance->generalLedgerBalanceGetAll($greater_than_value, $number_to_read, $skip_records, $period_id, $period_id_condition, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $account_id, $year_closed, $balance_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeneralLedgerBalanceApi->generalLedgerBalanceGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **period_id** | **string**|  | [optional]
 **period_id_condition** | **string**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **account_id** | **string**|  | [optional]
 **year_closed** | **int**|  | [optional]
 **balance_type** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[]**](../Model/GeneralLedgerBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

