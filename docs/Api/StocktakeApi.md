# Trollweb\VismaNetApi\StocktakeApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stocktakeGetAllStocktakes**](StocktakeApi.md#stocktakeGetAllStocktakes) | **GET** /controller/api/v1/stocktake | Get a range of stocktakes
[**stocktakeGetByreferenceNumber**](StocktakeApi.md#stocktakeGetByreferenceNumber) | **GET** /controller/api/v1/stocktake/{referenceNumber} | Get a specific
[**stocktakePutByreferenceNumber**](StocktakeApi.md#stocktakePutByreferenceNumber) | **PUT** /controller/api/v1/stocktake/{referenceNumber} | Update a specific stocktake


# **stocktakeGetAllStocktakes**
> \Trollweb\VismaNetApi\Model\StocktakeDto[] stocktakeGetAllStocktakes($warehouse, $location, $inventory, $lot_serial_number, $summary_status, $number_to_read, $start_with_line, $end_with_line, $freeze_date_time, $freeze_date_time_condition, $last_modified_date_time, $last_modified_date_time_condition, $expiration_date_time, $expiration_date_time_condition, $status, $skip_records)

Get a range of stocktakes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\StocktakeApi();
$warehouse = "warehouse_example"; // string | 
$location = "location_example"; // string | 
$inventory = "inventory_example"; // string | 
$lot_serial_number = "lot_serial_number_example"; // string | 
$summary_status = "summary_status_example"; // string | 
$number_to_read = 56; // int | 
$start_with_line = 56; // int | 
$end_with_line = 56; // int | 
$freeze_date_time = "freeze_date_time_example"; // string | 
$freeze_date_time_condition = "freeze_date_time_condition_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$expiration_date_time = "expiration_date_time_example"; // string | 
$expiration_date_time_condition = "expiration_date_time_condition_example"; // string | 
$status = "status_example"; // string | 
$skip_records = 56; // int | 

try {
    $result = $api_instance->stocktakeGetAllStocktakes($warehouse, $location, $inventory, $lot_serial_number, $summary_status, $number_to_read, $start_with_line, $end_with_line, $freeze_date_time, $freeze_date_time_condition, $last_modified_date_time, $last_modified_date_time_condition, $expiration_date_time, $expiration_date_time_condition, $status, $skip_records);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocktakeApi->stocktakeGetAllStocktakes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **warehouse** | **string**|  | [optional]
 **location** | **string**|  | [optional]
 **inventory** | **string**|  | [optional]
 **lot_serial_number** | **string**|  | [optional]
 **summary_status** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **start_with_line** | **int**|  | [optional]
 **end_with_line** | **int**|  | [optional]
 **freeze_date_time** | **string**|  | [optional]
 **freeze_date_time_condition** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **expiration_date_time** | **string**|  | [optional]
 **expiration_date_time_condition** | **string**|  | [optional]
 **status** | **string**|  | [optional]
 **skip_records** | **int**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\StocktakeDto[]**](../Model/StocktakeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stocktakeGetByreferenceNumber**
> \Trollweb\VismaNetApi\Model\StocktakeDto stocktakeGetByreferenceNumber($reference_number)

Get a specific

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\StocktakeApi();
$reference_number = "reference_number_example"; // string | Identifies the Stocktake

try {
    $result = $api_instance->stocktakeGetByreferenceNumber($reference_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocktakeApi->stocktakeGetByreferenceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reference_number** | **string**| Identifies the Stocktake |

### Return type

[**\Trollweb\VismaNetApi\Model\StocktakeDto**](../Model/StocktakeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **stocktakePutByreferenceNumber**
> \Trollweb\VismaNetApi\Model\Object stocktakePutByreferenceNumber($reference_number, $stocktake)

Update a specific stocktake

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\StocktakeApi();
$reference_number = "reference_number_example"; // string | Identifies the stocktake to update
$stocktake = new \Trollweb\VismaNetApi\Model\StocktakeUpdateDto(); // \Trollweb\VismaNetApi\Model\StocktakeUpdateDto | The data to update for stocktake

try {
    $result = $api_instance->stocktakePutByreferenceNumber($reference_number, $stocktake);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StocktakeApi->stocktakePutByreferenceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reference_number** | **string**| Identifies the stocktake to update |
 **stocktake** | [**\Trollweb\VismaNetApi\Model\StocktakeUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\StocktakeUpdateDto.md)| The data to update for stocktake |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

