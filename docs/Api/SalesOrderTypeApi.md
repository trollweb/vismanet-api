# Trollweb\VismaNetApi\SalesOrderTypeApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesOrderTypeGetAllSalesOrderTypes**](SalesOrderTypeApi.md#salesOrderTypeGetAllSalesOrderTypes) | **GET** /controller/api/v1/salesordertype | Get a range of SO OrderTypes
[**salesOrderTypeGetSalesOrderTypeByorderType**](SalesOrderTypeApi.md#salesOrderTypeGetSalesOrderTypeByorderType) | **GET** /controller/api/v1/salesordertype/{orderType} | Get a specific SO OrderType


# **salesOrderTypeGetAllSalesOrderTypes**
> \Trollweb\VismaNetApi\Model\SalesOrderTypeDto[] salesOrderTypeGetAllSalesOrderTypes($number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of SO OrderTypes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderTypeApi();
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->salesOrderTypeGetAllSalesOrderTypes($number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderTypeApi->salesOrderTypeGetAllSalesOrderTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SalesOrderTypeDto[]**](../Model/SalesOrderTypeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderTypeGetSalesOrderTypeByorderType**
> \Trollweb\VismaNetApi\Model\SalesOrderTypeDto salesOrderTypeGetSalesOrderTypeByorderType($order_type)

Get a specific SO OrderType

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderTypeApi();
$order_type = "order_type_example"; // string | Identifies the SO OrderType

try {
    $result = $api_instance->salesOrderTypeGetSalesOrderTypeByorderType($order_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderTypeApi->salesOrderTypeGetSalesOrderTypeByorderType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_type** | **string**| Identifies the SO OrderType |

### Return type

[**\Trollweb\VismaNetApi\Model\SalesOrderTypeDto**](../Model/SalesOrderTypeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

