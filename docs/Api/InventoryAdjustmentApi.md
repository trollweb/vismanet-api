# Trollweb\VismaNetApi\InventoryAdjustmentApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryAdjustmentGetAll**](InventoryAdjustmentApi.md#inventoryAdjustmentGetAll) | **GET** /controller/api/v1/inventoryadjustment | Get a range of Inventory Adjustment Dto
[**inventoryAdjustmentGetByinventoryAdjustmentNumber**](InventoryAdjustmentApi.md#inventoryAdjustmentGetByinventoryAdjustmentNumber) | **GET** /controller/api/v1/inventoryadjustment/{inventoryAdjustmentNumber} | Get a specific Inventory Adjustment document
[**inventoryAdjustmentPost**](InventoryAdjustmentApi.md#inventoryAdjustmentPost) | **POST** /controller/api/v1/inventoryadjustment | Create an inventory item
[**inventoryAdjustmentPutByadjRefNumber**](InventoryAdjustmentApi.md#inventoryAdjustmentPutByadjRefNumber) | **PUT** /controller/api/v1/inventoryadjustment/{adjRefNumber} | Update a specific inventory item
[**inventoryAdjustmentReleaseDocumentByadjRefNumber**](InventoryAdjustmentApi.md#inventoryAdjustmentReleaseDocumentByadjRefNumber) | **POST** /controller/api/v1/inventoryadjustment/{adjRefNumber}/action/release | Release inventory document action


# **inventoryAdjustmentGetAll**
> \Trollweb\VismaNetApi\Model\InventoryAdjustmentDto[] inventoryAdjustmentGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Inventory Adjustment Dto

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryAdjustmentApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->inventoryAdjustmentGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryAdjustmentApi->inventoryAdjustmentGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryAdjustmentDto[]**](../Model/InventoryAdjustmentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryAdjustmentGetByinventoryAdjustmentNumber**
> \Trollweb\VismaNetApi\Model\InventoryAdjustmentDto inventoryAdjustmentGetByinventoryAdjustmentNumber($inventory_adjustment_number)

Get a specific Inventory Adjustment document

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryAdjustmentApi();
$inventory_adjustment_number = "inventory_adjustment_number_example"; // string | Identifies the Inventory Adjustment document

try {
    $result = $api_instance->inventoryAdjustmentGetByinventoryAdjustmentNumber($inventory_adjustment_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryAdjustmentApi->inventoryAdjustmentGetByinventoryAdjustmentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_adjustment_number** | **string**| Identifies the Inventory Adjustment document |

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryAdjustmentDto**](../Model/InventoryAdjustmentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryAdjustmentPost**
> \Trollweb\VismaNetApi\Model\Object inventoryAdjustmentPost($inventory_adjustment)

Create an inventory item

Response Message has StatusCode Created if POST operation succed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryAdjustmentApi();
$inventory_adjustment = new \Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto | Define the data for the inventory adjustment to create

try {
    $result = $api_instance->inventoryAdjustmentPost($inventory_adjustment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryAdjustmentApi->inventoryAdjustmentPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_adjustment** | [**\Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto.md)| Define the data for the inventory adjustment to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryAdjustmentPutByadjRefNumber**
> \Trollweb\VismaNetApi\Model\Object inventoryAdjustmentPutByadjRefNumber($adj_ref_number, $adjustment)

Update a specific inventory item

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryAdjustmentApi();
$adj_ref_number = "adj_ref_number_example"; // string | Identifies the inventory item to update
$adjustment = new \Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto | The data to update for inventory item

try {
    $result = $api_instance->inventoryAdjustmentPutByadjRefNumber($adj_ref_number, $adjustment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryAdjustmentApi->inventoryAdjustmentPutByadjRefNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adj_ref_number** | **string**| Identifies the inventory item to update |
 **adjustment** | [**\Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryAdjustmentUpdateDto.md)| The data to update for inventory item |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryAdjustmentReleaseDocumentByadjRefNumber**
> \Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto inventoryAdjustmentReleaseDocumentByadjRefNumber($adj_ref_number)

Release inventory document action

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryAdjustmentApi();
$adj_ref_number = "adj_ref_number_example"; // string | Reference number of the released adjustment to be released

try {
    $result = $api_instance->inventoryAdjustmentReleaseDocumentByadjRefNumber($adj_ref_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryAdjustmentApi->inventoryAdjustmentReleaseDocumentByadjRefNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adj_ref_number** | **string**| Reference number of the released adjustment to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto**](../Model/ReleaseInventoryDocumentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

