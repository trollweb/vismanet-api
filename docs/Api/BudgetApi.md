# Trollweb\VismaNetApi\BudgetApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**budgetCreateBudget**](BudgetApi.md#budgetCreateBudget) | **POST** /controller/api/v1/budget | Create a Budget
[**budgetGetAll**](BudgetApi.md#budgetGetAll) | **GET** /controller/api/v1/budget | Get a range of General Ledger Budget Figures
[**budgetPut**](BudgetApi.md#budgetPut) | **PUT** /controller/api/v1/budget | Update a specific Budget


# **budgetCreateBudget**
> \Trollweb\VismaNetApi\Model\Object budgetCreateBudget($budget)

Create a Budget

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\BudgetApi();
$budget = new \Trollweb\VismaNetApi\Model\BudgetUpdateDto(); // \Trollweb\VismaNetApi\Model\BudgetUpdateDto | Defines the data for the Budget to create

try {
    $result = $api_instance->budgetCreateBudget($budget);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetApi->budgetCreateBudget: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **budget** | [**\Trollweb\VismaNetApi\Model\BudgetUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\BudgetUpdateDto.md)| Defines the data for the Budget to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **budgetGetAll**
> \Trollweb\VismaNetApi\Model\BudgetDto[] budgetGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $branch, $ledger, $financial_year, $subaccount)

Get a range of General Ledger Budget Figures

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\BudgetApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$branch = "branch_example"; // string | 
$ledger = "ledger_example"; // string | Mandatory
$financial_year = "financial_year_example"; // string | Mandatory
$subaccount = "subaccount_example"; // string | 

try {
    $result = $api_instance->budgetGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $branch, $ledger, $financial_year, $subaccount);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetApi->budgetGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **branch** | **string**|  | [optional]
 **ledger** | **string**| Mandatory | [optional]
 **financial_year** | **string**| Mandatory | [optional]
 **subaccount** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\BudgetDto[]**](../Model/BudgetDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **budgetPut**
> \Trollweb\VismaNetApi\Model\Object budgetPut($budget)

Update a specific Budget

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\BudgetApi();
$budget = new \Trollweb\VismaNetApi\Model\BudgetUpdateDto(); // \Trollweb\VismaNetApi\Model\BudgetUpdateDto | Defines the data for the Budget to update

try {
    $result = $api_instance->budgetPut($budget);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BudgetApi->budgetPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **budget** | [**\Trollweb\VismaNetApi\Model\BudgetUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\BudgetUpdateDto.md)| Defines the data for the Budget to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

