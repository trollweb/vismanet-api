# Trollweb\VismaNetApi\JournalTransactionApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**journalTransactionGetAllJournalTransactions**](JournalTransactionApi.md#journalTransactionGetAllJournalTransactions) | **GET** /controller/api/v1/journaltransaction | Get a range of Journal Transactions
[**journalTransactionGetSpecificJournalTransactionsByjournalTransactionNumber**](JournalTransactionApi.md#journalTransactionGetSpecificJournalTransactionsByjournalTransactionNumber) | **GET** /controller/api/v1/journaltransaction/{journalTransactionNumber} | Get a specific Journal Transaction
[**journalTransactionPost**](JournalTransactionApi.md#journalTransactionPost) | **POST** /controller/api/v1/journaltransaction | Create a Journal Transaction
[**journalTransactionPutByjournalTransactionNumber**](JournalTransactionApi.md#journalTransactionPutByjournalTransactionNumber) | **PUT** /controller/api/v1/journaltransaction/{journalTransactionNumber} | Update a Journal Transaction


# **journalTransactionGetAllJournalTransactions**
> \Trollweb\VismaNetApi\Model\JournalTransactionDto[] journalTransactionGetAllJournalTransactions($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $customer_supplier_start, $customer_supplier_end)

Get a range of Journal Transactions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\JournalTransactionApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$customer_supplier_start = "customer_supplier_start_example"; // string | 
$customer_supplier_end = "customer_supplier_end_example"; // string | 

try {
    $result = $api_instance->journalTransactionGetAllJournalTransactions($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $customer_supplier_start, $customer_supplier_end);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JournalTransactionApi->journalTransactionGetAllJournalTransactions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **customer_supplier_start** | **string**|  | [optional]
 **customer_supplier_end** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\JournalTransactionDto[]**](../Model/JournalTransactionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **journalTransactionGetSpecificJournalTransactionsByjournalTransactionNumber**
> \Trollweb\VismaNetApi\Model\JournalTransactionDto journalTransactionGetSpecificJournalTransactionsByjournalTransactionNumber($journal_transaction_number)

Get a specific Journal Transaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\JournalTransactionApi();
$journal_transaction_number = "journal_transaction_number_example"; // string | Identifies the Journal Transaction

try {
    $result = $api_instance->journalTransactionGetSpecificJournalTransactionsByjournalTransactionNumber($journal_transaction_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JournalTransactionApi->journalTransactionGetSpecificJournalTransactionsByjournalTransactionNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **journal_transaction_number** | **string**| Identifies the Journal Transaction |

### Return type

[**\Trollweb\VismaNetApi\Model\JournalTransactionDto**](../Model/JournalTransactionDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **journalTransactionPost**
> \Trollweb\VismaNetApi\Model\Object journalTransactionPost($journal_transaction)

Create a Journal Transaction

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\JournalTransactionApi();
$journal_transaction = new \Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto(); // \Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto | Defines the data for the Journal Transaction to create

try {
    $result = $api_instance->journalTransactionPost($journal_transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JournalTransactionApi->journalTransactionPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **journal_transaction** | [**\Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto.md)| Defines the data for the Journal Transaction to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **journalTransactionPutByjournalTransactionNumber**
> \Trollweb\VismaNetApi\Model\Object journalTransactionPutByjournalTransactionNumber($journal_transaction_number, $journal_transaction)

Update a Journal Transaction

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\JournalTransactionApi();
$journal_transaction_number = "journal_transaction_number_example"; // string | Identifies the Journal Transaction to update
$journal_transaction = new \Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto(); // \Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto | Defines the data for the Journal Transaction to update

try {
    $result = $api_instance->journalTransactionPutByjournalTransactionNumber($journal_transaction_number, $journal_transaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling JournalTransactionApi->journalTransactionPutByjournalTransactionNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **journal_transaction_number** | **string**| Identifies the Journal Transaction to update |
 **journal_transaction** | [**\Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\JournalTransactionUpdateDto.md)| Defines the data for the Journal Transaction to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

