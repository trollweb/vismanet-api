# Trollweb\VismaNetApi\CustomerInvoiceApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerInvoiceCreateHeaderAttachmentByinvoiceNumber**](CustomerInvoiceApi.md#customerInvoiceCreateHeaderAttachmentByinvoiceNumber) | **POST** /controller/api/v1/customerinvoice/{invoiceNumber}/attachment | Creates an attachment and associates it with an invoice. If the file already exists, a new revision is created.
[**customerInvoiceCreateInvoice**](CustomerInvoiceApi.md#customerInvoiceCreateInvoice) | **POST** /controller/api/v1/customerinvoice | Create an Invoice
[**customerInvoiceCreateLineAttachmentByinvoiceNumberlineNumber**](CustomerInvoiceApi.md#customerInvoiceCreateLineAttachmentByinvoiceNumberlineNumber) | **POST** /controller/api/v1/customerinvoice/{invoiceNumber}/{lineNumber}/attachment | Creates an attachment and associates it with a certain invoice line. If the file already exists, a new revision is created.
[**customerInvoiceGetAllInvoices**](CustomerInvoiceApi.md#customerInvoiceGetAllInvoices) | **GET** /controller/api/v1/customerinvoice | Get a range of Invoices
[**customerInvoiceGetByinvoiceNumber**](CustomerInvoiceApi.md#customerInvoiceGetByinvoiceNumber) | **GET** /controller/api/v1/customerinvoice/{invoiceNumber} | Get a specific Invoice
[**customerInvoiceGetRotRutByrefNbr**](CustomerInvoiceApi.md#customerInvoiceGetRotRutByrefNbr) | **GET** /controller/api/v1/customerinvoice/{refNbr}/rotrut | Get Rot Rut informations for a Customer Invoice
[**customerInvoiceGetWorkTypes**](CustomerInvoiceApi.md#customerInvoiceGetWorkTypes) | **GET** /controller/api/v1/customerinvoice/worktypes | Get all Work Types
[**customerInvoicePutByinvoiceNumber**](CustomerInvoiceApi.md#customerInvoicePutByinvoiceNumber) | **PUT** /controller/api/v1/customerinvoice/{invoiceNumber} | Update a specific Invoice
[**customerInvoiceReleaseInvoiceByinvoiceNumber**](CustomerInvoiceApi.md#customerInvoiceReleaseInvoiceByinvoiceNumber) | **POST** /controller/api/v1/customerinvoice/{invoiceNumber}/action/release | Release invoice operation
[**customerInvoiceReverseInvoiceByinvoiceNumber**](CustomerInvoiceApi.md#customerInvoiceReverseInvoiceByinvoiceNumber) | **POST** /controller/api/v1/customerinvoice/{invoiceNumber}/action/reverse | Reverse an Invoice


# **customerInvoiceCreateHeaderAttachmentByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\Object customerInvoiceCreateHeaderAttachmentByinvoiceNumber($invoice_number)

Creates an attachment and associates it with an invoice. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the invoice

try {
    $result = $api_instance->customerInvoiceCreateHeaderAttachmentByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceCreateHeaderAttachmentByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the invoice |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceCreateInvoice**
> \Trollweb\VismaNetApi\Model\Object customerInvoiceCreateInvoice($invoice)

Create an Invoice

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice = new \Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto(); // \Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto | Defines the data for the Invoice to create

try {
    $result = $api_instance->customerInvoiceCreateInvoice($invoice);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceCreateInvoice: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice** | [**\Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto.md)| Defines the data for the Invoice to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceCreateLineAttachmentByinvoiceNumberlineNumber**
> \Trollweb\VismaNetApi\Model\Object customerInvoiceCreateLineAttachmentByinvoiceNumberlineNumber($invoice_number, $line_number)

Creates an attachment and associates it with a certain invoice line. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the invoice
$line_number = 56; // int | Specifies line number

try {
    $result = $api_instance->customerInvoiceCreateLineAttachmentByinvoiceNumberlineNumber($invoice_number, $line_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceCreateLineAttachmentByinvoiceNumberlineNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the invoice |
 **line_number** | **int**| Specifies line number |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceGetAllInvoices**
> \Trollweb\VismaNetApi\Model\CustomerInvoiceDto[] customerInvoiceGetAllInvoices($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of Invoices

Data for Customer Invoice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->customerInvoiceGetAllInvoices($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceGetAllInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerInvoiceDto[]**](../Model/CustomerInvoiceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceGetByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\CustomerInvoiceDto customerInvoiceGetByinvoiceNumber($invoice_number)

Get a specific Invoice

Data for Customer Invoice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the Invoice

try {
    $result = $api_instance->customerInvoiceGetByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceGetByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the Invoice |

### Return type

[**\Trollweb\VismaNetApi\Model\CustomerInvoiceDto**](../Model/CustomerInvoiceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceGetRotRutByrefNbr**
> \Trollweb\VismaNetApi\Model\RotRutDto customerInvoiceGetRotRutByrefNbr($ref_nbr)

Get Rot Rut informations for a Customer Invoice

Data for the Rot Rut

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$ref_nbr = "ref_nbr_example"; // string | Identify the customer invoice for which to return data

try {
    $result = $api_instance->customerInvoiceGetRotRutByrefNbr($ref_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceGetRotRutByrefNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ref_nbr** | **string**| Identify the customer invoice for which to return data |

### Return type

[**\Trollweb\VismaNetApi\Model\RotRutDto**](../Model/RotRutDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceGetWorkTypes**
> \Trollweb\VismaNetApi\Model\WorkTypeDto[] customerInvoiceGetWorkTypes()

Get all Work Types

Data for the Work Type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();

try {
    $result = $api_instance->customerInvoiceGetWorkTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceGetWorkTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\WorkTypeDto[]**](../Model/WorkTypeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoicePutByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\Object customerInvoicePutByinvoiceNumber($invoice_number, $invoice)

Update a specific Invoice

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the Invoice to update
$invoice = new \Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto(); // \Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto | Defines the data for the Invoice to update

try {
    $result = $api_instance->customerInvoicePutByinvoiceNumber($invoice_number, $invoice);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoicePutByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the Invoice to update |
 **invoice** | [**\Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CustomerInvoiceUpdateDto.md)| Defines the data for the Invoice to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceReleaseInvoiceByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\ReleaseInvoiceActionResultDto customerInvoiceReleaseInvoiceByinvoiceNumber($invoice_number)

Release invoice operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Reference number of the invoice to be released

try {
    $result = $api_instance->customerInvoiceReleaseInvoiceByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceReleaseInvoiceByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Reference number of the invoice to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseInvoiceActionResultDto**](../Model/ReleaseInvoiceActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **customerInvoiceReverseInvoiceByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\ReverseInvoiceActionResultDto customerInvoiceReverseInvoiceByinvoiceNumber($invoice_number, $reverse_action_dto)

Reverse an Invoice

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CustomerInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Reference number of the released invoice to be reversed
$reverse_action_dto = new \Trollweb\VismaNetApi\Model\ReverseInvoiceActionDto(); // \Trollweb\VismaNetApi\Model\ReverseInvoiceActionDto | The action the dto use to condition the action

try {
    $result = $api_instance->customerInvoiceReverseInvoiceByinvoiceNumber($invoice_number, $reverse_action_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerInvoiceApi->customerInvoiceReverseInvoiceByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Reference number of the released invoice to be reversed |
 **reverse_action_dto** | [**\Trollweb\VismaNetApi\Model\ReverseInvoiceActionDto**](../Model/\Trollweb\VismaNetApi\Model\ReverseInvoiceActionDto.md)| The action the dto use to condition the action |

### Return type

[**\Trollweb\VismaNetApi\Model\ReverseInvoiceActionResultDto**](../Model/ReverseInvoiceActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

