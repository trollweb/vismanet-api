# Trollweb\VismaNetApi\FinancialPeriodApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**financialPeriodGetAll**](FinancialPeriodApi.md#financialPeriodGetAll) | **GET** /controller/api/v1/financialPeriod | Get a range of Financial Periods
[**financialPeriodGetByfinancialPeriodId**](FinancialPeriodApi.md#financialPeriodGetByfinancialPeriodId) | **GET** /controller/api/v1/financialPeriod/{financialPeriodId} | Get a specific Financial Period


# **financialPeriodGetAll**
> \Trollweb\VismaNetApi\Model\FinancialPeriodDto[] financialPeriodGetAll($greater_than_value, $number_to_read, $skip_records, $order_by)

Get a range of Financial Periods

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\FinancialPeriodApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 

try {
    $result = $api_instance->financialPeriodGetAll($greater_than_value, $number_to_read, $skip_records, $order_by);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinancialPeriodApi->financialPeriodGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\FinancialPeriodDto[]**](../Model/FinancialPeriodDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **financialPeriodGetByfinancialPeriodId**
> \Trollweb\VismaNetApi\Model\FinancialPeriodDto financialPeriodGetByfinancialPeriodId($financial_period_id)

Get a specific Financial Period

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\FinancialPeriodApi();
$financial_period_id = "financial_period_id_example"; // string | Identifies the Financial Period

try {
    $result = $api_instance->financialPeriodGetByfinancialPeriodId($financial_period_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FinancialPeriodApi->financialPeriodGetByfinancialPeriodId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **financial_period_id** | **string**| Identifies the Financial Period |

### Return type

[**\Trollweb\VismaNetApi\Model\FinancialPeriodDto**](../Model/FinancialPeriodDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

