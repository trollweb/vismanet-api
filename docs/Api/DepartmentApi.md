# Trollweb\VismaNetApi\DepartmentApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**departmentCreateDepartment**](DepartmentApi.md#departmentCreateDepartment) | **POST** /controller/api/v1/department | Creates a department
[**departmentGetAllDepartmentDtos**](DepartmentApi.md#departmentGetAllDepartmentDtos) | **GET** /controller/api/v1/department | Get a range of department
[**departmentGetDepartmentBydepartmentId**](DepartmentApi.md#departmentGetDepartmentBydepartmentId) | **GET** /controller/api/v1/department/{departmentId} | Get a specific department
[**departmentUpdateDepartmentBydepartmentId**](DepartmentApi.md#departmentUpdateDepartmentBydepartmentId) | **PUT** /controller/api/v1/department/{departmentId} | Updates a specific department


# **departmentCreateDepartment**
> \Trollweb\VismaNetApi\Model\Object departmentCreateDepartment($department_update_dto)

Creates a department

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DepartmentApi();
$department_update_dto = new \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto(); // \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto | Defines the data for the department to create

try {
    $result = $api_instance->departmentCreateDepartment($department_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DepartmentApi->departmentCreateDepartment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **department_update_dto** | [**\Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto**](../Model/\Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto.md)| Defines the data for the department to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **departmentGetAllDepartmentDtos**
> \Trollweb\VismaNetApi\Model\DepartmentDto[] departmentGetAllDepartmentDtos($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of department

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DepartmentApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->departmentGetAllDepartmentDtos($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DepartmentApi->departmentGetAllDepartmentDtos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\DepartmentDto[]**](../Model/DepartmentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **departmentGetDepartmentBydepartmentId**
> \Trollweb\VismaNetApi\Model\DepartmentDto departmentGetDepartmentBydepartmentId($department_id)

Get a specific department

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DepartmentApi();
$department_id = "department_id_example"; // string | Identifies the department

try {
    $result = $api_instance->departmentGetDepartmentBydepartmentId($department_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DepartmentApi->departmentGetDepartmentBydepartmentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **department_id** | **string**| Identifies the department |

### Return type

[**\Trollweb\VismaNetApi\Model\DepartmentDto**](../Model/DepartmentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **departmentUpdateDepartmentBydepartmentId**
> \Trollweb\VismaNetApi\Model\Object departmentUpdateDepartmentBydepartmentId($department_id, $department_update_dto)

Updates a specific department

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\DepartmentApi();
$department_id = "department_id_example"; // string | Identifies the department to update
$department_update_dto = new \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto(); // \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto | The data to update the department with

try {
    $result = $api_instance->departmentUpdateDepartmentBydepartmentId($department_id, $department_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DepartmentApi->departmentUpdateDepartmentBydepartmentId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **department_id** | **string**| Identifies the department to update |
 **department_update_dto** | [**\Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto**](../Model/\Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto.md)| The data to update the department with |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

