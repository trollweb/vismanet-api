# Trollweb\VismaNetApi\InventorySummaryApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventorySummaryGetAllInventorySummaryByinventoryNumber**](InventorySummaryApi.md#inventorySummaryGetAllInventorySummaryByinventoryNumber) | **GET** /controller/api/v1/inventorysummary/{inventoryNumber} | Get a range of InventorySummary


# **inventorySummaryGetAllInventorySummaryByinventoryNumber**
> \Trollweb\VismaNetApi\Model\InventorySummaryDto[] inventorySummaryGetAllInventorySummaryByinventoryNumber($inventory_number, $warehouse, $location)

Get a range of InventorySummary

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventorySummaryApi();
$inventory_number = "inventory_number_example"; // string | 
$warehouse = "warehouse_example"; // string | 
$location = "location_example"; // string | 

try {
    $result = $api_instance->inventorySummaryGetAllInventorySummaryByinventoryNumber($inventory_number, $warehouse, $location);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventorySummaryApi->inventorySummaryGetAllInventorySummaryByinventoryNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_number** | **string**|  |
 **warehouse** | **string**|  | [optional]
 **location** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\InventorySummaryDto[]**](../Model/InventorySummaryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

