# Trollweb\VismaNetApi\InventoryIssueApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryIssueGetAll**](InventoryIssueApi.md#inventoryIssueGetAll) | **GET** /controller/api/v1/inventoryissue | Get a range of Inventory items
[**inventoryIssueGetByinventoryIssueNumber**](InventoryIssueApi.md#inventoryIssueGetByinventoryIssueNumber) | **GET** /controller/api/v1/inventoryissue/{inventoryIssueNumber} | Get a specific Inventory Issue document
[**inventoryIssuePost**](InventoryIssueApi.md#inventoryIssuePost) | **POST** /controller/api/v1/inventoryissue | Create an inventory item
[**inventoryIssuePutByissueRefNumber**](InventoryIssueApi.md#inventoryIssuePutByissueRefNumber) | **PUT** /controller/api/v1/inventoryissue/{issueRefNumber} | Update a specific inventory item
[**inventoryIssueReleaseDocumentByinvoiceNumber**](InventoryIssueApi.md#inventoryIssueReleaseDocumentByinvoiceNumber) | **POST** /controller/api/v1/inventoryissue/{invoiceNumber}/action/release | Release inventory operation


# **inventoryIssueGetAll**
> \Trollweb\VismaNetApi\Model\InventoryIssueDto[] inventoryIssueGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Inventory items

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryIssueApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->inventoryIssueGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryIssueApi->inventoryIssueGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryIssueDto[]**](../Model/InventoryIssueDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryIssueGetByinventoryIssueNumber**
> \Trollweb\VismaNetApi\Model\InventoryIssueDto inventoryIssueGetByinventoryIssueNumber($inventory_issue_number)

Get a specific Inventory Issue document

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryIssueApi();
$inventory_issue_number = "inventory_issue_number_example"; // string | Identifies the Inventory Issue document

try {
    $result = $api_instance->inventoryIssueGetByinventoryIssueNumber($inventory_issue_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryIssueApi->inventoryIssueGetByinventoryIssueNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_issue_number** | **string**| Identifies the Inventory Issue document |

### Return type

[**\Trollweb\VismaNetApi\Model\InventoryIssueDto**](../Model/InventoryIssueDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryIssuePost**
> \Trollweb\VismaNetApi\Model\Object inventoryIssuePost($inventory_issue)

Create an inventory item

Response Message has StatusCode Created if POST operation succed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryIssueApi();
$inventory_issue = new \Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto | Define the data for the inventory issue to create

try {
    $result = $api_instance->inventoryIssuePost($inventory_issue);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryIssueApi->inventoryIssuePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inventory_issue** | [**\Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto.md)| Define the data for the inventory issue to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryIssuePutByissueRefNumber**
> \Trollweb\VismaNetApi\Model\Object inventoryIssuePutByissueRefNumber($issue_ref_number, $issue)

Update a specific inventory item

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryIssueApi();
$issue_ref_number = "issue_ref_number_example"; // string | Identifies the inventory item to update
$issue = new \Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto(); // \Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto | The data to update for inventory item

try {
    $result = $api_instance->inventoryIssuePutByissueRefNumber($issue_ref_number, $issue);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryIssueApi->inventoryIssuePutByissueRefNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issue_ref_number** | **string**| Identifies the inventory item to update |
 **issue** | [**\Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\InventoryIssueUpdateDto.md)| The data to update for inventory item |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventoryIssueReleaseDocumentByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto inventoryIssueReleaseDocumentByinvoiceNumber($invoice_number)

Release inventory operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\InventoryIssueApi();
$invoice_number = "invoice_number_example"; // string | Reference number of the released issue to be released

try {
    $result = $api_instance->inventoryIssueReleaseDocumentByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InventoryIssueApi->inventoryIssueReleaseDocumentByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Reference number of the released issue to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseInventoryDocumentActionResultDto**](../Model/ReleaseInventoryDocumentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

