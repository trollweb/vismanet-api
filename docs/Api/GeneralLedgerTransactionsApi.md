# Trollweb\VismaNetApi\GeneralLedgerTransactionsApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generalLedgerTransactionsGetAll**](GeneralLedgerTransactionsApi.md#generalLedgerTransactionsGetAll) | **GET** /controller/api/v1/GeneralLedgerTransactions | Get a range of General Ledger Transactions


# **generalLedgerTransactionsGetAll**
> \Trollweb\VismaNetApi\Model\GeneralLedgerTransactionDetailsDto[] generalLedgerTransactionsGetAll($branch, $ledger, $from_period, $to_period, $account, $subaccount_id, $from_date, $to_date, $include_unposted, $include_unreleased, $skip_records, $number_to_read, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of General Ledger Transactions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\GeneralLedgerTransactionsApi();
$branch = "branch_example"; // string | 
$ledger = "ledger_example"; // string | Mandatory
$from_period = "from_period_example"; // string | Mandatory
$to_period = "to_period_example"; // string | Mandatory
$account = "account_example"; // string | 
$subaccount_id = "subaccount_id_example"; // string | 
$from_date = "from_date_example"; // string | 
$to_date = "to_date_example"; // string | 
$include_unposted = true; // bool | 
$include_unreleased = true; // bool | 
$skip_records = 56; // int | 
$number_to_read = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->generalLedgerTransactionsGetAll($branch, $ledger, $from_period, $to_period, $account, $subaccount_id, $from_date, $to_date, $include_unposted, $include_unreleased, $skip_records, $number_to_read, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeneralLedgerTransactionsApi->generalLedgerTransactionsGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **branch** | **string**|  | [optional]
 **ledger** | **string**| Mandatory | [optional]
 **from_period** | **string**| Mandatory | [optional]
 **to_period** | **string**| Mandatory | [optional]
 **account** | **string**|  | [optional]
 **subaccount_id** | **string**|  | [optional]
 **from_date** | **string**|  | [optional]
 **to_date** | **string**|  | [optional]
 **include_unposted** | **bool**|  | [optional]
 **include_unreleased** | **bool**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\GeneralLedgerTransactionDetailsDto[]**](../Model/GeneralLedgerTransactionDetailsDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

