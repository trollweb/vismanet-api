# Trollweb\VismaNetApi\VatCategoryApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vatCategoryGetAllVatCategories**](VatCategoryApi.md#vatCategoryGetAllVatCategories) | **GET** /controller/api/v1/vatCategory | Get a range of VatCategories
[**vatCategoryGetVatCategoryBytaxCategoryId**](VatCategoryApi.md#vatCategoryGetVatCategoryBytaxCategoryId) | **GET** /controller/api/v1/vatCategory/{taxCategoryId} | Get a specific VatCategory


# **vatCategoryGetAllVatCategories**
> \Trollweb\VismaNetApi\Model\VatCategoryDto[] vatCategoryGetAllVatCategories($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of VatCategories

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\VatCategoryApi();
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->vatCategoryGetAllVatCategories($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatCategoryApi->vatCategoryGetAllVatCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\VatCategoryDto[]**](../Model/VatCategoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **vatCategoryGetVatCategoryBytaxCategoryId**
> \Trollweb\VismaNetApi\Model\VatCategoryDto vatCategoryGetVatCategoryBytaxCategoryId($tax_category_id)

Get a specific VatCategory

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\VatCategoryApi();
$tax_category_id = "tax_category_id_example"; // string | Identifies the VatCategory

try {
    $result = $api_instance->vatCategoryGetVatCategoryBytaxCategoryId($tax_category_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VatCategoryApi->vatCategoryGetVatCategoryBytaxCategoryId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tax_category_id** | **string**| Identifies the VatCategory |

### Return type

[**\Trollweb\VismaNetApi\Model\VatCategoryDto**](../Model/VatCategoryDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

