# Trollweb\VismaNetApi\CreditNoteApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**creditNoteCreateCreditNote**](CreditNoteApi.md#creditNoteCreateCreditNote) | **POST** /controller/api/v1/creditNote | Create a Credit Note
[**creditNoteCreateHeaderAttachmentBycreditNoteNumber**](CreditNoteApi.md#creditNoteCreateHeaderAttachmentBycreditNoteNumber) | **POST** /controller/api/v1/creditNote/{creditNoteNumber}/attachment | Creates an attachment and associates it with a Credit Note. If the file already exists, a new revision is created.
[**creditNoteCreateLineAttachmentBycreditNoteNumberlineNumber**](CreditNoteApi.md#creditNoteCreateLineAttachmentBycreditNoteNumberlineNumber) | **POST** /controller/api/v1/creditNote/{creditNoteNumber}/{lineNumber}/attachment | Creates an attachment and associates it with a certain Credit Note line. If the file already exists, a new revision is created.
[**creditNoteGetAllCreditNotesDto**](CreditNoteApi.md#creditNoteGetAllCreditNotesDto) | **GET** /controller/api/v1/creditNote | Get a range of Credit Notes
[**creditNoteGetBycreditNoteNumber**](CreditNoteApi.md#creditNoteGetBycreditNoteNumber) | **GET** /controller/api/v1/creditNote/{creditNoteNumber} | Get a specific Credit Note
[**creditNotePutBycreditNoteNumber**](CreditNoteApi.md#creditNotePutBycreditNoteNumber) | **PUT** /controller/api/v1/creditNote/{creditNoteNumber} | Update a specific Credit Note
[**creditNoteReleaseInvoiceBycreditNoteNumber**](CreditNoteApi.md#creditNoteReleaseInvoiceBycreditNoteNumber) | **POST** /controller/api/v1/creditNote/{creditNoteNumber}/action/release | Release credit note operation


# **creditNoteCreateCreditNote**
> \Trollweb\VismaNetApi\Model\Object creditNoteCreateCreditNote($credit_note_update_dto)

Create a Credit Note

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$credit_note_update_dto = new \Trollweb\VismaNetApi\Model\CreditNoteUpdateDto(); // \Trollweb\VismaNetApi\Model\CreditNoteUpdateDto | Defines the data for the Credit Note to create

try {
    $result = $api_instance->creditNoteCreateCreditNote($credit_note_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNoteCreateCreditNote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_note_update_dto** | [**\Trollweb\VismaNetApi\Model\CreditNoteUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CreditNoteUpdateDto.md)| Defines the data for the Credit Note to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **creditNoteCreateHeaderAttachmentBycreditNoteNumber**
> \Trollweb\VismaNetApi\Model\Object creditNoteCreateHeaderAttachmentBycreditNoteNumber($credit_note_number)

Creates an attachment and associates it with a Credit Note. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$credit_note_number = "credit_note_number_example"; // string | Identifies the Credit Note

try {
    $result = $api_instance->creditNoteCreateHeaderAttachmentBycreditNoteNumber($credit_note_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNoteCreateHeaderAttachmentBycreditNoteNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_note_number** | **string**| Identifies the Credit Note |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **creditNoteCreateLineAttachmentBycreditNoteNumberlineNumber**
> \Trollweb\VismaNetApi\Model\Object creditNoteCreateLineAttachmentBycreditNoteNumberlineNumber($credit_note_number, $line_number)

Creates an attachment and associates it with a certain Credit Note line. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$credit_note_number = "credit_note_number_example"; // string | Identifies the Credit Note
$line_number = 56; // int | Specifies line number

try {
    $result = $api_instance->creditNoteCreateLineAttachmentBycreditNoteNumberlineNumber($credit_note_number, $line_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNoteCreateLineAttachmentBycreditNoteNumberlineNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_note_number** | **string**| Identifies the Credit Note |
 **line_number** | **int**| Specifies line number |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **creditNoteGetAllCreditNotesDto**
> \Trollweb\VismaNetApi\Model\CreditNoteDto[] creditNoteGetAllCreditNotesDto($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of Credit Notes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->creditNoteGetAllCreditNotesDto($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNoteGetAllCreditNotesDto: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CreditNoteDto[]**](../Model/CreditNoteDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **creditNoteGetBycreditNoteNumber**
> \Trollweb\VismaNetApi\Model\CreditNoteDto creditNoteGetBycreditNoteNumber($credit_note_number)

Get a specific Credit Note

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$credit_note_number = "credit_note_number_example"; // string | Identifies the Credit Note

try {
    $result = $api_instance->creditNoteGetBycreditNoteNumber($credit_note_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNoteGetBycreditNoteNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_note_number** | **string**| Identifies the Credit Note |

### Return type

[**\Trollweb\VismaNetApi\Model\CreditNoteDto**](../Model/CreditNoteDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **creditNotePutBycreditNoteNumber**
> \Trollweb\VismaNetApi\Model\Object creditNotePutBycreditNoteNumber($credit_note_number, $credit_note_update_dto)

Update a specific Credit Note

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$credit_note_number = "credit_note_number_example"; // string | Identifies the Credit Note to update
$credit_note_update_dto = new \Trollweb\VismaNetApi\Model\CreditNoteUpdateDto(); // \Trollweb\VismaNetApi\Model\CreditNoteUpdateDto | Defines the data for the Credit Note to update

try {
    $result = $api_instance->creditNotePutBycreditNoteNumber($credit_note_number, $credit_note_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNotePutBycreditNoteNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_note_number** | **string**| Identifies the Credit Note to update |
 **credit_note_update_dto** | [**\Trollweb\VismaNetApi\Model\CreditNoteUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\CreditNoteUpdateDto.md)| Defines the data for the Credit Note to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **creditNoteReleaseInvoiceBycreditNoteNumber**
> \Trollweb\VismaNetApi\Model\ReleaseInvoiceActionResultDto creditNoteReleaseInvoiceBycreditNoteNumber($credit_note_number)

Release credit note operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CreditNoteApi();
$credit_note_number = "credit_note_number_example"; // string | Reference number of the credit note to be released

try {
    $result = $api_instance->creditNoteReleaseInvoiceBycreditNoteNumber($credit_note_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CreditNoteApi->creditNoteReleaseInvoiceBycreditNoteNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit_note_number** | **string**| Reference number of the credit note to be released |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseInvoiceActionResultDto**](../Model/ReleaseInvoiceActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

