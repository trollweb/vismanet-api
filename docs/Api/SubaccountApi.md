# Trollweb\VismaNetApi\SubaccountApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**subaccountGetAllSubaccounts**](SubaccountApi.md#subaccountGetAllSubaccounts) | **GET** /controller/api/v1/subaccount | Get all SubAccounts
[**subaccountGetSubaccountBysubCd**](SubaccountApi.md#subaccountGetSubaccountBysubCd) | **GET** /controller/api/v1/subaccount/{subCd} | Get a specific SubAccount
[**subaccountPost**](SubaccountApi.md#subaccountPost) | **POST** /controller/api/v1/subaccount | Create a Subaccount
[**subaccountPutBysubAccountCd**](SubaccountApi.md#subaccountPutBysubAccountCd) | **PUT** /controller/api/v1/subaccount/{subAccountCd} | Update a specific Subaccount


# **subaccountGetAllSubaccounts**
> \Trollweb\VismaNetApi\Model\SubAccountDto[] subaccountGetAllSubaccounts()

Get all SubAccounts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubaccountApi();

try {
    $result = $api_instance->subaccountGetAllSubaccounts();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubaccountApi->subaccountGetAllSubaccounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\SubAccountDto[]**](../Model/SubAccountDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subaccountGetSubaccountBysubCd**
> \Trollweb\VismaNetApi\Model\SubAccountDto subaccountGetSubaccountBysubCd($sub_cd)

Get a specific SubAccount

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubaccountApi();
$sub_cd = "sub_cd_example"; // string | Identifies the SubAccount

try {
    $result = $api_instance->subaccountGetSubaccountBysubCd($sub_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubaccountApi->subaccountGetSubaccountBysubCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sub_cd** | **string**| Identifies the SubAccount |

### Return type

[**\Trollweb\VismaNetApi\Model\SubAccountDto**](../Model/SubAccountDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subaccountPost**
> \Trollweb\VismaNetApi\Model\Object subaccountPost($sub_account_update_dto)

Create a Subaccount

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubaccountApi();
$sub_account_update_dto = new \Trollweb\VismaNetApi\Model\SubAccountUpdateDto(); // \Trollweb\VismaNetApi\Model\SubAccountUpdateDto | Defines the data for Subaccount to create

try {
    $result = $api_instance->subaccountPost($sub_account_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubaccountApi->subaccountPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sub_account_update_dto** | [**\Trollweb\VismaNetApi\Model\SubAccountUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SubAccountUpdateDto.md)| Defines the data for Subaccount to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **subaccountPutBysubAccountCd**
> \Trollweb\VismaNetApi\Model\Object subaccountPutBysubAccountCd($sub_account_cd, $sub_account_update_dto)

Update a specific Subaccount

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SubaccountApi();
$sub_account_cd = "sub_account_cd_example"; // string | Identifies the Subaccount to update
$sub_account_update_dto = new \Trollweb\VismaNetApi\Model\SubAccountUpdateDto(); // \Trollweb\VismaNetApi\Model\SubAccountUpdateDto | Defines the data for the Subaccount to update

try {
    $result = $api_instance->subaccountPutBysubAccountCd($sub_account_cd, $sub_account_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SubaccountApi->subaccountPutBysubAccountCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sub_account_cd** | **string**| Identifies the Subaccount to update |
 **sub_account_update_dto** | [**\Trollweb\VismaNetApi\Model\SubAccountUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SubAccountUpdateDto.md)| Defines the data for the Subaccount to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

