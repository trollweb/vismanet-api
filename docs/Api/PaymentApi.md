# Trollweb\VismaNetApi\PaymentApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentCreatePayment**](PaymentApi.md#paymentCreatePayment) | **POST** /controller/api/v1/payment | Create a Payment
[**paymentGetAllPayments**](PaymentApi.md#paymentGetAllPayments) | **GET** /controller/api/v1/payment | Get a range of Payments
[**paymentGetBypaymentNumber**](PaymentApi.md#paymentGetBypaymentNumber) | **GET** /controller/api/v1/payment/{paymentNumber} | Get a specific Payment
[**paymentPutBypaymentNumber**](PaymentApi.md#paymentPutBypaymentNumber) | **PUT** /controller/api/v1/payment/{paymentNumber} | Update a specific Payment
[**paymentReleasePaymentBypaymentNumber**](PaymentApi.md#paymentReleasePaymentBypaymentNumber) | **POST** /controller/api/v1/payment/{paymentNumber}/action/release | Release payment operation


# **paymentCreatePayment**
> \Trollweb\VismaNetApi\Model\Object paymentCreatePayment($payment)

Create a Payment

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentApi();
$payment = new \Trollweb\VismaNetApi\Model\PaymentUpdateDto(); // \Trollweb\VismaNetApi\Model\PaymentUpdateDto | Defines the data for the Payment to create

try {
    $result = $api_instance->paymentCreatePayment($payment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentCreatePayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment** | [**\Trollweb\VismaNetApi\Model\PaymentUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PaymentUpdateDto.md)| Defines the data for the Payment to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **paymentGetAllPayments**
> \Trollweb\VismaNetApi\Model\PaymentDto[] paymentGetAllPayments($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Payments

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->paymentGetAllPayments($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentGetAllPayments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PaymentDto[]**](../Model/PaymentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **paymentGetBypaymentNumber**
> \Trollweb\VismaNetApi\Model\PaymentDto paymentGetBypaymentNumber($payment_number)

Get a specific Payment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentApi();
$payment_number = "payment_number_example"; // string | Identifies the Payment

try {
    $result = $api_instance->paymentGetBypaymentNumber($payment_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentGetBypaymentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_number** | **string**| Identifies the Payment |

### Return type

[**\Trollweb\VismaNetApi\Model\PaymentDto**](../Model/PaymentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **paymentPutBypaymentNumber**
> \Trollweb\VismaNetApi\Model\Object paymentPutBypaymentNumber($payment_number, $payment)

Update a specific Payment

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentApi();
$payment_number = "payment_number_example"; // string | Identifies the Payment to update
$payment = new \Trollweb\VismaNetApi\Model\PaymentUpdateDto(); // \Trollweb\VismaNetApi\Model\PaymentUpdateDto | Defines the data for the Payment to update

try {
    $result = $api_instance->paymentPutBypaymentNumber($payment_number, $payment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentPutBypaymentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_number** | **string**| Identifies the Payment to update |
 **payment** | [**\Trollweb\VismaNetApi\Model\PaymentUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\PaymentUpdateDto.md)| Defines the data for the Payment to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **paymentReleasePaymentBypaymentNumber**
> \Trollweb\VismaNetApi\Model\ReleasePaymentActionResultDto paymentReleasePaymentBypaymentNumber($payment_number, $release_action_dto)

Release payment operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentApi();
$payment_number = "payment_number_example"; // string | Reference number of the payment to be released
$release_action_dto = new \Trollweb\VismaNetApi\Model\ReleasePaymentActionDto(); // \Trollweb\VismaNetApi\Model\ReleasePaymentActionDto | Contains the type of the payment

try {
    $result = $api_instance->paymentReleasePaymentBypaymentNumber($payment_number, $release_action_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentApi->paymentReleasePaymentBypaymentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_number** | **string**| Reference number of the payment to be released |
 **release_action_dto** | [**\Trollweb\VismaNetApi\Model\ReleasePaymentActionDto**](../Model/\Trollweb\VismaNetApi\Model\ReleasePaymentActionDto.md)| Contains the type of the payment |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleasePaymentActionResultDto**](../Model/ReleasePaymentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

