# Trollweb\VismaNetApi\WarehouseApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**warehouseGetAll**](WarehouseApi.md#warehouseGetAll) | **GET** /controller/api/v1/warehouse | Get a range of Warehouses
[**warehouseGetBywarehouseID**](WarehouseApi.md#warehouseGetBywarehouseID) | **GET** /controller/api/v1/warehouse/{warehouseID} | Get a specific Warehouse


# **warehouseGetAll**
> \Trollweb\VismaNetApi\Model\WarehouseDto[] warehouseGetAll()

Get a range of Warehouses

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\WarehouseApi();

try {
    $result = $api_instance->warehouseGetAll();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WarehouseApi->warehouseGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\WarehouseDto[]**](../Model/WarehouseDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **warehouseGetBywarehouseID**
> \Trollweb\VismaNetApi\Model\WarehouseDto warehouseGetBywarehouseID($warehouse_id)

Get a specific Warehouse

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\WarehouseApi();
$warehouse_id = "warehouse_id_example"; // string | Identifies the Warehouse

try {
    $result = $api_instance->warehouseGetBywarehouseID($warehouse_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WarehouseApi->warehouseGetBywarehouseID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **warehouse_id** | **string**| Identifies the Warehouse |

### Return type

[**\Trollweb\VismaNetApi\Model\WarehouseDto**](../Model/WarehouseDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

