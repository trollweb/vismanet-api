# Trollweb\VismaNetApi\ShipmentApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**shipmentConfirmShipmentByshipmentNumber**](ShipmentApi.md#shipmentConfirmShipmentByshipmentNumber) | **POST** /controller/api/v1/shipment/{shipmentNumber}/action/confirmShipment | Confirm shipment operation
[**shipmentGetAllShipments**](ShipmentApi.md#shipmentGetAllShipments) | **GET** /controller/api/v1/shipment | Get a range of SO Shipments
[**shipmentGetByshipmentNbr**](ShipmentApi.md#shipmentGetByshipmentNbr) | **GET** /controller/api/v1/shipment/{shipmentNbr} | Get a specific Shipment
[**shipmentPutByshipmentNumber**](ShipmentApi.md#shipmentPutByshipmentNumber) | **PUT** /controller/api/v1/shipment/{shipmentNumber} | Update a specific Shipment


# **shipmentConfirmShipmentByshipmentNumber**
> \Trollweb\VismaNetApi\Model\ConfirmShipmentActionResultDto shipmentConfirmShipmentByshipmentNumber($shipment_number)

Confirm shipment operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ShipmentApi();
$shipment_number = "shipment_number_example"; // string | Reference number of the shipment to be confirmed

try {
    $result = $api_instance->shipmentConfirmShipmentByshipmentNumber($shipment_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentApi->shipmentConfirmShipmentByshipmentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment_number** | **string**| Reference number of the shipment to be confirmed |

### Return type

[**\Trollweb\VismaNetApi\Model\ConfirmShipmentActionResultDto**](../Model/ConfirmShipmentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **shipmentGetAllShipments**
> \Trollweb\VismaNetApi\Model\ShipmentDto[] shipmentGetAllShipments($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of SO Shipments

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ShipmentApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->shipmentGetAllShipments($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentApi->shipmentGetAllShipments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ShipmentDto[]**](../Model/ShipmentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **shipmentGetByshipmentNbr**
> \Trollweb\VismaNetApi\Model\ShipmentDto shipmentGetByshipmentNbr($shipment_nbr)

Get a specific Shipment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ShipmentApi();
$shipment_nbr = "shipment_nbr_example"; // string | Identifies the Shipment

try {
    $result = $api_instance->shipmentGetByshipmentNbr($shipment_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentApi->shipmentGetByshipmentNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment_nbr** | **string**| Identifies the Shipment |

### Return type

[**\Trollweb\VismaNetApi\Model\ShipmentDto**](../Model/ShipmentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **shipmentPutByshipmentNumber**
> \Trollweb\VismaNetApi\Model\Object shipmentPutByshipmentNumber($shipment_number, $shipment_update_dto)

Update a specific Shipment

Response Message has StatusCode NoContent if PUT operation succeeded

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ShipmentApi();
$shipment_number = "shipment_number_example"; // string | Identifies the Shipment to update
$shipment_update_dto = new \Trollweb\VismaNetApi\Model\ShipmentUpdateDto(); // \Trollweb\VismaNetApi\Model\ShipmentUpdateDto | Defines the data for the Shipment to update

try {
    $result = $api_instance->shipmentPutByshipmentNumber($shipment_number, $shipment_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentApi->shipmentPutByshipmentNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipment_number** | **string**| Identifies the Shipment to update |
 **shipment_update_dto** | [**\Trollweb\VismaNetApi\Model\ShipmentUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ShipmentUpdateDto.md)| Defines the data for the Shipment to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

