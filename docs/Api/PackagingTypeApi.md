# Trollweb\VismaNetApi\PackagingTypeApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**packagingTypeGetAllPackagingTypes**](PackagingTypeApi.md#packagingTypeGetAllPackagingTypes) | **GET** /controller/api/v1/packagingType | Get a range of SO PackagingTypes
[**packagingTypeGetByboxId**](PackagingTypeApi.md#packagingTypeGetByboxId) | **GET** /controller/api/v1/packagingType/{boxId} | Get a specific PackagingType


# **packagingTypeGetAllPackagingTypes**
> \Trollweb\VismaNetApi\Model\PackagingTypeDto[] packagingTypeGetAllPackagingTypes($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of SO PackagingTypes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PackagingTypeApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->packagingTypeGetAllPackagingTypes($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PackagingTypeApi->packagingTypeGetAllPackagingTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PackagingTypeDto[]**](../Model/PackagingTypeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **packagingTypeGetByboxId**
> \Trollweb\VismaNetApi\Model\PackagingTypeDto packagingTypeGetByboxId($box_id)

Get a specific PackagingType

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PackagingTypeApi();
$box_id = "box_id_example"; // string | Identifies the PackagingType

try {
    $result = $api_instance->packagingTypeGetByboxId($box_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PackagingTypeApi->packagingTypeGetByboxId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **box_id** | **string**| Identifies the PackagingType |

### Return type

[**\Trollweb\VismaNetApi\Model\PackagingTypeDto**](../Model/PackagingTypeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

