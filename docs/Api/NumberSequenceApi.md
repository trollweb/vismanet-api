# Trollweb\VismaNetApi\NumberSequenceApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**numberSequenceGetAll**](NumberSequenceApi.md#numberSequenceGetAll) | **GET** /controller/api/v1/numberSequence | Get a specific Numbering
[**numberSequenceGetBynumberingId**](NumberSequenceApi.md#numberSequenceGetBynumberingId) | **GET** /controller/api/v1/numberSequence/{numberingId} | Get a specific Numbering


# **numberSequenceGetAll**
> \Trollweb\VismaNetApi\Model\NumberingDto[] numberSequenceGetAll()

Get a specific Numbering

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\NumberSequenceApi();

try {
    $result = $api_instance->numberSequenceGetAll();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumberSequenceApi->numberSequenceGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\NumberingDto[]**](../Model/NumberingDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **numberSequenceGetBynumberingId**
> \Trollweb\VismaNetApi\Model\NumberingDto numberSequenceGetBynumberingId($numbering_id)

Get a specific Numbering

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\NumberSequenceApi();
$numbering_id = "numbering_id_example"; // string | Identifies the Numbering

try {
    $result = $api_instance->numberSequenceGetBynumberingId($numbering_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumberSequenceApi->numberSequenceGetBynumberingId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numbering_id** | **string**| Identifies the Numbering |

### Return type

[**\Trollweb\VismaNetApi\Model\NumberingDto**](../Model/NumberingDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

