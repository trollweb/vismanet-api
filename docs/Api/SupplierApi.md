# Trollweb\VismaNetApi\SupplierApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierGetAll**](SupplierApi.md#supplierGetAll) | **GET** /controller/api/v1/supplier | Get a range of suppliers
[**supplierGetAllContactForSupplierBysupplierCd**](SupplierApi.md#supplierGetAllContactForSupplierBysupplierCd) | **GET** /controller/api/v1/supplier/{supplierCd}/contact | Get a range of a Supplier of a specific supplier
[**supplierGetAllDocumentsForSupplierBysupplierNumber**](SupplierApi.md#supplierGetAllDocumentsForSupplierBysupplierNumber) | **GET** /controller/api/v1/supplier/{supplierNumber}/document | Gets a range of documents for a specific supplier
[**supplierGetAllInvoicesForSupplierBysupplierNumber**](SupplierApi.md#supplierGetAllInvoicesForSupplierBysupplierNumber) | **GET** /controller/api/v1/supplier/{supplierNumber}/invoice | Get a range of invoices for a specific supplier
[**supplierGetAllSupplierBalance**](SupplierApi.md#supplierGetAllSupplierBalance) | **GET** /controller/api/v1/supplier/balance | Get the balance of a range of suppliers
[**supplierGetAllSupplierClasses**](SupplierApi.md#supplierGetAllSupplierClasses) | **GET** /controller/api/v1/supplier/supplierClass | Get supplier classes
[**supplierGetAllSupplierPOBalance**](SupplierApi.md#supplierGetAllSupplierPOBalance) | **GET** /controller/api/v1/supplier/POBalance | Get the PO balance of a range of suppliers
[**supplierGetBysupplierCd**](SupplierApi.md#supplierGetBysupplierCd) | **GET** /controller/api/v1/supplier/{supplierCd} | Get a specific supplier
[**supplierGetSpecificSupplierClassBysupplierClassId**](SupplierApi.md#supplierGetSpecificSupplierClassBysupplierClassId) | **GET** /controller/api/v1/supplier/supplierClass/{supplierClassId} | Get a specific supplier class
[**supplierGetSupplierBalanceBysupplierCd**](SupplierApi.md#supplierGetSupplierBalanceBysupplierCd) | **GET** /controller/api/v1/supplier/{supplierCd}/balance | Get a specific supplier&#39;s PO balance
[**supplierGetSupplierPOBalanceBysupplierCd**](SupplierApi.md#supplierGetSupplierPOBalanceBysupplierCd) | **GET** /controller/api/v1/supplier/{supplierCd}/POBalance | Get a specific supplier&#39;s PO balance
[**supplierPost**](SupplierApi.md#supplierPost) | **POST** /controller/api/v1/supplier | Create a supplier
[**supplierPutBysupplierCd**](SupplierApi.md#supplierPutBysupplierCd) | **PUT** /controller/api/v1/supplier/{supplierCd} | Update a specific supplier


# **supplierGetAll**
> \Trollweb\VismaNetApi\Model\SupplierDto[] supplierGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $name, $status, $vat_registration_id, $corporate_id, $attributes)

Get a range of suppliers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$name = "name_example"; // string | 
$status = "status_example"; // string | 
$vat_registration_id = "vat_registration_id_example"; // string | 
$corporate_id = "corporate_id_example"; // string | 
$attributes = "attributes_example"; // string | 

try {
    $result = $api_instance->supplierGetAll($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $name, $status, $vat_registration_id, $corporate_id, $attributes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **name** | **string**|  | [optional]
 **status** | **string**|  | [optional]
 **vat_registration_id** | **string**|  | [optional]
 **corporate_id** | **string**|  | [optional]
 **attributes** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierDto[]**](../Model/SupplierDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetAllContactForSupplierBysupplierCd**
> \Trollweb\VismaNetApi\Model\ContactDto[] supplierGetAllContactForSupplierBysupplierCd($supplier_cd, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of a Supplier of a specific supplier

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_cd = "supplier_cd_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->supplierGetAllContactForSupplierBysupplierCd($supplier_cd, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAllContactForSupplierBysupplierCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_cd** | **string**|  |
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ContactDto[]**](../Model/ContactDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetAllDocumentsForSupplierBysupplierNumber**
> \Trollweb\VismaNetApi\Model\SupplierDocumentDto[] supplierGetAllDocumentsForSupplierBysupplierNumber($supplier_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Gets a range of documents for a specific supplier

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_number = "supplier_number_example"; // string | Identifies the supplier for which to return data
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->supplierGetAllDocumentsForSupplierBysupplierNumber($supplier_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAllDocumentsForSupplierBysupplierNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_number** | **string**| Identifies the supplier for which to return data |
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierDocumentDto[]**](../Model/SupplierDocumentDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetAllInvoicesForSupplierBysupplierNumber**
> \Trollweb\VismaNetApi\Model\SupplierInvoiceDto[] supplierGetAllInvoicesForSupplierBysupplierNumber($supplier_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of invoices for a specific supplier

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_number = "supplier_number_example"; // string | Identifies the supplier for which to return data
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->supplierGetAllInvoicesForSupplierBysupplierNumber($supplier_number, $document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAllInvoicesForSupplierBysupplierNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_number** | **string**| Identifies the supplier for which to return data |
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierInvoiceDto[]**](../Model/SupplierInvoiceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetAllSupplierBalance**
> \Trollweb\VismaNetApi\Model\SupplierBalanceDto[] supplierGetAllSupplierBalance($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get the balance of a range of suppliers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->supplierGetAllSupplierBalance($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAllSupplierBalance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierBalanceDto[]**](../Model/SupplierBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetAllSupplierClasses**
> \Trollweb\VismaNetApi\Model\SupplierClassDto[] supplierGetAllSupplierClasses()

Get supplier classes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();

try {
    $result = $api_instance->supplierGetAllSupplierClasses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAllSupplierClasses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierClassDto[]**](../Model/SupplierClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetAllSupplierPOBalance**
> \Trollweb\VismaNetApi\Model\SupplierPOBalanceDto[] supplierGetAllSupplierPOBalance($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get the PO balance of a range of suppliers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->supplierGetAllSupplierPOBalance($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetAllSupplierPOBalance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierPOBalanceDto[]**](../Model/SupplierPOBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetBysupplierCd**
> \Trollweb\VismaNetApi\Model\SupplierDto supplierGetBysupplierCd($supplier_cd)

Get a specific supplier

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_cd = "supplier_cd_example"; // string | Identifies the supplier

try {
    $result = $api_instance->supplierGetBysupplierCd($supplier_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetBysupplierCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_cd** | **string**| Identifies the supplier |

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierDto**](../Model/SupplierDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetSpecificSupplierClassBysupplierClassId**
> \Trollweb\VismaNetApi\Model\SupplierClassDto supplierGetSpecificSupplierClassBysupplierClassId($supplier_class_id)

Get a specific supplier class

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_class_id = "supplier_class_id_example"; // string | Identifies the supplier class

try {
    $result = $api_instance->supplierGetSpecificSupplierClassBysupplierClassId($supplier_class_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetSpecificSupplierClassBysupplierClassId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_class_id** | **string**| Identifies the supplier class |

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierClassDto**](../Model/SupplierClassDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetSupplierBalanceBysupplierCd**
> \Trollweb\VismaNetApi\Model\SupplierBalanceDto supplierGetSupplierBalanceBysupplierCd($supplier_cd)

Get a specific supplier's PO balance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_cd = "supplier_cd_example"; // string | Indentifies the supplier for witch to return data

try {
    $result = $api_instance->supplierGetSupplierBalanceBysupplierCd($supplier_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetSupplierBalanceBysupplierCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_cd** | **string**| Indentifies the supplier for witch to return data |

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierBalanceDto**](../Model/SupplierBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierGetSupplierPOBalanceBysupplierCd**
> \Trollweb\VismaNetApi\Model\SupplierPOBalanceDto supplierGetSupplierPOBalanceBysupplierCd($supplier_cd)

Get a specific supplier's PO balance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_cd = "supplier_cd_example"; // string | Indentifies the supplier for witch to return data

try {
    $result = $api_instance->supplierGetSupplierPOBalanceBysupplierCd($supplier_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierGetSupplierPOBalanceBysupplierCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_cd** | **string**| Indentifies the supplier for witch to return data |

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierPOBalanceDto**](../Model/SupplierPOBalanceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierPost**
> \Trollweb\VismaNetApi\Model\Object supplierPost($supplier)

Create a supplier

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier = new \Trollweb\VismaNetApi\Model\SupplierUpdateDto(); // \Trollweb\VismaNetApi\Model\SupplierUpdateDto | Define the data for the supplier to create

try {
    $result = $api_instance->supplierPost($supplier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier** | [**\Trollweb\VismaNetApi\Model\SupplierUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SupplierUpdateDto.md)| Define the data for the supplier to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierPutBysupplierCd**
> \Trollweb\VismaNetApi\Model\Object supplierPutBysupplierCd($supplier_cd, $supplier)

Update a specific supplier

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierApi();
$supplier_cd = "supplier_cd_example"; // string | Identifies the supplier to update
$supplier = new \Trollweb\VismaNetApi\Model\SupplierUpdateDto(); // \Trollweb\VismaNetApi\Model\SupplierUpdateDto | The data to update for supplier

try {
    $result = $api_instance->supplierPutBysupplierCd($supplier_cd, $supplier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierApi->supplierPutBysupplierCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_cd** | **string**| Identifies the supplier to update |
 **supplier** | [**\Trollweb\VismaNetApi\Model\SupplierUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SupplierUpdateDto.md)| The data to update for supplier |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

