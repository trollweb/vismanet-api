# Trollweb\VismaNetApi\SupplierInvoiceApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**supplierInvoiceCreateHeaderAttachmentByinvoiceNumber**](SupplierInvoiceApi.md#supplierInvoiceCreateHeaderAttachmentByinvoiceNumber) | **POST** /controller/api/v1/supplierInvoice/{invoiceNumber}/attachment | Creates an attachment and associates it with a supplier invoice. If the file already exists, a new revision is created.
[**supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber**](SupplierInvoiceApi.md#supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber) | **POST** /controller/api/v1/supplierInvoice/{invoiceNumber}/{lineNumber}/attachment | Creates an attachment and associates it with a certain supplier invoice line. If the file already exists, a new revision is created.
[**supplierInvoiceGetAllInvoices**](SupplierInvoiceApi.md#supplierInvoiceGetAllInvoices) | **GET** /controller/api/v1/supplierInvoice | Get a range of Invoices
[**supplierInvoiceGetByinvoiceNumber**](SupplierInvoiceApi.md#supplierInvoiceGetByinvoiceNumber) | **GET** /controller/api/v1/supplierInvoice/{invoiceNumber} | Get a specific Invoice
[**supplierInvoicePost**](SupplierInvoiceApi.md#supplierInvoicePost) | **POST** /controller/api/v1/supplierInvoice | Create an SupplierInvoice
[**supplierInvoicePutBysupplierInvoiceNumber**](SupplierInvoiceApi.md#supplierInvoicePutBysupplierInvoiceNumber) | **PUT** /controller/api/v1/supplierInvoice/{supplierInvoiceNumber} | Update a specific SupplierInvoice
[**supplierInvoiceReleaseInvoiceByinvoiceNumber**](SupplierInvoiceApi.md#supplierInvoiceReleaseInvoiceByinvoiceNumber) | **POST** /controller/api/v1/supplierInvoice/{invoiceNumber}/action/release | Release invoice operation


# **supplierInvoiceCreateHeaderAttachmentByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\Object supplierInvoiceCreateHeaderAttachmentByinvoiceNumber($invoice_number)

Creates an attachment and associates it with a supplier invoice. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the supplier invoice

try {
    $result = $api_instance->supplierInvoiceCreateHeaderAttachmentByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoiceCreateHeaderAttachmentByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the supplier invoice |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber**
> \Trollweb\VismaNetApi\Model\Object supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber($invoice_number, $line_number)

Creates an attachment and associates it with a certain supplier invoice line. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the supplier invoice
$line_number = 56; // int | Specifies line number

try {
    $result = $api_instance->supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber($invoice_number, $line_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the supplier invoice |
 **line_number** | **int**| Specifies line number |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceGetAllInvoices**
> \Trollweb\VismaNetApi\Model\SupplierInvoiceDto[] supplierInvoiceGetAllInvoices($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition)

Get a range of Invoices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$document_type = "document_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$released = 56; // int | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 
$dunning_level = 56; // int | 
$closed_financial_period = "closed_financial_period_example"; // string | 
$dunning_letter_date_time = "dunning_letter_date_time_example"; // string | 
$dunning_letter_date_time_condition = "dunning_letter_date_time_condition_example"; // string | 

try {
    $result = $api_instance->supplierInvoiceGetAllInvoices($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoiceGetAllInvoices: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **document_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **released** | **int**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]
 **dunning_level** | **int**|  | [optional]
 **closed_financial_period** | **string**|  | [optional]
 **dunning_letter_date_time** | **string**|  | [optional]
 **dunning_letter_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierInvoiceDto[]**](../Model/SupplierInvoiceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceGetByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\SupplierInvoiceDto supplierInvoiceGetByinvoiceNumber($invoice_number)

Get a specific Invoice

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Identifies the Invoice

try {
    $result = $api_instance->supplierInvoiceGetByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoiceGetByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Identifies the Invoice |

### Return type

[**\Trollweb\VismaNetApi\Model\SupplierInvoiceDto**](../Model/SupplierInvoiceDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoicePost**
> \Trollweb\VismaNetApi\Model\Object supplierInvoicePost($supplier_invoice)

Create an SupplierInvoice

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$supplier_invoice = new \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto(); // \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto | Defines the data for the SupplierInvoice to create

try {
    $result = $api_instance->supplierInvoicePost($supplier_invoice);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoicePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_invoice** | [**\Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto.md)| Defines the data for the SupplierInvoice to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoicePutBysupplierInvoiceNumber**
> \Trollweb\VismaNetApi\Model\Object supplierInvoicePutBysupplierInvoiceNumber($supplier_invoice_number, $supplier_invoice)

Update a specific SupplierInvoice

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$supplier_invoice_number = "supplier_invoice_number_example"; // string | Identifies the SupplierInvoice to update
$supplier_invoice = new \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto(); // \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto | Defines the data for the Invoice to update

try {
    $result = $api_instance->supplierInvoicePutBysupplierInvoiceNumber($supplier_invoice_number, $supplier_invoice);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoicePutBysupplierInvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supplier_invoice_number** | **string**| Identifies the SupplierInvoice to update |
 **supplier_invoice** | [**\Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto.md)| Defines the data for the Invoice to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **supplierInvoiceReleaseInvoiceByinvoiceNumber**
> \Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto supplierInvoiceReleaseInvoiceByinvoiceNumber($invoice_number)

Release invoice operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SupplierInvoiceApi();
$invoice_number = "invoice_number_example"; // string | Reference number of the released invoice to be reversed

try {
    $result = $api_instance->supplierInvoiceReleaseInvoiceByinvoiceNumber($invoice_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SupplierInvoiceApi->supplierInvoiceReleaseInvoiceByinvoiceNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoice_number** | **string**| Reference number of the released invoice to be reversed |

### Return type

[**\Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto**](../Model/ReleaseSupplierInvoiceActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

