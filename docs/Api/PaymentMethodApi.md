# Trollweb\VismaNetApi\PaymentMethodApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**paymentMethodGetAllPaymentMethod**](PaymentMethodApi.md#paymentMethodGetAllPaymentMethod) | **GET** /controller/api/v1/paymentmethod | Get a range of Payment Method
[**paymentMethodGetBypaymentMethodNumber**](PaymentMethodApi.md#paymentMethodGetBypaymentMethodNumber) | **GET** /controller/api/v1/paymentmethod/{paymentMethodNumber} | Get a specific Payment Method


# **paymentMethodGetAllPaymentMethod**
> \Trollweb\VismaNetApi\Model\PaymentMethodDto[] paymentMethodGetAllPaymentMethod($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Payment Method

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentMethodApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->paymentMethodGetAllPaymentMethod($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentMethodApi->paymentMethodGetAllPaymentMethod: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\PaymentMethodDto[]**](../Model/PaymentMethodDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **paymentMethodGetBypaymentMethodNumber**
> \Trollweb\VismaNetApi\Model\PaymentMethodDto paymentMethodGetBypaymentMethodNumber($payment_method_number)

Get a specific Payment Method

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\PaymentMethodApi();
$payment_method_number = "payment_method_number_example"; // string | Identifies the Payment Method

try {
    $result = $api_instance->paymentMethodGetBypaymentMethodNumber($payment_method_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PaymentMethodApi->paymentMethodGetBypaymentMethodNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_method_number** | **string**| Identifies the Payment Method |

### Return type

[**\Trollweb\VismaNetApi\Model\PaymentMethodDto**](../Model/PaymentMethodDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

