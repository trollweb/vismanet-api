# Trollweb\VismaNetApi\ProjectApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**projectGetAll**](ProjectApi.md#projectGetAll) | **GET** /controller/api/v1/project | Get a range of Projects
[**projectGetByprojectID**](ProjectApi.md#projectGetByprojectID) | **GET** /controller/api/v1/project/{projectID} | Get a specific Project
[**projectPost**](ProjectApi.md#projectPost) | **POST** /controller/api/v1/project | Create an project
[**projectPutByprojectId**](ProjectApi.md#projectPutByprojectId) | **PUT** /controller/api/v1/project/{projectId} | Update a specific Project


# **projectGetAll**
> \Trollweb\VismaNetApi\Model\ProjectDto[] projectGetAll($status)

Get a range of Projects

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ProjectApi();
$status = "status_example"; // string | 

try {
    $result = $api_instance->projectGetAll($status);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectGetAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ProjectDto[]**](../Model/ProjectDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectGetByprojectID**
> \Trollweb\VismaNetApi\Model\ProjectDto projectGetByprojectID($project_id)

Get a specific Project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ProjectApi();
$project_id = "project_id_example"; // string | Identifies the Project

try {
    $result = $api_instance->projectGetByprojectID($project_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectGetByprojectID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **string**| Identifies the Project |

### Return type

[**\Trollweb\VismaNetApi\Model\ProjectDto**](../Model/ProjectDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectPost**
> \Trollweb\VismaNetApi\Model\Object projectPost($project_update_dto)

Create an project

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ProjectApi();
$project_update_dto = new \Trollweb\VismaNetApi\Model\ProjectUpdateDto(); // \Trollweb\VismaNetApi\Model\ProjectUpdateDto | Define the data for the project to create

try {
    $result = $api_instance->projectPost($project_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_update_dto** | [**\Trollweb\VismaNetApi\Model\ProjectUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ProjectUpdateDto.md)| Define the data for the project to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **projectPutByprojectId**
> \Trollweb\VismaNetApi\Model\Object projectPutByprojectId($project_id, $project_update_dto)

Update a specific Project

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\ProjectApi();
$project_id = "project_id_example"; // string | Identifies the Project to update
$project_update_dto = new \Trollweb\VismaNetApi\Model\ProjectUpdateDto(); // \Trollweb\VismaNetApi\Model\ProjectUpdateDto | Defines the data for the Project to update

try {
    $result = $api_instance->projectPutByprojectId($project_id, $project_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectApi->projectPutByprojectId: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **string**| Identifies the Project to update |
 **project_update_dto** | [**\Trollweb\VismaNetApi\Model\ProjectUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\ProjectUpdateDto.md)| Defines the data for the Project to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

