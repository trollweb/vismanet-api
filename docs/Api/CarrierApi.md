# Trollweb\VismaNetApi\CarrierApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**carrierGetAllCarriers**](CarrierApi.md#carrierGetAllCarriers) | **GET** /controller/api/v1/carrier | Get a range of Carriers
[**carrierGetCarrierBycarrierName**](CarrierApi.md#carrierGetCarrierBycarrierName) | **GET** /controller/api/v1/carrier/{carrierName} | Get a specific Carrier


# **carrierGetAllCarriers**
> \Trollweb\VismaNetApi\Model\CarrierDto[] carrierGetAllCarriers($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of Carriers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CarrierApi();
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->carrierGetAllCarriers($number_to_read, $skip_records, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CarrierApi->carrierGetAllCarriers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\CarrierDto[]**](../Model/CarrierDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **carrierGetCarrierBycarrierName**
> \Trollweb\VismaNetApi\Model\CarrierDto carrierGetCarrierBycarrierName($carrier_name)

Get a specific Carrier

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\CarrierApi();
$carrier_name = "carrier_name_example"; // string | Identifies the Carrier

try {
    $result = $api_instance->carrierGetCarrierBycarrierName($carrier_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CarrierApi->carrierGetCarrierBycarrierName: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **carrier_name** | **string**| Identifies the Carrier |

### Return type

[**\Trollweb\VismaNetApi\Model\CarrierDto**](../Model/CarrierDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

