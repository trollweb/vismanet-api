# Trollweb\VismaNetApi\SalesOrderApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**salesOrderCreateHeaderAttachmentByorderNumber**](SalesOrderApi.md#salesOrderCreateHeaderAttachmentByorderNumber) | **POST** /controller/api/v1/salesorder/{orderNumber}/attachment | Creates an attachment and associates it with a sales order. If the file already exists, a new revision is created.
[**salesOrderCreateLineAttachmentByorderNumberlineNumber**](SalesOrderApi.md#salesOrderCreateLineAttachmentByorderNumberlineNumber) | **POST** /controller/api/v1/salesorder/{orderNumber}/{lineNumber}/attachment | Creates an attachment and associates it with a certain sales order line. If the file already exists, a new revision is created.
[**salesOrderCreateShipmentActionBysaleOrderNumber**](SalesOrderApi.md#salesOrderCreateShipmentActionBysaleOrderNumber) | **POST** /controller/api/v1/salesorder/{saleOrderNumber}/action/createShipment | Crete shipment operation
[**salesOrderGetAllOrders**](SalesOrderApi.md#salesOrderGetAllOrders) | **GET** /controller/api/v1/salesorder | Get a range of SO Orders
[**salesOrderGetByorderNbr**](SalesOrderApi.md#salesOrderGetByorderNbr) | **GET** /controller/api/v1/salesorder/{orderNbr} | Get a specific SO Order
[**salesOrderPost**](SalesOrderApi.md#salesOrderPost) | **POST** /controller/api/v1/salesorder | Create a Sale Order
[**salesOrderPutBysalesOrderNumber**](SalesOrderApi.md#salesOrderPutBysalesOrderNumber) | **PUT** /controller/api/v1/salesorder/{salesOrderNumber} | Update a specific Sale Order


# **salesOrderCreateHeaderAttachmentByorderNumber**
> \Trollweb\VismaNetApi\Model\Object salesOrderCreateHeaderAttachmentByorderNumber($order_number)

Creates an attachment and associates it with a sales order. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$order_number = "order_number_example"; // string | Identifies the sales order

try {
    $result = $api_instance->salesOrderCreateHeaderAttachmentByorderNumber($order_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderCreateHeaderAttachmentByorderNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Identifies the sales order |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderCreateLineAttachmentByorderNumberlineNumber**
> \Trollweb\VismaNetApi\Model\Object salesOrderCreateLineAttachmentByorderNumberlineNumber($order_number, $line_number)

Creates an attachment and associates it with a certain sales order line. If the file already exists, a new revision is created.

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$order_number = "order_number_example"; // string | Identifies the sales order
$line_number = 56; // int | Specifies line number

try {
    $result = $api_instance->salesOrderCreateLineAttachmentByorderNumberlineNumber($order_number, $line_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderCreateLineAttachmentByorderNumberlineNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_number** | **string**| Identifies the sales order |
 **line_number** | **int**| Specifies line number |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderCreateShipmentActionBysaleOrderNumber**
> \Trollweb\VismaNetApi\Model\CreateShipmentActionResultDto salesOrderCreateShipmentActionBysaleOrderNumber($sale_order_number, $create_shipment_action_dto)

Crete shipment operation

The action result dto contains information about the result of running the action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$sale_order_number = "sale_order_number_example"; // string | Reference number of the sale oreder from which the shipment will be created
$create_shipment_action_dto = new \Trollweb\VismaNetApi\Model\CreateShipmentActionDto(); // \Trollweb\VismaNetApi\Model\CreateShipmentActionDto | Defines the data for the action

try {
    $result = $api_instance->salesOrderCreateShipmentActionBysaleOrderNumber($sale_order_number, $create_shipment_action_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderCreateShipmentActionBysaleOrderNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sale_order_number** | **string**| Reference number of the sale oreder from which the shipment will be created |
 **create_shipment_action_dto** | [**\Trollweb\VismaNetApi\Model\CreateShipmentActionDto**](../Model/\Trollweb\VismaNetApi\Model\CreateShipmentActionDto.md)| Defines the data for the action |

### Return type

[**\Trollweb\VismaNetApi\Model\CreateShipmentActionResultDto**](../Model/CreateShipmentActionResultDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderGetAllOrders**
> \Trollweb\VismaNetApi\Model\SalesOrderDto[] salesOrderGetAllOrders($order_type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of SO Orders

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$order_type = "order_type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->salesOrderGetAllOrders($order_type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderGetAllOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\SalesOrderDto[]**](../Model/SalesOrderDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderGetByorderNbr**
> \Trollweb\VismaNetApi\Model\SalesOrderDto salesOrderGetByorderNbr($order_nbr)

Get a specific SO Order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$order_nbr = "order_nbr_example"; // string | Identifies the So Order

try {
    $result = $api_instance->salesOrderGetByorderNbr($order_nbr);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderGetByorderNbr: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_nbr** | **string**| Identifies the So Order |

### Return type

[**\Trollweb\VismaNetApi\Model\SalesOrderDto**](../Model/SalesOrderDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderPost**
> \Trollweb\VismaNetApi\Model\Object salesOrderPost($sale_order_update_dto)

Create a Sale Order

Response Message has StatusCode Created if POST operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$sale_order_update_dto = new \Trollweb\VismaNetApi\Model\SalesOrderUpdateDto(); // \Trollweb\VismaNetApi\Model\SalesOrderUpdateDto | Defines the data for the Sale Order to create

try {
    $result = $api_instance->salesOrderPost($sale_order_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sale_order_update_dto** | [**\Trollweb\VismaNetApi\Model\SalesOrderUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SalesOrderUpdateDto.md)| Defines the data for the Sale Order to create |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **salesOrderPutBysalesOrderNumber**
> \Trollweb\VismaNetApi\Model\Object salesOrderPutBysalesOrderNumber($sales_order_number, $sale_order_update_dto)

Update a specific Sale Order

Response Message has StatusCode NoContent if PUT operation succeed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\SalesOrderApi();
$sales_order_number = "sales_order_number_example"; // string | Identifies the Sale Order to update
$sale_order_update_dto = new \Trollweb\VismaNetApi\Model\SalesOrderUpdateDto(); // \Trollweb\VismaNetApi\Model\SalesOrderUpdateDto | Defines the data for the Sale Order to update

try {
    $result = $api_instance->salesOrderPutBysalesOrderNumber($sales_order_number, $sale_order_update_dto);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SalesOrderApi->salesOrderPutBysalesOrderNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sales_order_number** | **string**| Identifies the Sale Order to update |
 **sale_order_update_dto** | [**\Trollweb\VismaNetApi\Model\SalesOrderUpdateDto**](../Model/\Trollweb\VismaNetApi\Model\SalesOrderUpdateDto.md)| Defines the data for the Sale Order to update |

### Return type

[**\Trollweb\VismaNetApi\Model\Object**](../Model/Object.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/xml, text/xml, application/x-www-form-urlencoded
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

