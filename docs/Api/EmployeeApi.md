# Trollweb\VismaNetApi\EmployeeApi

All URIs are relative to *https://localhost/API*

Method | HTTP request | Description
------------- | ------------- | -------------
[**employeeGetAllEmplyee**](EmployeeApi.md#employeeGetAllEmplyee) | **GET** /controller/api/v1/employee | Get a range of employees
[**employeeGetEmployeeByemployeeCd**](EmployeeApi.md#employeeGetEmployeeByemployeeCd) | **GET** /controller/api/v1/employee/{employeeCd} | Get a specific employee
[**employeeGetEmployeeExpenseClaimsByemployeeCd**](EmployeeApi.md#employeeGetEmployeeExpenseClaimsByemployeeCd) | **GET** /controller/api/v1/employee/{employeeCd}/expenseClaim | Get expense claims for a specific employee
[**employeeGetEmployeeExpenseReceiptsByemployeeCd**](EmployeeApi.md#employeeGetEmployeeExpenseReceiptsByemployeeCd) | **GET** /controller/api/v1/employee/{employeeCd}/expenseReceipt | Get expense receipts for a specific employee
[**employeeGetEmployeeTimeCardsByemployeeCd**](EmployeeApi.md#employeeGetEmployeeTimeCardsByemployeeCd) | **GET** /controller/api/v1/employee/{employeeCd}/timecards | Get a specific employee time cards


# **employeeGetAllEmplyee**
> \Trollweb\VismaNetApi\Model\EmployeeDto[] employeeGetAllEmplyee($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a range of employees

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\EmployeeApi();
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->employeeGetAllEmplyee($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeeApi->employeeGetAllEmplyee: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\EmployeeDto[]**](../Model/EmployeeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **employeeGetEmployeeByemployeeCd**
> \Trollweb\VismaNetApi\Model\EmployeeDto employeeGetEmployeeByemployeeCd($employee_cd)

Get a specific employee

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\EmployeeApi();
$employee_cd = "employee_cd_example"; // string | Identifies the employee

try {
    $result = $api_instance->employeeGetEmployeeByemployeeCd($employee_cd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeeApi->employeeGetEmployeeByemployeeCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employee_cd** | **string**| Identifies the employee |

### Return type

[**\Trollweb\VismaNetApi\Model\EmployeeDto**](../Model/EmployeeDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **employeeGetEmployeeExpenseClaimsByemployeeCd**
> \Trollweb\VismaNetApi\Model\ExpenseClaimDto[] employeeGetEmployeeExpenseClaimsByemployeeCd($employee_cd, $status, $date, $customer, $department_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get expense claims for a specific employee

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\EmployeeApi();
$employee_cd = "employee_cd_example"; // string | Identifies the employee
$status = "status_example"; // string | 
$date = new \DateTime(); // \DateTime | 
$customer = "customer_example"; // string | 
$department_id = "department_id_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->employeeGetEmployeeExpenseClaimsByemployeeCd($employee_cd, $status, $date, $customer, $department_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeeApi->employeeGetEmployeeExpenseClaimsByemployeeCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employee_cd** | **string**| Identifies the employee |
 **status** | **string**|  | [optional]
 **date** | **\DateTime**|  | [optional]
 **customer** | **string**|  | [optional]
 **department_id** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ExpenseClaimDto[]**](../Model/ExpenseClaimDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **employeeGetEmployeeExpenseReceiptsByemployeeCd**
> \Trollweb\VismaNetApi\Model\ExpenseReceiptDto[] employeeGetEmployeeExpenseReceiptsByemployeeCd($employee_cd, $date, $date_condition, $inventory, $project, $claimed_by, $project_task, $invoiceable, $status, $customer)

Get expense receipts for a specific employee

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\EmployeeApi();
$employee_cd = "employee_cd_example"; // string | Identifies the employee
$date = "date_example"; // string | 
$date_condition = "date_condition_example"; // string | 
$inventory = "inventory_example"; // string | 
$project = "project_example"; // string | 
$claimed_by = "claimed_by_example"; // string | 
$project_task = "project_task_example"; // string | 
$invoiceable = true; // bool | 
$status = "status_example"; // string | 
$customer = "customer_example"; // string | 

try {
    $result = $api_instance->employeeGetEmployeeExpenseReceiptsByemployeeCd($employee_cd, $date, $date_condition, $inventory, $project, $claimed_by, $project_task, $invoiceable, $status, $customer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeeApi->employeeGetEmployeeExpenseReceiptsByemployeeCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employee_cd** | **string**| Identifies the employee |
 **date** | **string**|  | [optional]
 **date_condition** | **string**|  | [optional]
 **inventory** | **string**|  | [optional]
 **project** | **string**|  | [optional]
 **claimed_by** | **string**|  | [optional]
 **project_task** | **string**|  | [optional]
 **invoiceable** | **bool**|  | [optional]
 **status** | **string**|  | [optional]
 **customer** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\ExpenseReceiptDto[]**](../Model/ExpenseReceiptDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json, application/xml, text/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **employeeGetEmployeeTimeCardsByemployeeCd**
> \Trollweb\VismaNetApi\Model\TimeCardDto[] employeeGetEmployeeTimeCardsByemployeeCd($employee_cd, $status, $week, $type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition)

Get a specific employee time cards

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: vna_oauth
Trollweb\VismaNetApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Trollweb\VismaNetApi\Api\EmployeeApi();
$employee_cd = "employee_cd_example"; // string | Identifies the employee
$status = "status_example"; // string | 
$week = "week_example"; // string | 
$type = "type_example"; // string | 
$greater_than_value = "greater_than_value_example"; // string | 
$number_to_read = 56; // int | 
$skip_records = 56; // int | 
$order_by = "order_by_example"; // string | 
$last_modified_date_time = "last_modified_date_time_example"; // string | 
$last_modified_date_time_condition = "last_modified_date_time_condition_example"; // string | 

try {
    $result = $api_instance->employeeGetEmployeeTimeCardsByemployeeCd($employee_cd, $status, $week, $type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EmployeeApi->employeeGetEmployeeTimeCardsByemployeeCd: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employee_cd** | **string**| Identifies the employee |
 **status** | **string**|  | [optional]
 **week** | **string**|  | [optional]
 **type** | **string**|  | [optional]
 **greater_than_value** | **string**|  | [optional]
 **number_to_read** | **int**|  | [optional]
 **skip_records** | **int**|  | [optional]
 **order_by** | **string**|  | [optional]
 **last_modified_date_time** | **string**|  | [optional]
 **last_modified_date_time_condition** | **string**|  | [optional]

### Return type

[**\Trollweb\VismaNetApi\Model\TimeCardDto[]**](../Model/TimeCardDto.md)

### Authorization

[vna_oauth](../../README.md#vna_oauth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

