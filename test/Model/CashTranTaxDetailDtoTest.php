<?php
/**
 * CashTranTaxDetailDtoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\VismaNetApi;

/**
 * CashTranTaxDetailDtoTest Class Doc Comment
 *
 * @category    Class */
// * @description CashTranTaxDetailDto
/**
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CashTranTaxDetailDtoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "CashTranTaxDetailDto"
     */
    public function testCashTranTaxDetailDto()
    {

    }

    /**
     * Test attribute "tax"
     */
    public function testPropertyTax()
    {

    }

    /**
     * Test attribute "tax_rate"
     */
    public function testPropertyTaxRate()
    {

    }

    /**
     * Test attribute "taxable_amount"
     */
    public function testPropertyTaxableAmount()
    {

    }

    /**
     * Test attribute "tax_amount"
     */
    public function testPropertyTaxAmount()
    {

    }

    /**
     * Test attribute "deductible_tax_rate"
     */
    public function testPropertyDeductibleTaxRate()
    {

    }

    /**
     * Test attribute "expense_amount"
     */
    public function testPropertyExpenseAmount()
    {

    }

    /**
     * Test attribute "include_in_vat_exempt_total"
     */
    public function testPropertyIncludeInVatExemptTotal()
    {

    }

    /**
     * Test attribute "pending_vat"
     */
    public function testPropertyPendingVat()
    {

    }

    /**
     * Test attribute "statistical_vat"
     */
    public function testPropertyStatisticalVat()
    {

    }

    /**
     * Test attribute "reverse_vat"
     */
    public function testPropertyReverseVat()
    {

    }

    /**
     * Test attribute "tax_type"
     */
    public function testPropertyTaxType()
    {

    }

}
