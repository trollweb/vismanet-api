<?php
/**
 * SupplierPOBalanceDtoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\VismaNetApi;

/**
 * SupplierPOBalanceDtoTest Class Doc Comment
 *
 * @category    Class */
// * @description SupplierPOBalanceDto
/**
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SupplierPOBalanceDtoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "SupplierPOBalanceDto"
     */
    public function testSupplierPOBalanceDto()
    {

    }

    /**
     * Test attribute "supplier"
     */
    public function testPropertySupplier()
    {

    }

    /**
     * Test attribute "total_po_on_hold_order_total"
     */
    public function testPropertyTotalPoOnHoldOrderTotal()
    {

    }

    /**
     * Test attribute "total_po_on_hold_line_total"
     */
    public function testPropertyTotalPoOnHoldLineTotal()
    {

    }

    /**
     * Test attribute "total_open_po_order_total"
     */
    public function testPropertyTotalOpenPoOrderTotal()
    {

    }

    /**
     * Test attribute "total_open_po_line_total"
     */
    public function testPropertyTotalOpenPoLineTotal()
    {

    }

    /**
     * Test attribute "total_closed_po_order_total"
     */
    public function testPropertyTotalClosedPoOrderTotal()
    {

    }

    /**
     * Test attribute "total_closed_po_line_total"
     */
    public function testPropertyTotalClosedPoLineTotal()
    {

    }

    /**
     * Test attribute "last_modified_date_time"
     */
    public function testPropertyLastModifiedDateTime()
    {

    }

}
