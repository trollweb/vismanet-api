<?php
/**
 * CustomerSalesPriceDtoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\VismaNetApi;

/**
 * CustomerSalesPriceDtoTest Class Doc Comment
 *
 * @category    Class */
// * @description This class represents a CustomerSalesPrice in CustomerSalesPriceController.
/**
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CustomerSalesPriceDtoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "CustomerSalesPriceDto"
     */
    public function testCustomerSalesPriceDto()
    {

    }

    /**
     * Test attribute "record_id"
     */
    public function testPropertyRecordId()
    {

    }

    /**
     * Test attribute "price_type"
     */
    public function testPropertyPriceType()
    {

    }

    /**
     * Test attribute "price_code"
     */
    public function testPropertyPriceCode()
    {

    }

    /**
     * Test attribute "inventory_id"
     */
    public function testPropertyInventoryId()
    {

    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {

    }

    /**
     * Test attribute "uo_m"
     */
    public function testPropertyUoM()
    {

    }

    /**
     * Test attribute "promotion"
     */
    public function testPropertyPromotion()
    {

    }

    /**
     * Test attribute "break_qty"
     */
    public function testPropertyBreakQty()
    {

    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {

    }

    /**
     * Test attribute "currency"
     */
    public function testPropertyCurrency()
    {

    }

    /**
     * Test attribute "vat"
     */
    public function testPropertyVat()
    {

    }

    /**
     * Test attribute "effective_date"
     */
    public function testPropertyEffectiveDate()
    {

    }

    /**
     * Test attribute "expiration_date"
     */
    public function testPropertyExpirationDate()
    {

    }

    /**
     * Test attribute "last_modified_date_time"
     */
    public function testPropertyLastModifiedDateTime()
    {

    }

}
