<?php
/**
 * StocktakeLineDtoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Trollweb\VismaNetApi;

/**
 * StocktakeLineDtoTest Class Doc Comment
 *
 * @category    Class */
// * @description StocktakeLineDto
/**
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class StocktakeLineDtoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {

    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {

    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {

    }

    /**
     * Test "StocktakeLineDto"
     */
    public function testStocktakeLineDto()
    {

    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {

    }

    /**
     * Test attribute "line_nbr"
     */
    public function testPropertyLineNbr()
    {

    }

    /**
     * Test attribute "tag_nbr"
     */
    public function testPropertyTagNbr()
    {

    }

    /**
     * Test attribute "inventory"
     */
    public function testPropertyInventory()
    {

    }

    /**
     * Test attribute "location"
     */
    public function testPropertyLocation()
    {

    }

    /**
     * Test attribute "warehouse"
     */
    public function testPropertyWarehouse()
    {

    }

    /**
     * Test attribute "lot_serial_nbr"
     */
    public function testPropertyLotSerialNbr()
    {

    }

    /**
     * Test attribute "expiration_date"
     */
    public function testPropertyExpirationDate()
    {

    }

    /**
     * Test attribute "book_quantity"
     */
    public function testPropertyBookQuantity()
    {

    }

    /**
     * Test attribute "physical_quantity"
     */
    public function testPropertyPhysicalQuantity()
    {

    }

    /**
     * Test attribute "variance_quantity"
     */
    public function testPropertyVarianceQuantity()
    {

    }

    /**
     * Test attribute "unit_cost"
     */
    public function testPropertyUnitCost()
    {

    }

    /**
     * Test attribute "ext_variance_cost"
     */
    public function testPropertyExtVarianceCost()
    {

    }

    /**
     * Test attribute "reason_code"
     */
    public function testPropertyReasonCode()
    {

    }

    /**
     * Test attribute "last_modified_date_time"
     */
    public function testPropertyLastModifiedDateTime()
    {

    }

}
