<?php
/**
 * JournalTransactionUpdateDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * JournalTransactionUpdateDto Class Doc Comment
 *
 * @category    Class */
 // @description This class represents a journal transaction in JournalTransactionController. Used for creating/updating data.
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class JournalTransactionUpdateDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'JournalTransactionUpdateDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'batch_number' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'hold' => '\Trollweb\VismaNetApi\Model\DtoValueBoolean',
        'transaction_date' => '\Trollweb\VismaNetApi\Model\DtoValueDateTime',
        'post_period' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'financial_period' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'ledger' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'currency_id' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'exchange_rate' => '\Trollweb\VismaNetApi\Model\DtoValueDecimal',
        'auto_reversing' => '\Trollweb\VismaNetApi\Model\DtoValueBoolean',
        'description' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'control_total_in_currency' => '\Trollweb\VismaNetApi\Model\DtoValueDecimal',
        'create_vat_transaction' => '\Trollweb\VismaNetApi\Model\DtoValueBoolean',
        'skip_vat_amount_validation' => '\Trollweb\VismaNetApi\Model\DtoValueBoolean',
        'transaction_code' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'branch' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'journal_transaction_lines' => '\Trollweb\VismaNetApi\Model\JournalTransactionLineUpdateDto[]'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'batch_number' => 'batchNumber',
        'hold' => 'hold',
        'transaction_date' => 'transactionDate',
        'post_period' => 'postPeriod',
        'financial_period' => 'financialPeriod',
        'ledger' => 'ledger',
        'currency_id' => 'currencyId',
        'exchange_rate' => 'exchangeRate',
        'auto_reversing' => 'autoReversing',
        'description' => 'description',
        'control_total_in_currency' => 'controlTotalInCurrency',
        'create_vat_transaction' => 'createVatTransaction',
        'skip_vat_amount_validation' => 'skipVatAmountValidation',
        'transaction_code' => 'transactionCode',
        'branch' => 'branch',
        'journal_transaction_lines' => 'journalTransactionLines'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'batch_number' => 'setBatchNumber',
        'hold' => 'setHold',
        'transaction_date' => 'setTransactionDate',
        'post_period' => 'setPostPeriod',
        'financial_period' => 'setFinancialPeriod',
        'ledger' => 'setLedger',
        'currency_id' => 'setCurrencyId',
        'exchange_rate' => 'setExchangeRate',
        'auto_reversing' => 'setAutoReversing',
        'description' => 'setDescription',
        'control_total_in_currency' => 'setControlTotalInCurrency',
        'create_vat_transaction' => 'setCreateVatTransaction',
        'skip_vat_amount_validation' => 'setSkipVatAmountValidation',
        'transaction_code' => 'setTransactionCode',
        'branch' => 'setBranch',
        'journal_transaction_lines' => 'setJournalTransactionLines'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'batch_number' => 'getBatchNumber',
        'hold' => 'getHold',
        'transaction_date' => 'getTransactionDate',
        'post_period' => 'getPostPeriod',
        'financial_period' => 'getFinancialPeriod',
        'ledger' => 'getLedger',
        'currency_id' => 'getCurrencyId',
        'exchange_rate' => 'getExchangeRate',
        'auto_reversing' => 'getAutoReversing',
        'description' => 'getDescription',
        'control_total_in_currency' => 'getControlTotalInCurrency',
        'create_vat_transaction' => 'getCreateVatTransaction',
        'skip_vat_amount_validation' => 'getSkipVatAmountValidation',
        'transaction_code' => 'getTransactionCode',
        'branch' => 'getBranch',
        'journal_transaction_lines' => 'getJournalTransactionLines'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['batch_number'] = isset($data['batch_number']) ? $data['batch_number'] : null;
        $this->container['hold'] = isset($data['hold']) ? $data['hold'] : null;
        $this->container['transaction_date'] = isset($data['transaction_date']) ? $data['transaction_date'] : null;
        $this->container['post_period'] = isset($data['post_period']) ? $data['post_period'] : null;
        $this->container['financial_period'] = isset($data['financial_period']) ? $data['financial_period'] : null;
        $this->container['ledger'] = isset($data['ledger']) ? $data['ledger'] : null;
        $this->container['currency_id'] = isset($data['currency_id']) ? $data['currency_id'] : null;
        $this->container['exchange_rate'] = isset($data['exchange_rate']) ? $data['exchange_rate'] : null;
        $this->container['auto_reversing'] = isset($data['auto_reversing']) ? $data['auto_reversing'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['control_total_in_currency'] = isset($data['control_total_in_currency']) ? $data['control_total_in_currency'] : null;
        $this->container['create_vat_transaction'] = isset($data['create_vat_transaction']) ? $data['create_vat_transaction'] : null;
        $this->container['skip_vat_amount_validation'] = isset($data['skip_vat_amount_validation']) ? $data['skip_vat_amount_validation'] : null;
        $this->container['transaction_code'] = isset($data['transaction_code']) ? $data['transaction_code'] : null;
        $this->container['branch'] = isset($data['branch']) ? $data['branch'] : null;
        $this->container['journal_transaction_lines'] = isset($data['journal_transaction_lines']) ? $data['journal_transaction_lines'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets batch_number
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getBatchNumber()
    {
        return $this->container['batch_number'];
    }

    /**
     * Sets batch_number
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $batch_number
     * @return $this
     */
    public function setBatchNumber($batch_number)
    {
        $this->container['batch_number'] = $batch_number;

        return $this;
    }

    /**
     * Gets hold
     * @return \Trollweb\VismaNetApi\Model\DtoValueBoolean
     */
    public function getHold()
    {
        return $this->container['hold'];
    }

    /**
     * Sets hold
     * @param \Trollweb\VismaNetApi\Model\DtoValueBoolean $hold
     * @return $this
     */
    public function setHold($hold)
    {
        $this->container['hold'] = $hold;

        return $this;
    }

    /**
     * Gets transaction_date
     * @return \Trollweb\VismaNetApi\Model\DtoValueDateTime
     */
    public function getTransactionDate()
    {
        return $this->container['transaction_date'];
    }

    /**
     * Sets transaction_date
     * @param \Trollweb\VismaNetApi\Model\DtoValueDateTime $transaction_date
     * @return $this
     */
    public function setTransactionDate($transaction_date)
    {
        $this->container['transaction_date'] = $transaction_date;

        return $this;
    }

    /**
     * Gets post_period
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getPostPeriod()
    {
        return $this->container['post_period'];
    }

    /**
     * Sets post_period
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $post_period The financial period to which the transactions recorded in the document should be posted. Use the format MMYYYY.
     * @return $this
     */
    public function setPostPeriod($post_period)
    {
        $this->container['post_period'] = $post_period;

        return $this;
    }

    /**
     * Gets financial_period
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getFinancialPeriod()
    {
        return $this->container['financial_period'];
    }

    /**
     * Sets financial_period
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $financial_period The financial period to which the transactions recorded in the document should be posted. Use the format YYYYMM.
     * @return $this
     */
    public function setFinancialPeriod($financial_period)
    {
        $this->container['financial_period'] = $financial_period;

        return $this;
    }

    /**
     * Gets ledger
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getLedger()
    {
        return $this->container['ledger'];
    }

    /**
     * Sets ledger
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $ledger
     * @return $this
     */
    public function setLedger($ledger)
    {
        $this->container['ledger'] = $ledger;

        return $this;
    }

    /**
     * Gets currency_id
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getCurrencyId()
    {
        return $this->container['currency_id'];
    }

    /**
     * Sets currency_id
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $currency_id
     * @return $this
     */
    public function setCurrencyId($currency_id)
    {
        $this->container['currency_id'] = $currency_id;

        return $this;
    }

    /**
     * Gets exchange_rate
     * @return \Trollweb\VismaNetApi\Model\DtoValueDecimal
     */
    public function getExchangeRate()
    {
        return $this->container['exchange_rate'];
    }

    /**
     * Sets exchange_rate
     * @param \Trollweb\VismaNetApi\Model\DtoValueDecimal $exchange_rate
     * @return $this
     */
    public function setExchangeRate($exchange_rate)
    {
        $this->container['exchange_rate'] = $exchange_rate;

        return $this;
    }

    /**
     * Gets auto_reversing
     * @return \Trollweb\VismaNetApi\Model\DtoValueBoolean
     */
    public function getAutoReversing()
    {
        return $this->container['auto_reversing'];
    }

    /**
     * Sets auto_reversing
     * @param \Trollweb\VismaNetApi\Model\DtoValueBoolean $auto_reversing
     * @return $this
     */
    public function setAutoReversing($auto_reversing)
    {
        $this->container['auto_reversing'] = $auto_reversing;

        return $this;
    }

    /**
     * Gets description
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets control_total_in_currency
     * @return \Trollweb\VismaNetApi\Model\DtoValueDecimal
     */
    public function getControlTotalInCurrency()
    {
        return $this->container['control_total_in_currency'];
    }

    /**
     * Sets control_total_in_currency
     * @param \Trollweb\VismaNetApi\Model\DtoValueDecimal $control_total_in_currency
     * @return $this
     */
    public function setControlTotalInCurrency($control_total_in_currency)
    {
        $this->container['control_total_in_currency'] = $control_total_in_currency;

        return $this;
    }

    /**
     * Gets create_vat_transaction
     * @return \Trollweb\VismaNetApi\Model\DtoValueBoolean
     */
    public function getCreateVatTransaction()
    {
        return $this->container['create_vat_transaction'];
    }

    /**
     * Sets create_vat_transaction
     * @param \Trollweb\VismaNetApi\Model\DtoValueBoolean $create_vat_transaction
     * @return $this
     */
    public function setCreateVatTransaction($create_vat_transaction)
    {
        $this->container['create_vat_transaction'] = $create_vat_transaction;

        return $this;
    }

    /**
     * Gets skip_vat_amount_validation
     * @return \Trollweb\VismaNetApi\Model\DtoValueBoolean
     */
    public function getSkipVatAmountValidation()
    {
        return $this->container['skip_vat_amount_validation'];
    }

    /**
     * Sets skip_vat_amount_validation
     * @param \Trollweb\VismaNetApi\Model\DtoValueBoolean $skip_vat_amount_validation
     * @return $this
     */
    public function setSkipVatAmountValidation($skip_vat_amount_validation)
    {
        $this->container['skip_vat_amount_validation'] = $skip_vat_amount_validation;

        return $this;
    }

    /**
     * Gets transaction_code
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getTransactionCode()
    {
        return $this->container['transaction_code'];
    }

    /**
     * Sets transaction_code
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $transaction_code
     * @return $this
     */
    public function setTransactionCode($transaction_code)
    {
        $this->container['transaction_code'] = $transaction_code;

        return $this;
    }

    /**
     * Gets branch
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getBranch()
    {
        return $this->container['branch'];
    }

    /**
     * Sets branch
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $branch
     * @return $this
     */
    public function setBranch($branch)
    {
        $this->container['branch'] = $branch;

        return $this;
    }

    /**
     * Gets journal_transaction_lines
     * @return \Trollweb\VismaNetApi\Model\JournalTransactionLineUpdateDto[]
     */
    public function getJournalTransactionLines()
    {
        return $this->container['journal_transaction_lines'];
    }

    /**
     * Sets journal_transaction_lines
     * @param \Trollweb\VismaNetApi\Model\JournalTransactionLineUpdateDto[] $journal_transaction_lines
     * @return $this
     */
    public function setJournalTransactionLines($journal_transaction_lines)
    {
        $this->container['journal_transaction_lines'] = $journal_transaction_lines;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


