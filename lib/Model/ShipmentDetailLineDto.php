<?php
/**
 * ShipmentDetailLineDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * ShipmentDetailLineDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ShipmentDetailLineDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ShipmentDetailLineDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'line_number' => 'int',
        'order_type' => 'string',
        'order_nbr' => 'string',
        'inventory_number' => 'string',
        'free_item' => 'bool',
        'warehouse' => '\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto',
        'location' => '\Trollweb\VismaNetApi\Model\LocationDto',
        'uom' => 'string',
        'shipped_qty' => 'double',
        'ordered_qty' => 'double',
        'open_qty' => 'double',
        'lot_serial_nbr' => 'string',
        'expiration_date' => '\DateTime',
        'reason_code' => 'string',
        'description' => 'string',
        'allocations' => '\Trollweb\VismaNetApi\Model\AllocationsDto[]'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'line_number' => 'lineNumber',
        'order_type' => 'orderType',
        'order_nbr' => 'orderNbr',
        'inventory_number' => 'inventoryNumber',
        'free_item' => 'freeItem',
        'warehouse' => 'warehouse',
        'location' => 'location',
        'uom' => 'uom',
        'shipped_qty' => 'shippedQty',
        'ordered_qty' => 'orderedQty',
        'open_qty' => 'openQty',
        'lot_serial_nbr' => 'lotSerialNbr',
        'expiration_date' => 'expirationDate',
        'reason_code' => 'reasonCode',
        'description' => 'description',
        'allocations' => 'allocations'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'line_number' => 'setLineNumber',
        'order_type' => 'setOrderType',
        'order_nbr' => 'setOrderNbr',
        'inventory_number' => 'setInventoryNumber',
        'free_item' => 'setFreeItem',
        'warehouse' => 'setWarehouse',
        'location' => 'setLocation',
        'uom' => 'setUom',
        'shipped_qty' => 'setShippedQty',
        'ordered_qty' => 'setOrderedQty',
        'open_qty' => 'setOpenQty',
        'lot_serial_nbr' => 'setLotSerialNbr',
        'expiration_date' => 'setExpirationDate',
        'reason_code' => 'setReasonCode',
        'description' => 'setDescription',
        'allocations' => 'setAllocations'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'line_number' => 'getLineNumber',
        'order_type' => 'getOrderType',
        'order_nbr' => 'getOrderNbr',
        'inventory_number' => 'getInventoryNumber',
        'free_item' => 'getFreeItem',
        'warehouse' => 'getWarehouse',
        'location' => 'getLocation',
        'uom' => 'getUom',
        'shipped_qty' => 'getShippedQty',
        'ordered_qty' => 'getOrderedQty',
        'open_qty' => 'getOpenQty',
        'lot_serial_nbr' => 'getLotSerialNbr',
        'expiration_date' => 'getExpirationDate',
        'reason_code' => 'getReasonCode',
        'description' => 'getDescription',
        'allocations' => 'getAllocations'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['line_number'] = isset($data['line_number']) ? $data['line_number'] : null;
        $this->container['order_type'] = isset($data['order_type']) ? $data['order_type'] : null;
        $this->container['order_nbr'] = isset($data['order_nbr']) ? $data['order_nbr'] : null;
        $this->container['inventory_number'] = isset($data['inventory_number']) ? $data['inventory_number'] : null;
        $this->container['free_item'] = isset($data['free_item']) ? $data['free_item'] : null;
        $this->container['warehouse'] = isset($data['warehouse']) ? $data['warehouse'] : null;
        $this->container['location'] = isset($data['location']) ? $data['location'] : null;
        $this->container['uom'] = isset($data['uom']) ? $data['uom'] : null;
        $this->container['shipped_qty'] = isset($data['shipped_qty']) ? $data['shipped_qty'] : null;
        $this->container['ordered_qty'] = isset($data['ordered_qty']) ? $data['ordered_qty'] : null;
        $this->container['open_qty'] = isset($data['open_qty']) ? $data['open_qty'] : null;
        $this->container['lot_serial_nbr'] = isset($data['lot_serial_nbr']) ? $data['lot_serial_nbr'] : null;
        $this->container['expiration_date'] = isset($data['expiration_date']) ? $data['expiration_date'] : null;
        $this->container['reason_code'] = isset($data['reason_code']) ? $data['reason_code'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['allocations'] = isset($data['allocations']) ? $data['allocations'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets line_number
     * @return int
     */
    public function getLineNumber()
    {
        return $this->container['line_number'];
    }

    /**
     * Sets line_number
     * @param int $line_number
     * @return $this
     */
    public function setLineNumber($line_number)
    {
        $this->container['line_number'] = $line_number;

        return $this;
    }

    /**
     * Gets order_type
     * @return string
     */
    public function getOrderType()
    {
        return $this->container['order_type'];
    }

    /**
     * Sets order_type
     * @param string $order_type
     * @return $this
     */
    public function setOrderType($order_type)
    {
        $this->container['order_type'] = $order_type;

        return $this;
    }

    /**
     * Gets order_nbr
     * @return string
     */
    public function getOrderNbr()
    {
        return $this->container['order_nbr'];
    }

    /**
     * Sets order_nbr
     * @param string $order_nbr
     * @return $this
     */
    public function setOrderNbr($order_nbr)
    {
        $this->container['order_nbr'] = $order_nbr;

        return $this;
    }

    /**
     * Gets inventory_number
     * @return string
     */
    public function getInventoryNumber()
    {
        return $this->container['inventory_number'];
    }

    /**
     * Sets inventory_number
     * @param string $inventory_number
     * @return $this
     */
    public function setInventoryNumber($inventory_number)
    {
        $this->container['inventory_number'] = $inventory_number;

        return $this;
    }

    /**
     * Gets free_item
     * @return bool
     */
    public function getFreeItem()
    {
        return $this->container['free_item'];
    }

    /**
     * Sets free_item
     * @param bool $free_item
     * @return $this
     */
    public function setFreeItem($free_item)
    {
        $this->container['free_item'] = $free_item;

        return $this;
    }

    /**
     * Gets warehouse
     * @return \Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto
     */
    public function getWarehouse()
    {
        return $this->container['warehouse'];
    }

    /**
     * Sets warehouse
     * @param \Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto $warehouse
     * @return $this
     */
    public function setWarehouse($warehouse)
    {
        $this->container['warehouse'] = $warehouse;

        return $this;
    }

    /**
     * Gets location
     * @return \Trollweb\VismaNetApi\Model\LocationDto
     */
    public function getLocation()
    {
        return $this->container['location'];
    }

    /**
     * Sets location
     * @param \Trollweb\VismaNetApi\Model\LocationDto $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->container['location'] = $location;

        return $this;
    }

    /**
     * Gets uom
     * @return string
     */
    public function getUom()
    {
        return $this->container['uom'];
    }

    /**
     * Sets uom
     * @param string $uom
     * @return $this
     */
    public function setUom($uom)
    {
        $this->container['uom'] = $uom;

        return $this;
    }

    /**
     * Gets shipped_qty
     * @return double
     */
    public function getShippedQty()
    {
        return $this->container['shipped_qty'];
    }

    /**
     * Sets shipped_qty
     * @param double $shipped_qty
     * @return $this
     */
    public function setShippedQty($shipped_qty)
    {
        $this->container['shipped_qty'] = $shipped_qty;

        return $this;
    }

    /**
     * Gets ordered_qty
     * @return double
     */
    public function getOrderedQty()
    {
        return $this->container['ordered_qty'];
    }

    /**
     * Sets ordered_qty
     * @param double $ordered_qty
     * @return $this
     */
    public function setOrderedQty($ordered_qty)
    {
        $this->container['ordered_qty'] = $ordered_qty;

        return $this;
    }

    /**
     * Gets open_qty
     * @return double
     */
    public function getOpenQty()
    {
        return $this->container['open_qty'];
    }

    /**
     * Sets open_qty
     * @param double $open_qty
     * @return $this
     */
    public function setOpenQty($open_qty)
    {
        $this->container['open_qty'] = $open_qty;

        return $this;
    }

    /**
     * Gets lot_serial_nbr
     * @return string
     */
    public function getLotSerialNbr()
    {
        return $this->container['lot_serial_nbr'];
    }

    /**
     * Sets lot_serial_nbr
     * @param string $lot_serial_nbr
     * @return $this
     */
    public function setLotSerialNbr($lot_serial_nbr)
    {
        $this->container['lot_serial_nbr'] = $lot_serial_nbr;

        return $this;
    }

    /**
     * Gets expiration_date
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->container['expiration_date'];
    }

    /**
     * Sets expiration_date
     * @param \DateTime $expiration_date
     * @return $this
     */
    public function setExpirationDate($expiration_date)
    {
        $this->container['expiration_date'] = $expiration_date;

        return $this;
    }

    /**
     * Gets reason_code
     * @return string
     */
    public function getReasonCode()
    {
        return $this->container['reason_code'];
    }

    /**
     * Sets reason_code
     * @param string $reason_code
     * @return $this
     */
    public function setReasonCode($reason_code)
    {
        $this->container['reason_code'] = $reason_code;

        return $this;
    }

    /**
     * Gets description
     * @return string
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets allocations
     * @return \Trollweb\VismaNetApi\Model\AllocationsDto[]
     */
    public function getAllocations()
    {
        return $this->container['allocations'];
    }

    /**
     * Sets allocations
     * @param \Trollweb\VismaNetApi\Model\AllocationsDto[] $allocations
     * @return $this
     */
    public function setAllocations($allocations)
    {
        $this->container['allocations'] = $allocations;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


