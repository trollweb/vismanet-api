<?php
/**
 * OrderToApplyDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * OrderToApplyDto Class Doc Comment
 *
 * @category    Class */
 // @description This class represents an OrderToApply in Payments
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderToApplyDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'OrderToApplyDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'order_type' => 'string',
        'order_no' => 'string',
        'status' => 'string',
        'applied_to_order' => 'double',
        'transferred_to_invoice' => 'double',
        'date' => '\DateTime',
        'due_date' => '\DateTime',
        'cash_discount_date' => '\DateTime',
        'balance' => 'double',
        'description' => 'string',
        'order_total' => 'double',
        'currency' => 'string',
        'invoice_nbr' => 'string',
        'invoice_date' => '\DateTime'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'order_type' => 'orderType',
        'order_no' => 'orderNo',
        'status' => 'status',
        'applied_to_order' => 'appliedToOrder',
        'transferred_to_invoice' => 'transferredToInvoice',
        'date' => 'date',
        'due_date' => 'dueDate',
        'cash_discount_date' => 'cashDiscountDate',
        'balance' => 'balance',
        'description' => 'description',
        'order_total' => 'orderTotal',
        'currency' => 'currency',
        'invoice_nbr' => 'invoiceNbr',
        'invoice_date' => 'invoiceDate'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'order_type' => 'setOrderType',
        'order_no' => 'setOrderNo',
        'status' => 'setStatus',
        'applied_to_order' => 'setAppliedToOrder',
        'transferred_to_invoice' => 'setTransferredToInvoice',
        'date' => 'setDate',
        'due_date' => 'setDueDate',
        'cash_discount_date' => 'setCashDiscountDate',
        'balance' => 'setBalance',
        'description' => 'setDescription',
        'order_total' => 'setOrderTotal',
        'currency' => 'setCurrency',
        'invoice_nbr' => 'setInvoiceNbr',
        'invoice_date' => 'setInvoiceDate'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'order_type' => 'getOrderType',
        'order_no' => 'getOrderNo',
        'status' => 'getStatus',
        'applied_to_order' => 'getAppliedToOrder',
        'transferred_to_invoice' => 'getTransferredToInvoice',
        'date' => 'getDate',
        'due_date' => 'getDueDate',
        'cash_discount_date' => 'getCashDiscountDate',
        'balance' => 'getBalance',
        'description' => 'getDescription',
        'order_total' => 'getOrderTotal',
        'currency' => 'getCurrency',
        'invoice_nbr' => 'getInvoiceNbr',
        'invoice_date' => 'getInvoiceDate'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const STATUS_OPEN = 'Open';
    const STATUS_HOLD = 'Hold';
    const STATUS_CREDIT_HOLD = 'CreditHold';
    const STATUS_COMPLETED = 'Completed';
    const STATUS_CANCELLED = 'Cancelled';
    const STATUS_BACK_ORDER = 'BackOrder';
    const STATUS_SHIPPING = 'Shipping';
    const STATUS_INVOICED = 'Invoiced';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getStatusAllowableValues()
    {
        return [
            self::STATUS_OPEN,
            self::STATUS_HOLD,
            self::STATUS_CREDIT_HOLD,
            self::STATUS_COMPLETED,
            self::STATUS_CANCELLED,
            self::STATUS_BACK_ORDER,
            self::STATUS_SHIPPING,
            self::STATUS_INVOICED,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['order_type'] = isset($data['order_type']) ? $data['order_type'] : null;
        $this->container['order_no'] = isset($data['order_no']) ? $data['order_no'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['applied_to_order'] = isset($data['applied_to_order']) ? $data['applied_to_order'] : null;
        $this->container['transferred_to_invoice'] = isset($data['transferred_to_invoice']) ? $data['transferred_to_invoice'] : null;
        $this->container['date'] = isset($data['date']) ? $data['date'] : null;
        $this->container['due_date'] = isset($data['due_date']) ? $data['due_date'] : null;
        $this->container['cash_discount_date'] = isset($data['cash_discount_date']) ? $data['cash_discount_date'] : null;
        $this->container['balance'] = isset($data['balance']) ? $data['balance'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['order_total'] = isset($data['order_total']) ? $data['order_total'] : null;
        $this->container['currency'] = isset($data['currency']) ? $data['currency'] : null;
        $this->container['invoice_nbr'] = isset($data['invoice_nbr']) ? $data['invoice_nbr'] : null;
        $this->container['invoice_date'] = isset($data['invoice_date']) ? $data['invoice_date'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("Open", "Hold", "CreditHold", "Completed", "Cancelled", "BackOrder", "Shipping", "Invoiced");
        if (!in_array($this->container['status'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'status', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("Open", "Hold", "CreditHold", "Completed", "Cancelled", "BackOrder", "Shipping", "Invoiced");
        if (!in_array($this->container['status'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets order_type
     * @return string
     */
    public function getOrderType()
    {
        return $this->container['order_type'];
    }

    /**
     * Sets order_type
     * @param string $order_type
     * @return $this
     */
    public function setOrderType($order_type)
    {
        $this->container['order_type'] = $order_type;

        return $this;
    }

    /**
     * Gets order_no
     * @return string
     */
    public function getOrderNo()
    {
        return $this->container['order_no'];
    }

    /**
     * Sets order_no
     * @param string $order_no
     * @return $this
     */
    public function setOrderNo($order_no)
    {
        $this->container['order_no'] = $order_no;

        return $this;
    }

    /**
     * Gets status
     * @return string
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $allowed_values = array('Open', 'Hold', 'CreditHold', 'Completed', 'Cancelled', 'BackOrder', 'Shipping', 'Invoiced');
        if (!in_array($status, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'status', must be one of 'Open', 'Hold', 'CreditHold', 'Completed', 'Cancelled', 'BackOrder', 'Shipping', 'Invoiced'");
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets applied_to_order
     * @return double
     */
    public function getAppliedToOrder()
    {
        return $this->container['applied_to_order'];
    }

    /**
     * Sets applied_to_order
     * @param double $applied_to_order
     * @return $this
     */
    public function setAppliedToOrder($applied_to_order)
    {
        $this->container['applied_to_order'] = $applied_to_order;

        return $this;
    }

    /**
     * Gets transferred_to_invoice
     * @return double
     */
    public function getTransferredToInvoice()
    {
        return $this->container['transferred_to_invoice'];
    }

    /**
     * Sets transferred_to_invoice
     * @param double $transferred_to_invoice
     * @return $this
     */
    public function setTransferredToInvoice($transferred_to_invoice)
    {
        $this->container['transferred_to_invoice'] = $transferred_to_invoice;

        return $this;
    }

    /**
     * Gets date
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->container['date'];
    }

    /**
     * Sets date
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->container['date'] = $date;

        return $this;
    }

    /**
     * Gets due_date
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->container['due_date'];
    }

    /**
     * Sets due_date
     * @param \DateTime $due_date
     * @return $this
     */
    public function setDueDate($due_date)
    {
        $this->container['due_date'] = $due_date;

        return $this;
    }

    /**
     * Gets cash_discount_date
     * @return \DateTime
     */
    public function getCashDiscountDate()
    {
        return $this->container['cash_discount_date'];
    }

    /**
     * Sets cash_discount_date
     * @param \DateTime $cash_discount_date
     * @return $this
     */
    public function setCashDiscountDate($cash_discount_date)
    {
        $this->container['cash_discount_date'] = $cash_discount_date;

        return $this;
    }

    /**
     * Gets balance
     * @return double
     */
    public function getBalance()
    {
        return $this->container['balance'];
    }

    /**
     * Sets balance
     * @param double $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->container['balance'] = $balance;

        return $this;
    }

    /**
     * Gets description
     * @return string
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets order_total
     * @return double
     */
    public function getOrderTotal()
    {
        return $this->container['order_total'];
    }

    /**
     * Sets order_total
     * @param double $order_total
     * @return $this
     */
    public function setOrderTotal($order_total)
    {
        $this->container['order_total'] = $order_total;

        return $this;
    }

    /**
     * Gets currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->container['currency'];
    }

    /**
     * Sets currency
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->container['currency'] = $currency;

        return $this;
    }

    /**
     * Gets invoice_nbr
     * @return string
     */
    public function getInvoiceNbr()
    {
        return $this->container['invoice_nbr'];
    }

    /**
     * Sets invoice_nbr
     * @param string $invoice_nbr
     * @return $this
     */
    public function setInvoiceNbr($invoice_nbr)
    {
        $this->container['invoice_nbr'] = $invoice_nbr;

        return $this;
    }

    /**
     * Gets invoice_date
     * @return \DateTime
     */
    public function getInvoiceDate()
    {
        return $this->container['invoice_date'];
    }

    /**
     * Sets invoice_date
     * @param \DateTime $invoice_date
     * @return $this
     */
    public function setInvoiceDate($invoice_date)
    {
        $this->container['invoice_date'] = $invoice_date;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


