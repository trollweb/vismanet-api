<?php
/**
 * PurchaseOrderLineDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * PurchaseOrderLineDto Class Doc Comment
 *
 * @category    Class */
 // @description This class represents a Purchase Order Line in PurchaseOrderController. Used for getting data.
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PurchaseOrderLineDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'PurchaseOrderLineDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'line_nbr' => 'int',
        'branch' => '\Trollweb\VismaNetApi\Model\BranchNumberDto',
        'inventory' => '\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto',
        'line_type' => 'string',
        'warehouse' => '\Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto',
        'line_description' => 'string',
        'uom' => 'string',
        'order_qty' => 'double',
        'qty_on_receipts' => 'double',
        'unit_cost' => 'double',
        'ext_cost' => 'double',
        'discount_percent' => 'double',
        'discount_amount' => 'double',
        'manual_discount' => 'bool',
        'discount_code' => '\Trollweb\VismaNetApi\Model\DiscountCodeNumberDescriptionDto',
        'amount' => 'double',
        'received_amt' => 'double',
        'alternate_id' => 'string',
        'min_receipt' => 'double',
        'max_receipt' => 'double',
        'complete_on' => 'double',
        'receipt_action' => 'string',
        'tax_category' => '\Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto',
        'account' => '\Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto',
        'sub' => '\Trollweb\VismaNetApi\Model\SubAccountDto',
        'project' => '\Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto',
        'project_task' => '\Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto',
        'requested' => '\DateTime',
        'promised' => '\DateTime',
        'completed' => 'bool',
        'canceled' => 'bool',
        'order_type' => 'string',
        'order_number' => 'string'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'line_nbr' => 'lineNbr',
        'branch' => 'branch',
        'inventory' => 'inventory',
        'line_type' => 'lineType',
        'warehouse' => 'warehouse',
        'line_description' => 'lineDescription',
        'uom' => 'uom',
        'order_qty' => 'orderQty',
        'qty_on_receipts' => 'qtyOnReceipts',
        'unit_cost' => 'unitCost',
        'ext_cost' => 'extCost',
        'discount_percent' => 'discountPercent',
        'discount_amount' => 'discountAmount',
        'manual_discount' => 'manualDiscount',
        'discount_code' => 'discountCode',
        'amount' => 'amount',
        'received_amt' => 'receivedAmt',
        'alternate_id' => 'alternateId',
        'min_receipt' => 'minReceipt',
        'max_receipt' => 'maxReceipt',
        'complete_on' => 'completeOn',
        'receipt_action' => 'receiptAction',
        'tax_category' => 'taxCategory',
        'account' => 'account',
        'sub' => 'sub',
        'project' => 'project',
        'project_task' => 'projectTask',
        'requested' => 'requested',
        'promised' => 'promised',
        'completed' => 'completed',
        'canceled' => 'canceled',
        'order_type' => 'orderType',
        'order_number' => 'orderNumber'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'line_nbr' => 'setLineNbr',
        'branch' => 'setBranch',
        'inventory' => 'setInventory',
        'line_type' => 'setLineType',
        'warehouse' => 'setWarehouse',
        'line_description' => 'setLineDescription',
        'uom' => 'setUom',
        'order_qty' => 'setOrderQty',
        'qty_on_receipts' => 'setQtyOnReceipts',
        'unit_cost' => 'setUnitCost',
        'ext_cost' => 'setExtCost',
        'discount_percent' => 'setDiscountPercent',
        'discount_amount' => 'setDiscountAmount',
        'manual_discount' => 'setManualDiscount',
        'discount_code' => 'setDiscountCode',
        'amount' => 'setAmount',
        'received_amt' => 'setReceivedAmt',
        'alternate_id' => 'setAlternateId',
        'min_receipt' => 'setMinReceipt',
        'max_receipt' => 'setMaxReceipt',
        'complete_on' => 'setCompleteOn',
        'receipt_action' => 'setReceiptAction',
        'tax_category' => 'setTaxCategory',
        'account' => 'setAccount',
        'sub' => 'setSub',
        'project' => 'setProject',
        'project_task' => 'setProjectTask',
        'requested' => 'setRequested',
        'promised' => 'setPromised',
        'completed' => 'setCompleted',
        'canceled' => 'setCanceled',
        'order_type' => 'setOrderType',
        'order_number' => 'setOrderNumber'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'line_nbr' => 'getLineNbr',
        'branch' => 'getBranch',
        'inventory' => 'getInventory',
        'line_type' => 'getLineType',
        'warehouse' => 'getWarehouse',
        'line_description' => 'getLineDescription',
        'uom' => 'getUom',
        'order_qty' => 'getOrderQty',
        'qty_on_receipts' => 'getQtyOnReceipts',
        'unit_cost' => 'getUnitCost',
        'ext_cost' => 'getExtCost',
        'discount_percent' => 'getDiscountPercent',
        'discount_amount' => 'getDiscountAmount',
        'manual_discount' => 'getManualDiscount',
        'discount_code' => 'getDiscountCode',
        'amount' => 'getAmount',
        'received_amt' => 'getReceivedAmt',
        'alternate_id' => 'getAlternateId',
        'min_receipt' => 'getMinReceipt',
        'max_receipt' => 'getMaxReceipt',
        'complete_on' => 'getCompleteOn',
        'receipt_action' => 'getReceiptAction',
        'tax_category' => 'getTaxCategory',
        'account' => 'getAccount',
        'sub' => 'getSub',
        'project' => 'getProject',
        'project_task' => 'getProjectTask',
        'requested' => 'getRequested',
        'promised' => 'getPromised',
        'completed' => 'getCompleted',
        'canceled' => 'getCanceled',
        'order_type' => 'getOrderType',
        'order_number' => 'getOrderNumber'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const LINE_TYPE_GOODS_FOR_INVENTORY = 'GoodsForInventory';
    const LINE_TYPE_GOODS_FOR_SALES_ORDER = 'GoodsForSalesOrder';
    const LINE_TYPE_GOODS_FOR_REPLENISHMENT = 'GoodsForReplenishment';
    const LINE_TYPE_GOODS_FOR_DROP_SHIP = 'GoodsForDropShip';
    const LINE_TYPE_NON_STOCK_FOR_DROP_SHIP = 'NonStockForDropShip';
    const LINE_TYPE_NON_STOCK_FOR_SALES_ORDER = 'NonStockForSalesOrder';
    const LINE_TYPE_NON_STOCK = 'NonStock';
    const LINE_TYPE_SERVICE = 'Service';
    const LINE_TYPE_FREIGHT = 'Freight';
    const LINE_TYPE_DESCRIPTION = 'Description';
    const RECEIPT_ACTION_REJECT = 'Reject';
    const RECEIPT_ACTION_ACCEPT_BUT_WARN = 'AcceptButWarn';
    const RECEIPT_ACTION_ACCEPT = 'Accept';
    const ORDER_TYPE_REGULAR_ORDER = 'RegularOrder';
    const ORDER_TYPE_DROP_SHIP = 'DropShip';
    const ORDER_TYPE_BLANKET = 'Blanket';
    const ORDER_TYPE_STANDARD_BLANKET = 'StandardBlanket';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getLineTypeAllowableValues()
    {
        return [
            self::LINE_TYPE_GOODS_FOR_INVENTORY,
            self::LINE_TYPE_GOODS_FOR_SALES_ORDER,
            self::LINE_TYPE_GOODS_FOR_REPLENISHMENT,
            self::LINE_TYPE_GOODS_FOR_DROP_SHIP,
            self::LINE_TYPE_NON_STOCK_FOR_DROP_SHIP,
            self::LINE_TYPE_NON_STOCK_FOR_SALES_ORDER,
            self::LINE_TYPE_NON_STOCK,
            self::LINE_TYPE_SERVICE,
            self::LINE_TYPE_FREIGHT,
            self::LINE_TYPE_DESCRIPTION,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getReceiptActionAllowableValues()
    {
        return [
            self::RECEIPT_ACTION_REJECT,
            self::RECEIPT_ACTION_ACCEPT_BUT_WARN,
            self::RECEIPT_ACTION_ACCEPT,
        ];
    }
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getOrderTypeAllowableValues()
    {
        return [
            self::ORDER_TYPE_REGULAR_ORDER,
            self::ORDER_TYPE_DROP_SHIP,
            self::ORDER_TYPE_BLANKET,
            self::ORDER_TYPE_STANDARD_BLANKET,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['line_nbr'] = isset($data['line_nbr']) ? $data['line_nbr'] : null;
        $this->container['branch'] = isset($data['branch']) ? $data['branch'] : null;
        $this->container['inventory'] = isset($data['inventory']) ? $data['inventory'] : null;
        $this->container['line_type'] = isset($data['line_type']) ? $data['line_type'] : null;
        $this->container['warehouse'] = isset($data['warehouse']) ? $data['warehouse'] : null;
        $this->container['line_description'] = isset($data['line_description']) ? $data['line_description'] : null;
        $this->container['uom'] = isset($data['uom']) ? $data['uom'] : null;
        $this->container['order_qty'] = isset($data['order_qty']) ? $data['order_qty'] : null;
        $this->container['qty_on_receipts'] = isset($data['qty_on_receipts']) ? $data['qty_on_receipts'] : null;
        $this->container['unit_cost'] = isset($data['unit_cost']) ? $data['unit_cost'] : null;
        $this->container['ext_cost'] = isset($data['ext_cost']) ? $data['ext_cost'] : null;
        $this->container['discount_percent'] = isset($data['discount_percent']) ? $data['discount_percent'] : null;
        $this->container['discount_amount'] = isset($data['discount_amount']) ? $data['discount_amount'] : null;
        $this->container['manual_discount'] = isset($data['manual_discount']) ? $data['manual_discount'] : null;
        $this->container['discount_code'] = isset($data['discount_code']) ? $data['discount_code'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
        $this->container['received_amt'] = isset($data['received_amt']) ? $data['received_amt'] : null;
        $this->container['alternate_id'] = isset($data['alternate_id']) ? $data['alternate_id'] : null;
        $this->container['min_receipt'] = isset($data['min_receipt']) ? $data['min_receipt'] : null;
        $this->container['max_receipt'] = isset($data['max_receipt']) ? $data['max_receipt'] : null;
        $this->container['complete_on'] = isset($data['complete_on']) ? $data['complete_on'] : null;
        $this->container['receipt_action'] = isset($data['receipt_action']) ? $data['receipt_action'] : null;
        $this->container['tax_category'] = isset($data['tax_category']) ? $data['tax_category'] : null;
        $this->container['account'] = isset($data['account']) ? $data['account'] : null;
        $this->container['sub'] = isset($data['sub']) ? $data['sub'] : null;
        $this->container['project'] = isset($data['project']) ? $data['project'] : null;
        $this->container['project_task'] = isset($data['project_task']) ? $data['project_task'] : null;
        $this->container['requested'] = isset($data['requested']) ? $data['requested'] : null;
        $this->container['promised'] = isset($data['promised']) ? $data['promised'] : null;
        $this->container['completed'] = isset($data['completed']) ? $data['completed'] : null;
        $this->container['canceled'] = isset($data['canceled']) ? $data['canceled'] : null;
        $this->container['order_type'] = isset($data['order_type']) ? $data['order_type'] : null;
        $this->container['order_number'] = isset($data['order_number']) ? $data['order_number'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("GoodsForInventory", "GoodsForSalesOrder", "GoodsForReplenishment", "GoodsForDropShip", "NonStockForDropShip", "NonStockForSalesOrder", "NonStock", "Service", "Freight", "Description");
        if (!in_array($this->container['line_type'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'line_type', must be one of #{allowed_values}.";
        }

        $allowed_values = array("Reject", "AcceptButWarn", "Accept");
        if (!in_array($this->container['receipt_action'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'receipt_action', must be one of #{allowed_values}.";
        }

        $allowed_values = array("RegularOrder", "DropShip", "Blanket", "StandardBlanket");
        if (!in_array($this->container['order_type'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'order_type', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("GoodsForInventory", "GoodsForSalesOrder", "GoodsForReplenishment", "GoodsForDropShip", "NonStockForDropShip", "NonStockForSalesOrder", "NonStock", "Service", "Freight", "Description");
        if (!in_array($this->container['line_type'], $allowed_values)) {
            return false;
        }
        $allowed_values = array("Reject", "AcceptButWarn", "Accept");
        if (!in_array($this->container['receipt_action'], $allowed_values)) {
            return false;
        }
        $allowed_values = array("RegularOrder", "DropShip", "Blanket", "StandardBlanket");
        if (!in_array($this->container['order_type'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets line_nbr
     * @return int
     */
    public function getLineNbr()
    {
        return $this->container['line_nbr'];
    }

    /**
     * Sets line_nbr
     * @param int $line_nbr
     * @return $this
     */
    public function setLineNbr($line_nbr)
    {
        $this->container['line_nbr'] = $line_nbr;

        return $this;
    }

    /**
     * Gets branch
     * @return \Trollweb\VismaNetApi\Model\BranchNumberDto
     */
    public function getBranch()
    {
        return $this->container['branch'];
    }

    /**
     * Sets branch
     * @param \Trollweb\VismaNetApi\Model\BranchNumberDto $branch
     * @return $this
     */
    public function setBranch($branch)
    {
        $this->container['branch'] = $branch;

        return $this;
    }

    /**
     * Gets inventory
     * @return \Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto
     */
    public function getInventory()
    {
        return $this->container['inventory'];
    }

    /**
     * Sets inventory
     * @param \Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto $inventory
     * @return $this
     */
    public function setInventory($inventory)
    {
        $this->container['inventory'] = $inventory;

        return $this;
    }

    /**
     * Gets line_type
     * @return string
     */
    public function getLineType()
    {
        return $this->container['line_type'];
    }

    /**
     * Sets line_type
     * @param string $line_type
     * @return $this
     */
    public function setLineType($line_type)
    {
        $allowed_values = array('GoodsForInventory', 'GoodsForSalesOrder', 'GoodsForReplenishment', 'GoodsForDropShip', 'NonStockForDropShip', 'NonStockForSalesOrder', 'NonStock', 'Service', 'Freight', 'Description');
        if (!in_array($line_type, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'line_type', must be one of 'GoodsForInventory', 'GoodsForSalesOrder', 'GoodsForReplenishment', 'GoodsForDropShip', 'NonStockForDropShip', 'NonStockForSalesOrder', 'NonStock', 'Service', 'Freight', 'Description'");
        }
        $this->container['line_type'] = $line_type;

        return $this;
    }

    /**
     * Gets warehouse
     * @return \Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto
     */
    public function getWarehouse()
    {
        return $this->container['warehouse'];
    }

    /**
     * Sets warehouse
     * @param \Trollweb\VismaNetApi\Model\WarehouseIdDescriptionDto $warehouse
     * @return $this
     */
    public function setWarehouse($warehouse)
    {
        $this->container['warehouse'] = $warehouse;

        return $this;
    }

    /**
     * Gets line_description
     * @return string
     */
    public function getLineDescription()
    {
        return $this->container['line_description'];
    }

    /**
     * Sets line_description
     * @param string $line_description
     * @return $this
     */
    public function setLineDescription($line_description)
    {
        $this->container['line_description'] = $line_description;

        return $this;
    }

    /**
     * Gets uom
     * @return string
     */
    public function getUom()
    {
        return $this->container['uom'];
    }

    /**
     * Sets uom
     * @param string $uom
     * @return $this
     */
    public function setUom($uom)
    {
        $this->container['uom'] = $uom;

        return $this;
    }

    /**
     * Gets order_qty
     * @return double
     */
    public function getOrderQty()
    {
        return $this->container['order_qty'];
    }

    /**
     * Sets order_qty
     * @param double $order_qty
     * @return $this
     */
    public function setOrderQty($order_qty)
    {
        $this->container['order_qty'] = $order_qty;

        return $this;
    }

    /**
     * Gets qty_on_receipts
     * @return double
     */
    public function getQtyOnReceipts()
    {
        return $this->container['qty_on_receipts'];
    }

    /**
     * Sets qty_on_receipts
     * @param double $qty_on_receipts
     * @return $this
     */
    public function setQtyOnReceipts($qty_on_receipts)
    {
        $this->container['qty_on_receipts'] = $qty_on_receipts;

        return $this;
    }

    /**
     * Gets unit_cost
     * @return double
     */
    public function getUnitCost()
    {
        return $this->container['unit_cost'];
    }

    /**
     * Sets unit_cost
     * @param double $unit_cost
     * @return $this
     */
    public function setUnitCost($unit_cost)
    {
        $this->container['unit_cost'] = $unit_cost;

        return $this;
    }

    /**
     * Gets ext_cost
     * @return double
     */
    public function getExtCost()
    {
        return $this->container['ext_cost'];
    }

    /**
     * Sets ext_cost
     * @param double $ext_cost
     * @return $this
     */
    public function setExtCost($ext_cost)
    {
        $this->container['ext_cost'] = $ext_cost;

        return $this;
    }

    /**
     * Gets discount_percent
     * @return double
     */
    public function getDiscountPercent()
    {
        return $this->container['discount_percent'];
    }

    /**
     * Sets discount_percent
     * @param double $discount_percent
     * @return $this
     */
    public function setDiscountPercent($discount_percent)
    {
        $this->container['discount_percent'] = $discount_percent;

        return $this;
    }

    /**
     * Gets discount_amount
     * @return double
     */
    public function getDiscountAmount()
    {
        return $this->container['discount_amount'];
    }

    /**
     * Sets discount_amount
     * @param double $discount_amount
     * @return $this
     */
    public function setDiscountAmount($discount_amount)
    {
        $this->container['discount_amount'] = $discount_amount;

        return $this;
    }

    /**
     * Gets manual_discount
     * @return bool
     */
    public function getManualDiscount()
    {
        return $this->container['manual_discount'];
    }

    /**
     * Sets manual_discount
     * @param bool $manual_discount
     * @return $this
     */
    public function setManualDiscount($manual_discount)
    {
        $this->container['manual_discount'] = $manual_discount;

        return $this;
    }

    /**
     * Gets discount_code
     * @return \Trollweb\VismaNetApi\Model\DiscountCodeNumberDescriptionDto
     */
    public function getDiscountCode()
    {
        return $this->container['discount_code'];
    }

    /**
     * Sets discount_code
     * @param \Trollweb\VismaNetApi\Model\DiscountCodeNumberDescriptionDto $discount_code
     * @return $this
     */
    public function setDiscountCode($discount_code)
    {
        $this->container['discount_code'] = $discount_code;

        return $this;
    }

    /**
     * Gets amount
     * @return double
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     * @param double $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets received_amt
     * @return double
     */
    public function getReceivedAmt()
    {
        return $this->container['received_amt'];
    }

    /**
     * Sets received_amt
     * @param double $received_amt
     * @return $this
     */
    public function setReceivedAmt($received_amt)
    {
        $this->container['received_amt'] = $received_amt;

        return $this;
    }

    /**
     * Gets alternate_id
     * @return string
     */
    public function getAlternateId()
    {
        return $this->container['alternate_id'];
    }

    /**
     * Sets alternate_id
     * @param string $alternate_id
     * @return $this
     */
    public function setAlternateId($alternate_id)
    {
        $this->container['alternate_id'] = $alternate_id;

        return $this;
    }

    /**
     * Gets min_receipt
     * @return double
     */
    public function getMinReceipt()
    {
        return $this->container['min_receipt'];
    }

    /**
     * Sets min_receipt
     * @param double $min_receipt
     * @return $this
     */
    public function setMinReceipt($min_receipt)
    {
        $this->container['min_receipt'] = $min_receipt;

        return $this;
    }

    /**
     * Gets max_receipt
     * @return double
     */
    public function getMaxReceipt()
    {
        return $this->container['max_receipt'];
    }

    /**
     * Sets max_receipt
     * @param double $max_receipt
     * @return $this
     */
    public function setMaxReceipt($max_receipt)
    {
        $this->container['max_receipt'] = $max_receipt;

        return $this;
    }

    /**
     * Gets complete_on
     * @return double
     */
    public function getCompleteOn()
    {
        return $this->container['complete_on'];
    }

    /**
     * Sets complete_on
     * @param double $complete_on
     * @return $this
     */
    public function setCompleteOn($complete_on)
    {
        $this->container['complete_on'] = $complete_on;

        return $this;
    }

    /**
     * Gets receipt_action
     * @return string
     */
    public function getReceiptAction()
    {
        return $this->container['receipt_action'];
    }

    /**
     * Sets receipt_action
     * @param string $receipt_action
     * @return $this
     */
    public function setReceiptAction($receipt_action)
    {
        $allowed_values = array('Reject', 'AcceptButWarn', 'Accept');
        if (!in_array($receipt_action, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'receipt_action', must be one of 'Reject', 'AcceptButWarn', 'Accept'");
        }
        $this->container['receipt_action'] = $receipt_action;

        return $this;
    }

    /**
     * Gets tax_category
     * @return \Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto
     */
    public function getTaxCategory()
    {
        return $this->container['tax_category'];
    }

    /**
     * Sets tax_category
     * @param \Trollweb\VismaNetApi\Model\TaxCategoryNumberDescriptionDto $tax_category
     * @return $this
     */
    public function setTaxCategory($tax_category)
    {
        $this->container['tax_category'] = $tax_category;

        return $this;
    }

    /**
     * Gets account
     * @return \Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto
     */
    public function getAccount()
    {
        return $this->container['account'];
    }

    /**
     * Sets account
     * @param \Trollweb\VismaNetApi\Model\AccountNumberTypeDescriptionDto $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->container['account'] = $account;

        return $this;
    }

    /**
     * Gets sub
     * @return \Trollweb\VismaNetApi\Model\SubAccountDto
     */
    public function getSub()
    {
        return $this->container['sub'];
    }

    /**
     * Sets sub
     * @param \Trollweb\VismaNetApi\Model\SubAccountDto $sub
     * @return $this
     */
    public function setSub($sub)
    {
        $this->container['sub'] = $sub;

        return $this;
    }

    /**
     * Gets project
     * @return \Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto
     */
    public function getProject()
    {
        return $this->container['project'];
    }

    /**
     * Sets project
     * @param \Trollweb\VismaNetApi\Model\ProjectIdDescriptionDto $project
     * @return $this
     */
    public function setProject($project)
    {
        $this->container['project'] = $project;

        return $this;
    }

    /**
     * Gets project_task
     * @return \Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto
     */
    public function getProjectTask()
    {
        return $this->container['project_task'];
    }

    /**
     * Sets project_task
     * @param \Trollweb\VismaNetApi\Model\ProjectTaskIdDescriptionDto $project_task
     * @return $this
     */
    public function setProjectTask($project_task)
    {
        $this->container['project_task'] = $project_task;

        return $this;
    }

    /**
     * Gets requested
     * @return \DateTime
     */
    public function getRequested()
    {
        return $this->container['requested'];
    }

    /**
     * Sets requested
     * @param \DateTime $requested
     * @return $this
     */
    public function setRequested($requested)
    {
        $this->container['requested'] = $requested;

        return $this;
    }

    /**
     * Gets promised
     * @return \DateTime
     */
    public function getPromised()
    {
        return $this->container['promised'];
    }

    /**
     * Sets promised
     * @param \DateTime $promised
     * @return $this
     */
    public function setPromised($promised)
    {
        $this->container['promised'] = $promised;

        return $this;
    }

    /**
     * Gets completed
     * @return bool
     */
    public function getCompleted()
    {
        return $this->container['completed'];
    }

    /**
     * Sets completed
     * @param bool $completed
     * @return $this
     */
    public function setCompleted($completed)
    {
        $this->container['completed'] = $completed;

        return $this;
    }

    /**
     * Gets canceled
     * @return bool
     */
    public function getCanceled()
    {
        return $this->container['canceled'];
    }

    /**
     * Sets canceled
     * @param bool $canceled
     * @return $this
     */
    public function setCanceled($canceled)
    {
        $this->container['canceled'] = $canceled;

        return $this;
    }

    /**
     * Gets order_type
     * @return string
     */
    public function getOrderType()
    {
        return $this->container['order_type'];
    }

    /**
     * Sets order_type
     * @param string $order_type
     * @return $this
     */
    public function setOrderType($order_type)
    {
        $allowed_values = array('RegularOrder', 'DropShip', 'Blanket', 'StandardBlanket');
        if (!in_array($order_type, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'order_type', must be one of 'RegularOrder', 'DropShip', 'Blanket', 'StandardBlanket'");
        }
        $this->container['order_type'] = $order_type;

        return $this;
    }

    /**
     * Gets order_number
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->container['order_number'];
    }

    /**
     * Sets order_number
     * @param string $order_number
     * @return $this
     */
    public function setOrderNumber($order_number)
    {
        $this->container['order_number'] = $order_number;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


