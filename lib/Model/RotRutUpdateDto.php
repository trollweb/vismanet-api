<?php
/**
 * RotRutUpdateDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * RotRutUpdateDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class RotRutUpdateDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'RotRutUpdateDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'distributed_automaticaly' => '\Trollweb\VismaNetApi\Model\DtoValueNullableBoolean',
        'type' => '\Trollweb\VismaNetApi\Model\DtoValueNullableRutRotTypes',
        'appartment' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'estate' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'organization_nbr' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'distribution' => '\Trollweb\VismaNetApi\Model\RotRutDistributionUpdateDto[]'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'distributed_automaticaly' => 'distributedAutomaticaly',
        'type' => 'type',
        'appartment' => 'appartment',
        'estate' => 'estate',
        'organization_nbr' => 'organizationNbr',
        'distribution' => 'distribution'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'distributed_automaticaly' => 'setDistributedAutomaticaly',
        'type' => 'setType',
        'appartment' => 'setAppartment',
        'estate' => 'setEstate',
        'organization_nbr' => 'setOrganizationNbr',
        'distribution' => 'setDistribution'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'distributed_automaticaly' => 'getDistributedAutomaticaly',
        'type' => 'getType',
        'appartment' => 'getAppartment',
        'estate' => 'getEstate',
        'organization_nbr' => 'getOrganizationNbr',
        'distribution' => 'getDistribution'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['distributed_automaticaly'] = isset($data['distributed_automaticaly']) ? $data['distributed_automaticaly'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['appartment'] = isset($data['appartment']) ? $data['appartment'] : null;
        $this->container['estate'] = isset($data['estate']) ? $data['estate'] : null;
        $this->container['organization_nbr'] = isset($data['organization_nbr']) ? $data['organization_nbr'] : null;
        $this->container['distribution'] = isset($data['distribution']) ? $data['distribution'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets distributed_automaticaly
     * @return \Trollweb\VismaNetApi\Model\DtoValueNullableBoolean
     */
    public function getDistributedAutomaticaly()
    {
        return $this->container['distributed_automaticaly'];
    }

    /**
     * Sets distributed_automaticaly
     * @param \Trollweb\VismaNetApi\Model\DtoValueNullableBoolean $distributed_automaticaly
     * @return $this
     */
    public function setDistributedAutomaticaly($distributed_automaticaly)
    {
        $this->container['distributed_automaticaly'] = $distributed_automaticaly;

        return $this;
    }

    /**
     * Gets type
     * @return \Trollweb\VismaNetApi\Model\DtoValueNullableRutRotTypes
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     * @param \Trollweb\VismaNetApi\Model\DtoValueNullableRutRotTypes $type
     * @return $this
     */
    public function setType($type)
    {
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets appartment
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getAppartment()
    {
        return $this->container['appartment'];
    }

    /**
     * Sets appartment
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $appartment
     * @return $this
     */
    public function setAppartment($appartment)
    {
        $this->container['appartment'] = $appartment;

        return $this;
    }

    /**
     * Gets estate
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getEstate()
    {
        return $this->container['estate'];
    }

    /**
     * Sets estate
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $estate
     * @return $this
     */
    public function setEstate($estate)
    {
        $this->container['estate'] = $estate;

        return $this;
    }

    /**
     * Gets organization_nbr
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getOrganizationNbr()
    {
        return $this->container['organization_nbr'];
    }

    /**
     * Sets organization_nbr
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $organization_nbr
     * @return $this
     */
    public function setOrganizationNbr($organization_nbr)
    {
        $this->container['organization_nbr'] = $organization_nbr;

        return $this;
    }

    /**
     * Gets distribution
     * @return \Trollweb\VismaNetApi\Model\RotRutDistributionUpdateDto[]
     */
    public function getDistribution()
    {
        return $this->container['distribution'];
    }

    /**
     * Sets distribution
     * @param \Trollweb\VismaNetApi\Model\RotRutDistributionUpdateDto[] $distribution
     * @return $this
     */
    public function setDistribution($distribution)
    {
        $this->container['distribution'] = $distribution;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


