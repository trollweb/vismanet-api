<?php
/**
 * SupplierBalanceDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * SupplierBalanceDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SupplierBalanceDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'SupplierBalanceDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'supplier' => '\Trollweb\VismaNetApi\Model\SupplierDescriptionDto',
        'balance' => 'double',
        'unreleased_purchases_not_in_approval' => '\Trollweb\VismaNetApi\Model\WithoutWithVatDto',
        'total_sent_for_approval' => '\Trollweb\VismaNetApi\Model\WithoutWithVatDto',
        'total_purchase_invoice_period' => '\Trollweb\VismaNetApi\Model\WithoutWithVatDto',
        'total_purchase_invoice_year' => '\Trollweb\VismaNetApi\Model\WithoutWithVatDto',
        'last_modified_date_time' => '\DateTime'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'supplier' => 'supplier',
        'balance' => 'balance',
        'unreleased_purchases_not_in_approval' => 'unreleasedPurchasesNotInApproval',
        'total_sent_for_approval' => 'totalSentForApproval',
        'total_purchase_invoice_period' => 'totalPurchaseInvoicePeriod',
        'total_purchase_invoice_year' => 'totalPurchaseInvoiceYear',
        'last_modified_date_time' => 'lastModifiedDateTime'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'supplier' => 'setSupplier',
        'balance' => 'setBalance',
        'unreleased_purchases_not_in_approval' => 'setUnreleasedPurchasesNotInApproval',
        'total_sent_for_approval' => 'setTotalSentForApproval',
        'total_purchase_invoice_period' => 'setTotalPurchaseInvoicePeriod',
        'total_purchase_invoice_year' => 'setTotalPurchaseInvoiceYear',
        'last_modified_date_time' => 'setLastModifiedDateTime'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'supplier' => 'getSupplier',
        'balance' => 'getBalance',
        'unreleased_purchases_not_in_approval' => 'getUnreleasedPurchasesNotInApproval',
        'total_sent_for_approval' => 'getTotalSentForApproval',
        'total_purchase_invoice_period' => 'getTotalPurchaseInvoicePeriod',
        'total_purchase_invoice_year' => 'getTotalPurchaseInvoiceYear',
        'last_modified_date_time' => 'getLastModifiedDateTime'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['supplier'] = isset($data['supplier']) ? $data['supplier'] : null;
        $this->container['balance'] = isset($data['balance']) ? $data['balance'] : null;
        $this->container['unreleased_purchases_not_in_approval'] = isset($data['unreleased_purchases_not_in_approval']) ? $data['unreleased_purchases_not_in_approval'] : null;
        $this->container['total_sent_for_approval'] = isset($data['total_sent_for_approval']) ? $data['total_sent_for_approval'] : null;
        $this->container['total_purchase_invoice_period'] = isset($data['total_purchase_invoice_period']) ? $data['total_purchase_invoice_period'] : null;
        $this->container['total_purchase_invoice_year'] = isset($data['total_purchase_invoice_year']) ? $data['total_purchase_invoice_year'] : null;
        $this->container['last_modified_date_time'] = isset($data['last_modified_date_time']) ? $data['last_modified_date_time'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets supplier
     * @return \Trollweb\VismaNetApi\Model\SupplierDescriptionDto
     */
    public function getSupplier()
    {
        return $this->container['supplier'];
    }

    /**
     * Sets supplier
     * @param \Trollweb\VismaNetApi\Model\SupplierDescriptionDto $supplier
     * @return $this
     */
    public function setSupplier($supplier)
    {
        $this->container['supplier'] = $supplier;

        return $this;
    }

    /**
     * Gets balance
     * @return double
     */
    public function getBalance()
    {
        return $this->container['balance'];
    }

    /**
     * Sets balance
     * @param double $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->container['balance'] = $balance;

        return $this;
    }

    /**
     * Gets unreleased_purchases_not_in_approval
     * @return \Trollweb\VismaNetApi\Model\WithoutWithVatDto
     */
    public function getUnreleasedPurchasesNotInApproval()
    {
        return $this->container['unreleased_purchases_not_in_approval'];
    }

    /**
     * Sets unreleased_purchases_not_in_approval
     * @param \Trollweb\VismaNetApi\Model\WithoutWithVatDto $unreleased_purchases_not_in_approval
     * @return $this
     */
    public function setUnreleasedPurchasesNotInApproval($unreleased_purchases_not_in_approval)
    {
        $this->container['unreleased_purchases_not_in_approval'] = $unreleased_purchases_not_in_approval;

        return $this;
    }

    /**
     * Gets total_sent_for_approval
     * @return \Trollweb\VismaNetApi\Model\WithoutWithVatDto
     */
    public function getTotalSentForApproval()
    {
        return $this->container['total_sent_for_approval'];
    }

    /**
     * Sets total_sent_for_approval
     * @param \Trollweb\VismaNetApi\Model\WithoutWithVatDto $total_sent_for_approval
     * @return $this
     */
    public function setTotalSentForApproval($total_sent_for_approval)
    {
        $this->container['total_sent_for_approval'] = $total_sent_for_approval;

        return $this;
    }

    /**
     * Gets total_purchase_invoice_period
     * @return \Trollweb\VismaNetApi\Model\WithoutWithVatDto
     */
    public function getTotalPurchaseInvoicePeriod()
    {
        return $this->container['total_purchase_invoice_period'];
    }

    /**
     * Sets total_purchase_invoice_period
     * @param \Trollweb\VismaNetApi\Model\WithoutWithVatDto $total_purchase_invoice_period
     * @return $this
     */
    public function setTotalPurchaseInvoicePeriod($total_purchase_invoice_period)
    {
        $this->container['total_purchase_invoice_period'] = $total_purchase_invoice_period;

        return $this;
    }

    /**
     * Gets total_purchase_invoice_year
     * @return \Trollweb\VismaNetApi\Model\WithoutWithVatDto
     */
    public function getTotalPurchaseInvoiceYear()
    {
        return $this->container['total_purchase_invoice_year'];
    }

    /**
     * Sets total_purchase_invoice_year
     * @param \Trollweb\VismaNetApi\Model\WithoutWithVatDto $total_purchase_invoice_year
     * @return $this
     */
    public function setTotalPurchaseInvoiceYear($total_purchase_invoice_year)
    {
        $this->container['total_purchase_invoice_year'] = $total_purchase_invoice_year;

        return $this;
    }

    /**
     * Gets last_modified_date_time
     * @return \DateTime
     */
    public function getLastModifiedDateTime()
    {
        return $this->container['last_modified_date_time'];
    }

    /**
     * Sets last_modified_date_time
     * @param \DateTime $last_modified_date_time
     * @return $this
     */
    public function setLastModifiedDateTime($last_modified_date_time)
    {
        $this->container['last_modified_date_time'] = $last_modified_date_time;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


