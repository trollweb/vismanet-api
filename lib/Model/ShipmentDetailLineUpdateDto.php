<?php
/**
 * ShipmentDetailLineUpdateDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * ShipmentDetailLineUpdateDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ShipmentDetailLineUpdateDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ShipmentDetailLineUpdateDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'operation' => 'string',
        'line_number' => '\Trollweb\VismaNetApi\Model\DtoValueInt32',
        'warehouse' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'location' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'uom' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'shipped_qty' => '\Trollweb\VismaNetApi\Model\DtoValueNullableDecimal',
        'lot_serial_nbr' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'expiration_date' => '\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime',
        'reason_code' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'description' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'allocations' => '\Trollweb\VismaNetApi\Model\AllocationsUpdateDto[]'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'operation' => 'operation',
        'line_number' => 'lineNumber',
        'warehouse' => 'warehouse',
        'location' => 'location',
        'uom' => 'uom',
        'shipped_qty' => 'shippedQty',
        'lot_serial_nbr' => 'lotSerialNbr',
        'expiration_date' => 'expirationDate',
        'reason_code' => 'reasonCode',
        'description' => 'description',
        'allocations' => 'allocations'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'operation' => 'setOperation',
        'line_number' => 'setLineNumber',
        'warehouse' => 'setWarehouse',
        'location' => 'setLocation',
        'uom' => 'setUom',
        'shipped_qty' => 'setShippedQty',
        'lot_serial_nbr' => 'setLotSerialNbr',
        'expiration_date' => 'setExpirationDate',
        'reason_code' => 'setReasonCode',
        'description' => 'setDescription',
        'allocations' => 'setAllocations'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'operation' => 'getOperation',
        'line_number' => 'getLineNumber',
        'warehouse' => 'getWarehouse',
        'location' => 'getLocation',
        'uom' => 'getUom',
        'shipped_qty' => 'getShippedQty',
        'lot_serial_nbr' => 'getLotSerialNbr',
        'expiration_date' => 'getExpirationDate',
        'reason_code' => 'getReasonCode',
        'description' => 'getDescription',
        'allocations' => 'getAllocations'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const OPERATION_INSERT = 'Insert';
    const OPERATION_UPDATE = 'Update';
    const OPERATION_DELETE = 'Delete';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getOperationAllowableValues()
    {
        return [
            self::OPERATION_INSERT,
            self::OPERATION_UPDATE,
            self::OPERATION_DELETE,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['operation'] = isset($data['operation']) ? $data['operation'] : null;
        $this->container['line_number'] = isset($data['line_number']) ? $data['line_number'] : null;
        $this->container['warehouse'] = isset($data['warehouse']) ? $data['warehouse'] : null;
        $this->container['location'] = isset($data['location']) ? $data['location'] : null;
        $this->container['uom'] = isset($data['uom']) ? $data['uom'] : null;
        $this->container['shipped_qty'] = isset($data['shipped_qty']) ? $data['shipped_qty'] : null;
        $this->container['lot_serial_nbr'] = isset($data['lot_serial_nbr']) ? $data['lot_serial_nbr'] : null;
        $this->container['expiration_date'] = isset($data['expiration_date']) ? $data['expiration_date'] : null;
        $this->container['reason_code'] = isset($data['reason_code']) ? $data['reason_code'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['allocations'] = isset($data['allocations']) ? $data['allocations'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("Insert", "Update", "Delete");
        if (!in_array($this->container['operation'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'operation', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("Insert", "Update", "Delete");
        if (!in_array($this->container['operation'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets operation
     * @return string
     */
    public function getOperation()
    {
        return $this->container['operation'];
    }

    /**
     * Sets operation
     * @param string $operation
     * @return $this
     */
    public function setOperation($operation)
    {
        $allowed_values = array('Insert', 'Update', 'Delete');
        if (!in_array($operation, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'operation', must be one of 'Insert', 'Update', 'Delete'");
        }
        $this->container['operation'] = $operation;

        return $this;
    }

    /**
     * Gets line_number
     * @return \Trollweb\VismaNetApi\Model\DtoValueInt32
     */
    public function getLineNumber()
    {
        return $this->container['line_number'];
    }

    /**
     * Sets line_number
     * @param \Trollweb\VismaNetApi\Model\DtoValueInt32 $line_number
     * @return $this
     */
    public function setLineNumber($line_number)
    {
        $this->container['line_number'] = $line_number;

        return $this;
    }

    /**
     * Gets warehouse
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getWarehouse()
    {
        return $this->container['warehouse'];
    }

    /**
     * Sets warehouse
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $warehouse
     * @return $this
     */
    public function setWarehouse($warehouse)
    {
        $this->container['warehouse'] = $warehouse;

        return $this;
    }

    /**
     * Gets location
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getLocation()
    {
        return $this->container['location'];
    }

    /**
     * Sets location
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->container['location'] = $location;

        return $this;
    }

    /**
     * Gets uom
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getUom()
    {
        return $this->container['uom'];
    }

    /**
     * Sets uom
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $uom
     * @return $this
     */
    public function setUom($uom)
    {
        $this->container['uom'] = $uom;

        return $this;
    }

    /**
     * Gets shipped_qty
     * @return \Trollweb\VismaNetApi\Model\DtoValueNullableDecimal
     */
    public function getShippedQty()
    {
        return $this->container['shipped_qty'];
    }

    /**
     * Sets shipped_qty
     * @param \Trollweb\VismaNetApi\Model\DtoValueNullableDecimal $shipped_qty
     * @return $this
     */
    public function setShippedQty($shipped_qty)
    {
        $this->container['shipped_qty'] = $shipped_qty;

        return $this;
    }

    /**
     * Gets lot_serial_nbr
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getLotSerialNbr()
    {
        return $this->container['lot_serial_nbr'];
    }

    /**
     * Sets lot_serial_nbr
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $lot_serial_nbr
     * @return $this
     */
    public function setLotSerialNbr($lot_serial_nbr)
    {
        $this->container['lot_serial_nbr'] = $lot_serial_nbr;

        return $this;
    }

    /**
     * Gets expiration_date
     * @return \Trollweb\VismaNetApi\Model\DtoValueNullableDateTime
     */
    public function getExpirationDate()
    {
        return $this->container['expiration_date'];
    }

    /**
     * Sets expiration_date
     * @param \Trollweb\VismaNetApi\Model\DtoValueNullableDateTime $expiration_date Property will become obsolete after version 5.31
     * @return $this
     */
    public function setExpirationDate($expiration_date)
    {
        $this->container['expiration_date'] = $expiration_date;

        return $this;
    }

    /**
     * Gets reason_code
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getReasonCode()
    {
        return $this->container['reason_code'];
    }

    /**
     * Sets reason_code
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $reason_code
     * @return $this
     */
    public function setReasonCode($reason_code)
    {
        $this->container['reason_code'] = $reason_code;

        return $this;
    }

    /**
     * Gets description
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets allocations
     * @return \Trollweb\VismaNetApi\Model\AllocationsUpdateDto[]
     */
    public function getAllocations()
    {
        return $this->container['allocations'];
    }

    /**
     * Sets allocations
     * @param \Trollweb\VismaNetApi\Model\AllocationsUpdateDto[] $allocations
     * @return $this
     */
    public function setAllocations($allocations)
    {
        $this->container['allocations'] = $allocations;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


