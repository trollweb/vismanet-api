<?php
/**
 * CreateDunningLetterActionResultDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * CreateDunningLetterActionResultDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class CreateDunningLetterActionResultDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'CreateDunningLetterActionResultDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'action_id' => 'string',
        'action_result' => 'string',
        'error_info' => 'string'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'action_id' => 'actionId',
        'action_result' => 'actionResult',
        'error_info' => 'errorInfo'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'action_id' => 'setActionId',
        'action_result' => 'setActionResult',
        'error_info' => 'setErrorInfo'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'action_id' => 'getActionId',
        'action_result' => 'getActionResult',
        'error_info' => 'getErrorInfo'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const ACTION_RESULT_QUEUED = 'Queued';
    const ACTION_RESULT_IN_PROCESS = 'InProcess';
    const ACTION_RESULT_FAILED = 'Failed';
    const ACTION_RESULT_DONE = 'Done';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getActionResultAllowableValues()
    {
        return [
            self::ACTION_RESULT_QUEUED,
            self::ACTION_RESULT_IN_PROCESS,
            self::ACTION_RESULT_FAILED,
            self::ACTION_RESULT_DONE,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['action_id'] = isset($data['action_id']) ? $data['action_id'] : null;
        $this->container['action_result'] = isset($data['action_result']) ? $data['action_result'] : null;
        $this->container['error_info'] = isset($data['error_info']) ? $data['error_info'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("Queued", "InProcess", "Failed", "Done");
        if (!in_array($this->container['action_result'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'action_result', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("Queued", "InProcess", "Failed", "Done");
        if (!in_array($this->container['action_result'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets action_id
     * @return string
     */
    public function getActionId()
    {
        return $this->container['action_id'];
    }

    /**
     * Sets action_id
     * @param string $action_id
     * @return $this
     */
    public function setActionId($action_id)
    {
        $this->container['action_id'] = $action_id;

        return $this;
    }

    /**
     * Gets action_result
     * @return string
     */
    public function getActionResult()
    {
        return $this->container['action_result'];
    }

    /**
     * Sets action_result
     * @param string $action_result
     * @return $this
     */
    public function setActionResult($action_result)
    {
        $allowed_values = array('Queued', 'InProcess', 'Failed', 'Done');
        if (!in_array($action_result, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'action_result', must be one of 'Queued', 'InProcess', 'Failed', 'Done'");
        }
        $this->container['action_result'] = $action_result;

        return $this;
    }

    /**
     * Gets error_info
     * @return string
     */
    public function getErrorInfo()
    {
        return $this->container['error_info'];
    }

    /**
     * Sets error_info
     * @param string $error_info
     * @return $this
     */
    public function setErrorInfo($error_info)
    {
        $this->container['error_info'] = $error_info;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


