<?php
/**
 * ExpenseClaimUpdateDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * ExpenseClaimUpdateDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ExpenseClaimUpdateDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'ExpenseClaimUpdateDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'date' => '\Trollweb\VismaNetApi\Model\DtoValueNullableDateTime',
        'description' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'claimed_by' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'customer' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'customer_update_answer' => 'string',
        'location' => '\Trollweb\VismaNetApi\Model\DtoValueString',
        'details' => '\Trollweb\VismaNetApi\Model\ExpenseClaimDetailUpdateDto[]',
        'extras' => 'map[string,\Trollweb\VismaNetApi\Model\Object]'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'date' => 'date',
        'description' => 'description',
        'claimed_by' => 'claimedBy',
        'customer' => 'customer',
        'customer_update_answer' => 'customerUpdateAnswer',
        'location' => 'location',
        'details' => 'details',
        'extras' => 'extras'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'date' => 'setDate',
        'description' => 'setDescription',
        'claimed_by' => 'setClaimedBy',
        'customer' => 'setCustomer',
        'customer_update_answer' => 'setCustomerUpdateAnswer',
        'location' => 'setLocation',
        'details' => 'setDetails',
        'extras' => 'setExtras'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'date' => 'getDate',
        'description' => 'getDescription',
        'claimed_by' => 'getClaimedBy',
        'customer' => 'getCustomer',
        'customer_update_answer' => 'getCustomerUpdateAnswer',
        'location' => 'getLocation',
        'details' => 'getDetails',
        'extras' => 'getExtras'
    );

    public static function getters()
    {
        return self::$getters;
    }

    const CUSTOMER_UPDATE_ANSWER_SELECTED_CUSTOMER = 'SelectedCustomer';
    const CUSTOMER_UPDATE_ANSWER_ALL_LINES = 'AllLines';
    const CUSTOMER_UPDATE_ANSWER_NOTHING = 'Nothing';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getCustomerUpdateAnswerAllowableValues()
    {
        return [
            self::CUSTOMER_UPDATE_ANSWER_SELECTED_CUSTOMER,
            self::CUSTOMER_UPDATE_ANSWER_ALL_LINES,
            self::CUSTOMER_UPDATE_ANSWER_NOTHING,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['date'] = isset($data['date']) ? $data['date'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['claimed_by'] = isset($data['claimed_by']) ? $data['claimed_by'] : null;
        $this->container['customer'] = isset($data['customer']) ? $data['customer'] : null;
        $this->container['customer_update_answer'] = isset($data['customer_update_answer']) ? $data['customer_update_answer'] : null;
        $this->container['location'] = isset($data['location']) ? $data['location'] : null;
        $this->container['details'] = isset($data['details']) ? $data['details'] : null;
        $this->container['extras'] = isset($data['extras']) ? $data['extras'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        $allowed_values = array("SelectedCustomer", "AllLines", "Nothing");
        if (!in_array($this->container['customer_update_answer'], $allowed_values)) {
            $invalid_properties[] = "invalid value for 'customer_update_answer', must be one of #{allowed_values}.";
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        $allowed_values = array("SelectedCustomer", "AllLines", "Nothing");
        if (!in_array($this->container['customer_update_answer'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets date
     * @return \Trollweb\VismaNetApi\Model\DtoValueNullableDateTime
     */
    public function getDate()
    {
        return $this->container['date'];
    }

    /**
     * Sets date
     * @param \Trollweb\VismaNetApi\Model\DtoValueNullableDateTime $date The date when the claim was entered.
     * @return $this
     */
    public function setDate($date)
    {
        $this->container['date'] = $date;

        return $this;
    }

    /**
     * Gets description
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $description A description of the claim.
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets claimed_by
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getClaimedBy()
    {
        return $this->container['claimed_by'];
    }

    /**
     * Sets claimed_by
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $claimed_by The employee who is claiming the expenses. If the claim is released, an Accounts Payable bill will be generated to this employee.
     * @return $this
     */
    public function setClaimedBy($claimed_by)
    {
        $this->container['claimed_by'] = $claimed_by;

        return $this;
    }

    /**
     * Gets customer
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getCustomer()
    {
        return $this->container['customer'];
    }

    /**
     * Sets customer
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $customer The applicable customer.
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->container['customer'] = $customer;

        return $this;
    }

    /**
     * Gets customer_update_answer
     * @return string
     */
    public function getCustomerUpdateAnswer()
    {
        return $this->container['customer_update_answer'];
    }

    /**
     * Sets customer_update_answer
     * @param string $customer_update_answer If the customer is updated the claim details customer information can be updated using the provided answer. By default 'SelectedCustomer' is selected.
     * @return $this
     */
    public function setCustomerUpdateAnswer($customer_update_answer)
    {
        $allowed_values = array('SelectedCustomer', 'AllLines', 'Nothing');
        if (!in_array($customer_update_answer, $allowed_values)) {
            throw new \InvalidArgumentException("Invalid value for 'customer_update_answer', must be one of 'SelectedCustomer', 'AllLines', 'Nothing'");
        }
        $this->container['customer_update_answer'] = $customer_update_answer;

        return $this;
    }

    /**
     * Gets location
     * @return \Trollweb\VismaNetApi\Model\DtoValueString
     */
    public function getLocation()
    {
        return $this->container['location'];
    }

    /**
     * Sets location
     * @param \Trollweb\VismaNetApi\Model\DtoValueString $location The company location associated with the claim.
     * @return $this
     */
    public function setLocation($location)
    {
        $this->container['location'] = $location;

        return $this;
    }

    /**
     * Gets details
     * @return \Trollweb\VismaNetApi\Model\ExpenseClaimDetailUpdateDto[]
     */
    public function getDetails()
    {
        return $this->container['details'];
    }

    /**
     * Sets details
     * @param \Trollweb\VismaNetApi\Model\ExpenseClaimDetailUpdateDto[] $details Expense Claim detail information
     * @return $this
     */
    public function setDetails($details)
    {
        $this->container['details'] = $details;

        return $this;
    }

    /**
     * Gets extras
     * @return map[string,\Trollweb\VismaNetApi\Model\Object]
     */
    public function getExtras()
    {
        return $this->container['extras'];
    }

    /**
     * Sets extras
     * @param map[string,\Trollweb\VismaNetApi\Model\Object] $extras
     * @return $this
     */
    public function setExtras($extras)
    {
        $this->container['extras'] = $extras;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


