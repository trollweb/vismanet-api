<?php
/**
 * InventoryTransferLineDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Model;

use \ArrayAccess;

/**
 * InventoryTransferLineDto Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Trollweb\VismaNetApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class InventoryTransferLineDto implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'InventoryTransferLineDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'to_location' => '\Trollweb\VismaNetApi\Model\LocationDto',
        'line_number' => 'int',
        'inventory_item' => '\Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto',
        'location' => '\Trollweb\VismaNetApi\Model\LocationDto',
        'quantity' => 'double',
        'uom' => 'string',
        'reason_code' => '\Trollweb\VismaNetApi\Model\ReasonCodeDto',
        'description' => 'string',
        'attachments' => '\Trollweb\VismaNetApi\Model\AttachmentDto[]',
        'branch_number' => '\Trollweb\VismaNetApi\Model\BranchNumberDto'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'to_location' => 'toLocation',
        'line_number' => 'lineNumber',
        'inventory_item' => 'inventoryItem',
        'location' => 'location',
        'quantity' => 'quantity',
        'uom' => 'uom',
        'reason_code' => 'reasonCode',
        'description' => 'description',
        'attachments' => 'attachments',
        'branch_number' => 'branchNumber'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'to_location' => 'setToLocation',
        'line_number' => 'setLineNumber',
        'inventory_item' => 'setInventoryItem',
        'location' => 'setLocation',
        'quantity' => 'setQuantity',
        'uom' => 'setUom',
        'reason_code' => 'setReasonCode',
        'description' => 'setDescription',
        'attachments' => 'setAttachments',
        'branch_number' => 'setBranchNumber'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'to_location' => 'getToLocation',
        'line_number' => 'getLineNumber',
        'inventory_item' => 'getInventoryItem',
        'location' => 'getLocation',
        'quantity' => 'getQuantity',
        'uom' => 'getUom',
        'reason_code' => 'getReasonCode',
        'description' => 'getDescription',
        'attachments' => 'getAttachments',
        'branch_number' => 'getBranchNumber'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['to_location'] = isset($data['to_location']) ? $data['to_location'] : null;
        $this->container['line_number'] = isset($data['line_number']) ? $data['line_number'] : null;
        $this->container['inventory_item'] = isset($data['inventory_item']) ? $data['inventory_item'] : null;
        $this->container['location'] = isset($data['location']) ? $data['location'] : null;
        $this->container['quantity'] = isset($data['quantity']) ? $data['quantity'] : null;
        $this->container['uom'] = isset($data['uom']) ? $data['uom'] : null;
        $this->container['reason_code'] = isset($data['reason_code']) ? $data['reason_code'] : null;
        $this->container['description'] = isset($data['description']) ? $data['description'] : null;
        $this->container['attachments'] = isset($data['attachments']) ? $data['attachments'] : null;
        $this->container['branch_number'] = isset($data['branch_number']) ? $data['branch_number'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets to_location
     * @return \Trollweb\VismaNetApi\Model\LocationDto
     */
    public function getToLocation()
    {
        return $this->container['to_location'];
    }

    /**
     * Sets to_location
     * @param \Trollweb\VismaNetApi\Model\LocationDto $to_location
     * @return $this
     */
    public function setToLocation($to_location)
    {
        $this->container['to_location'] = $to_location;

        return $this;
    }

    /**
     * Gets line_number
     * @return int
     */
    public function getLineNumber()
    {
        return $this->container['line_number'];
    }

    /**
     * Sets line_number
     * @param int $line_number
     * @return $this
     */
    public function setLineNumber($line_number)
    {
        $this->container['line_number'] = $line_number;

        return $this;
    }

    /**
     * Gets inventory_item
     * @return \Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto
     */
    public function getInventoryItem()
    {
        return $this->container['inventory_item'];
    }

    /**
     * Sets inventory_item
     * @param \Trollweb\VismaNetApi\Model\InventoryNumberDescriptionDto $inventory_item
     * @return $this
     */
    public function setInventoryItem($inventory_item)
    {
        $this->container['inventory_item'] = $inventory_item;

        return $this;
    }

    /**
     * Gets location
     * @return \Trollweb\VismaNetApi\Model\LocationDto
     */
    public function getLocation()
    {
        return $this->container['location'];
    }

    /**
     * Sets location
     * @param \Trollweb\VismaNetApi\Model\LocationDto $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->container['location'] = $location;

        return $this;
    }

    /**
     * Gets quantity
     * @return double
     */
    public function getQuantity()
    {
        return $this->container['quantity'];
    }

    /**
     * Sets quantity
     * @param double $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->container['quantity'] = $quantity;

        return $this;
    }

    /**
     * Gets uom
     * @return string
     */
    public function getUom()
    {
        return $this->container['uom'];
    }

    /**
     * Sets uom
     * @param string $uom
     * @return $this
     */
    public function setUom($uom)
    {
        $this->container['uom'] = $uom;

        return $this;
    }

    /**
     * Gets reason_code
     * @return \Trollweb\VismaNetApi\Model\ReasonCodeDto
     */
    public function getReasonCode()
    {
        return $this->container['reason_code'];
    }

    /**
     * Sets reason_code
     * @param \Trollweb\VismaNetApi\Model\ReasonCodeDto $reason_code
     * @return $this
     */
    public function setReasonCode($reason_code)
    {
        $this->container['reason_code'] = $reason_code;

        return $this;
    }

    /**
     * Gets description
     * @return string
     */
    public function getDescription()
    {
        return $this->container['description'];
    }

    /**
     * Sets description
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->container['description'] = $description;

        return $this;
    }

    /**
     * Gets attachments
     * @return \Trollweb\VismaNetApi\Model\AttachmentDto[]
     */
    public function getAttachments()
    {
        return $this->container['attachments'];
    }

    /**
     * Sets attachments
     * @param \Trollweb\VismaNetApi\Model\AttachmentDto[] $attachments
     * @return $this
     */
    public function setAttachments($attachments)
    {
        $this->container['attachments'] = $attachments;

        return $this;
    }

    /**
     * Gets branch_number
     * @return \Trollweb\VismaNetApi\Model\BranchNumberDto
     */
    public function getBranchNumber()
    {
        return $this->container['branch_number'];
    }

    /**
     * Sets branch_number
     * @param \Trollweb\VismaNetApi\Model\BranchNumberDto $branch_number
     * @return $this
     */
    public function setBranchNumber($branch_number)
    {
        $this->container['branch_number'] = $branch_number;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Trollweb\VismaNetApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


