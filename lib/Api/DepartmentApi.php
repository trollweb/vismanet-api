<?php
/**
 * DepartmentApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Api;

use \Trollweb\VismaNetApi\Configuration;
use \Trollweb\VismaNetApi\ApiClient;
use \Trollweb\VismaNetApi\ApiException;
use \Trollweb\VismaNetApi\ObjectSerializer;

/**
 * DepartmentApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class DepartmentApi
{

    /**
     * API Client
     *
     * @var \Trollweb\VismaNetApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\VismaNetApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\VismaNetApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://localhost/API');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\VismaNetApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\VismaNetApi\ApiClient $apiClient set the API client
     *
     * @return DepartmentApi
     */
    public function setApiClient(\Trollweb\VismaNetApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation departmentCreateDepartment
     *
     * Creates a department
     *
     * @param \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto $department_update_dto Defines the data for the department to create (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentCreateDepartment($department_update_dto)
    {
        list($response) = $this->departmentCreateDepartmentWithHttpInfo($department_update_dto);
        return $response;
    }

    /**
     * Operation departmentCreateDepartmentWithHttpInfo
     *
     * Creates a department
     *
     * @param \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto $department_update_dto Defines the data for the department to create (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentCreateDepartmentWithHttpInfo($department_update_dto)
    {
        // verify the required parameter 'department_update_dto' is set
        if ($department_update_dto === null) {
            throw new \InvalidArgumentException('Missing the required parameter $department_update_dto when calling departmentCreateDepartment');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/department";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($department_update_dto)) {
            $_tempBody = $department_update_dto;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/department'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation departmentGetAllDepartmentDtos
     *
     * Get a range of department
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return \Trollweb\VismaNetApi\Model\DepartmentDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentGetAllDepartmentDtos($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        list($response) = $this->departmentGetAllDepartmentDtosWithHttpInfo($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
        return $response;
    }

    /**
     * Operation departmentGetAllDepartmentDtosWithHttpInfo
     *
     * Get a range of department
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\DepartmentDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentGetAllDepartmentDtosWithHttpInfo($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/department";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\DepartmentDto[]',
                '/controller/api/v1/department'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\DepartmentDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\DepartmentDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation departmentGetDepartmentBydepartmentId
     *
     * Get a specific department
     *
     * @param string $department_id Identifies the department (required)
     * @return \Trollweb\VismaNetApi\Model\DepartmentDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentGetDepartmentBydepartmentId($department_id)
    {
        list($response) = $this->departmentGetDepartmentBydepartmentIdWithHttpInfo($department_id);
        return $response;
    }

    /**
     * Operation departmentGetDepartmentBydepartmentIdWithHttpInfo
     *
     * Get a specific department
     *
     * @param string $department_id Identifies the department (required)
     * @return Array of \Trollweb\VismaNetApi\Model\DepartmentDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentGetDepartmentBydepartmentIdWithHttpInfo($department_id)
    {
        // verify the required parameter 'department_id' is set
        if ($department_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $department_id when calling departmentGetDepartmentBydepartmentId');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/department/{departmentId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($department_id !== null) {
            $resourcePath = str_replace(
                "{" . "departmentId" . "}",
                $this->apiClient->getSerializer()->toPathValue($department_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\DepartmentDto',
                '/controller/api/v1/department/{departmentId}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\DepartmentDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\DepartmentDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation departmentUpdateDepartmentBydepartmentId
     *
     * Updates a specific department
     *
     * @param string $department_id Identifies the department to update (required)
     * @param \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto $department_update_dto The data to update the department with (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentUpdateDepartmentBydepartmentId($department_id, $department_update_dto)
    {
        list($response) = $this->departmentUpdateDepartmentBydepartmentIdWithHttpInfo($department_id, $department_update_dto);
        return $response;
    }

    /**
     * Operation departmentUpdateDepartmentBydepartmentIdWithHttpInfo
     *
     * Updates a specific department
     *
     * @param string $department_id Identifies the department to update (required)
     * @param \Trollweb\VismaNetApi\Model\DepartmentUpdateBaseDto $department_update_dto The data to update the department with (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function departmentUpdateDepartmentBydepartmentIdWithHttpInfo($department_id, $department_update_dto)
    {
        // verify the required parameter 'department_id' is set
        if ($department_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $department_id when calling departmentUpdateDepartmentBydepartmentId');
        }
        // verify the required parameter 'department_update_dto' is set
        if ($department_update_dto === null) {
            throw new \InvalidArgumentException('Missing the required parameter $department_update_dto when calling departmentUpdateDepartmentBydepartmentId');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/department/{departmentId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // path params
        if ($department_id !== null) {
            $resourcePath = str_replace(
                "{" . "departmentId" . "}",
                $this->apiClient->getSerializer()->toPathValue($department_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($department_update_dto)) {
            $_tempBody = $department_update_dto;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/department/{departmentId}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 204:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
