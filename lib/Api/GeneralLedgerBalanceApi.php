<?php
/**
 * GeneralLedgerBalanceApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Api;

use \Trollweb\VismaNetApi\Configuration;
use \Trollweb\VismaNetApi\ApiClient;
use \Trollweb\VismaNetApi\ApiException;
use \Trollweb\VismaNetApi\ObjectSerializer;

/**
 * GeneralLedgerBalanceApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class GeneralLedgerBalanceApi
{

    /**
     * API Client
     *
     * @var \Trollweb\VismaNetApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\VismaNetApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\VismaNetApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://localhost/API');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\VismaNetApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\VismaNetApi\ApiClient $apiClient set the API client
     *
     * @return GeneralLedgerBalanceApi
     */
    public function setApiClient(\Trollweb\VismaNetApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation generalLedgerBalanceGetAll
     *
     * Get a range of General Ledger Balances
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $period_id  (optional)
     * @param string $period_id_condition  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @param string $account_id  (optional)
     * @param int $year_closed  (optional)
     * @param string $balance_type  (optional)
     * @return \Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function generalLedgerBalanceGetAll($greater_than_value = null, $number_to_read = null, $skip_records = null, $period_id = null, $period_id_condition = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null, $account_id = null, $year_closed = null, $balance_type = null)
    {
        list($response) = $this->generalLedgerBalanceGetAllWithHttpInfo($greater_than_value, $number_to_read, $skip_records, $period_id, $period_id_condition, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $account_id, $year_closed, $balance_type);
        return $response;
    }

    /**
     * Operation generalLedgerBalanceGetAllWithHttpInfo
     *
     * Get a range of General Ledger Balances
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $period_id  (optional)
     * @param string $period_id_condition  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @param string $account_id  (optional)
     * @param int $year_closed  (optional)
     * @param string $balance_type  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function generalLedgerBalanceGetAllWithHttpInfo($greater_than_value = null, $number_to_read = null, $skip_records = null, $period_id = null, $period_id_condition = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null, $account_id = null, $year_closed = null, $balance_type = null)
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/generalLedgerBalance";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($period_id !== null) {
            $queryParams['periodId'] = $this->apiClient->getSerializer()->toQueryValue($period_id);
        }
        // query params
        if ($period_id_condition !== null) {
            $queryParams['periodIdCondition'] = $this->apiClient->getSerializer()->toQueryValue($period_id_condition);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // query params
        if ($account_id !== null) {
            $queryParams['accountId'] = $this->apiClient->getSerializer()->toQueryValue($account_id);
        }
        // query params
        if ($year_closed !== null) {
            $queryParams['yearClosed'] = $this->apiClient->getSerializer()->toQueryValue($year_closed);
        }
        // query params
        if ($balance_type !== null) {
            $queryParams['balanceType'] = $this->apiClient->getSerializer()->toQueryValue($balance_type);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[]',
                '/controller/api/v1/generalLedgerBalance'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\GeneralLedgerBalanceDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
