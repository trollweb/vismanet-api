<?php
/**
 * InventoryApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Api;

use \Trollweb\VismaNetApi\Configuration;
use \Trollweb\VismaNetApi\ApiClient;
use \Trollweb\VismaNetApi\ApiException;
use \Trollweb\VismaNetApi\ObjectSerializer;

/**
 * InventoryApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class InventoryApi
{

    /**
     * API Client
     *
     * @var \Trollweb\VismaNetApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\VismaNetApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\VismaNetApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://localhost/API');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\VismaNetApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\VismaNetApi\ApiClient $apiClient set the API client
     *
     * @return InventoryApi
     */
    public function setApiClient(\Trollweb\VismaNetApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation inventoryGetAll
     *
     * Get a range of Inventory items
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @param string $alternate_id  (optional)
     * @param int $sales_category  (optional)
     * @param string $attributes  (optional)
     * @return \Trollweb\VismaNetApi\Model\InventoryDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetAll($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null, $alternate_id = null, $sales_category = null, $attributes = null)
    {
        list($response) = $this->inventoryGetAllWithHttpInfo($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $alternate_id, $sales_category, $attributes);
        return $response;
    }

    /**
     * Operation inventoryGetAllWithHttpInfo
     *
     * Get a range of Inventory items
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @param string $alternate_id  (optional)
     * @param int $sales_category  (optional)
     * @param string $attributes  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\InventoryDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetAllWithHttpInfo($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null, $alternate_id = null, $sales_category = null, $attributes = null)
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // query params
        if ($alternate_id !== null) {
            $queryParams['alternateID'] = $this->apiClient->getSerializer()->toQueryValue($alternate_id);
        }
        // query params
        if ($sales_category !== null) {
            $queryParams['salesCategory'] = $this->apiClient->getSerializer()->toQueryValue($sales_category);
        }
        // query params
        if ($attributes !== null) {
            $queryParams['attributes'] = $this->apiClient->getSerializer()->toQueryValue($attributes);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\InventoryDto[]',
                '/controller/api/v1/inventory'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\InventoryDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\InventoryDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation inventoryGetByinventoryNumber
     *
     * Get a specific Inventory item
     *
     * @param string $inventory_number Identifies the Inventory item (required)
     * @return \Trollweb\VismaNetApi\Model\InventoryDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetByinventoryNumber($inventory_number)
    {
        list($response) = $this->inventoryGetByinventoryNumberWithHttpInfo($inventory_number);
        return $response;
    }

    /**
     * Operation inventoryGetByinventoryNumberWithHttpInfo
     *
     * Get a specific Inventory item
     *
     * @param string $inventory_number Identifies the Inventory item (required)
     * @return Array of \Trollweb\VismaNetApi\Model\InventoryDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetByinventoryNumberWithHttpInfo($inventory_number)
    {
        // verify the required parameter 'inventory_number' is set
        if ($inventory_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $inventory_number when calling inventoryGetByinventoryNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory/{inventoryNumber}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($inventory_number !== null) {
            $resourcePath = str_replace(
                "{" . "inventoryNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($inventory_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\InventoryDto',
                '/controller/api/v1/inventory/{inventoryNumber}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\InventoryDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\InventoryDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation inventoryGetItemClasses
     *
     * Get Inventory Item Classes
     *
     * @return \Trollweb\VismaNetApi\Model\ItemClassDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetItemClasses()
    {
        list($response) = $this->inventoryGetItemClassesWithHttpInfo();
        return $response;
    }

    /**
     * Operation inventoryGetItemClassesWithHttpInfo
     *
     * Get Inventory Item Classes
     *
     * @return Array of \Trollweb\VismaNetApi\Model\ItemClassDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetItemClassesWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory/itemClass";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\ItemClassDto[]',
                '/controller/api/v1/inventory/itemClass'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\ItemClassDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\ItemClassDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation inventoryGetItemPostClasses
     *
     * Get Inventory Item Post Classes
     *
     * @return \Trollweb\VismaNetApi\Model\PostingClassDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetItemPostClasses()
    {
        list($response) = $this->inventoryGetItemPostClassesWithHttpInfo();
        return $response;
    }

    /**
     * Operation inventoryGetItemPostClassesWithHttpInfo
     *
     * Get Inventory Item Post Classes
     *
     * @return Array of \Trollweb\VismaNetApi\Model\PostingClassDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetItemPostClassesWithHttpInfo()
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory/itemPostClass";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\PostingClassDto[]',
                '/controller/api/v1/inventory/itemPostClass'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\PostingClassDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\PostingClassDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation inventoryGetSpecificItemClassByitemClassNumber
     *
     * Get Specific Inventory Item Class
     *
     * @param string $item_class_number  (required)
     * @return \Trollweb\VismaNetApi\Model\ItemClassDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetSpecificItemClassByitemClassNumber($item_class_number)
    {
        list($response) = $this->inventoryGetSpecificItemClassByitemClassNumberWithHttpInfo($item_class_number);
        return $response;
    }

    /**
     * Operation inventoryGetSpecificItemClassByitemClassNumberWithHttpInfo
     *
     * Get Specific Inventory Item Class
     *
     * @param string $item_class_number  (required)
     * @return Array of \Trollweb\VismaNetApi\Model\ItemClassDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryGetSpecificItemClassByitemClassNumberWithHttpInfo($item_class_number)
    {
        // verify the required parameter 'item_class_number' is set
        if ($item_class_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $item_class_number when calling inventoryGetSpecificItemClassByitemClassNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory/itemclass/{itemClassNumber}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($item_class_number !== null) {
            $resourcePath = str_replace(
                "{" . "itemClassNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($item_class_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\ItemClassDto',
                '/controller/api/v1/inventory/itemclass/{itemClassNumber}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\ItemClassDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\ItemClassDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation inventoryPost
     *
     * Create an inventory item
     *
     * @param \Trollweb\VismaNetApi\Model\InventoryUpdateDto $inventory Define the data for the inventory item to create (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryPost($inventory)
    {
        list($response) = $this->inventoryPostWithHttpInfo($inventory);
        return $response;
    }

    /**
     * Operation inventoryPostWithHttpInfo
     *
     * Create an inventory item
     *
     * @param \Trollweb\VismaNetApi\Model\InventoryUpdateDto $inventory Define the data for the inventory item to create (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryPostWithHttpInfo($inventory)
    {
        // verify the required parameter 'inventory' is set
        if ($inventory === null) {
            throw new \InvalidArgumentException('Missing the required parameter $inventory when calling inventoryPost');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($inventory)) {
            $_tempBody = $inventory;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/inventory'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation inventoryPutByinventoryCd
     *
     * Update a specific inventory item
     *
     * @param string $inventory_cd Identifies the inventory item to update (required)
     * @param \Trollweb\VismaNetApi\Model\InventoryUpdateDto $inventory The data to update for inventory item (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryPutByinventoryCd($inventory_cd, $inventory)
    {
        list($response) = $this->inventoryPutByinventoryCdWithHttpInfo($inventory_cd, $inventory);
        return $response;
    }

    /**
     * Operation inventoryPutByinventoryCdWithHttpInfo
     *
     * Update a specific inventory item
     *
     * @param string $inventory_cd Identifies the inventory item to update (required)
     * @param \Trollweb\VismaNetApi\Model\InventoryUpdateDto $inventory The data to update for inventory item (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function inventoryPutByinventoryCdWithHttpInfo($inventory_cd, $inventory)
    {
        // verify the required parameter 'inventory_cd' is set
        if ($inventory_cd === null) {
            throw new \InvalidArgumentException('Missing the required parameter $inventory_cd when calling inventoryPutByinventoryCd');
        }
        // verify the required parameter 'inventory' is set
        if ($inventory === null) {
            throw new \InvalidArgumentException('Missing the required parameter $inventory when calling inventoryPutByinventoryCd');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/inventory/{inventoryCd}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // path params
        if ($inventory_cd !== null) {
            $resourcePath = str_replace(
                "{" . "inventoryCd" . "}",
                $this->apiClient->getSerializer()->toPathValue($inventory_cd),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($inventory)) {
            $_tempBody = $inventory;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/inventory/{inventoryCd}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 204:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
