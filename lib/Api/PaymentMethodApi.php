<?php
/**
 * PaymentMethodApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Api;

use \Trollweb\VismaNetApi\Configuration;
use \Trollweb\VismaNetApi\ApiClient;
use \Trollweb\VismaNetApi\ApiException;
use \Trollweb\VismaNetApi\ObjectSerializer;

/**
 * PaymentMethodApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PaymentMethodApi
{

    /**
     * API Client
     *
     * @var \Trollweb\VismaNetApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\VismaNetApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\VismaNetApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://localhost/API');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\VismaNetApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\VismaNetApi\ApiClient $apiClient set the API client
     *
     * @return PaymentMethodApi
     */
    public function setApiClient(\Trollweb\VismaNetApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation paymentMethodGetAllPaymentMethod
     *
     * Get a range of Payment Method
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return \Trollweb\VismaNetApi\Model\PaymentMethodDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function paymentMethodGetAllPaymentMethod($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        list($response) = $this->paymentMethodGetAllPaymentMethodWithHttpInfo($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
        return $response;
    }

    /**
     * Operation paymentMethodGetAllPaymentMethodWithHttpInfo
     *
     * Get a range of Payment Method
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\PaymentMethodDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function paymentMethodGetAllPaymentMethodWithHttpInfo($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/paymentmethod";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\PaymentMethodDto[]',
                '/controller/api/v1/paymentmethod'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\PaymentMethodDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\PaymentMethodDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation paymentMethodGetBypaymentMethodNumber
     *
     * Get a specific Payment Method
     *
     * @param string $payment_method_number Identifies the Payment Method (required)
     * @return \Trollweb\VismaNetApi\Model\PaymentMethodDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function paymentMethodGetBypaymentMethodNumber($payment_method_number)
    {
        list($response) = $this->paymentMethodGetBypaymentMethodNumberWithHttpInfo($payment_method_number);
        return $response;
    }

    /**
     * Operation paymentMethodGetBypaymentMethodNumberWithHttpInfo
     *
     * Get a specific Payment Method
     *
     * @param string $payment_method_number Identifies the Payment Method (required)
     * @return Array of \Trollweb\VismaNetApi\Model\PaymentMethodDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function paymentMethodGetBypaymentMethodNumberWithHttpInfo($payment_method_number)
    {
        // verify the required parameter 'payment_method_number' is set
        if ($payment_method_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $payment_method_number when calling paymentMethodGetBypaymentMethodNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/paymentmethod/{paymentMethodNumber}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($payment_method_number !== null) {
            $resourcePath = str_replace(
                "{" . "paymentMethodNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($payment_method_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\PaymentMethodDto',
                '/controller/api/v1/paymentmethod/{paymentMethodNumber}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\PaymentMethodDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\PaymentMethodDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
