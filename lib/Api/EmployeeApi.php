<?php
/**
 * EmployeeApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Api;

use \Trollweb\VismaNetApi\Configuration;
use \Trollweb\VismaNetApi\ApiClient;
use \Trollweb\VismaNetApi\ApiException;
use \Trollweb\VismaNetApi\ObjectSerializer;

/**
 * EmployeeApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class EmployeeApi
{

    /**
     * API Client
     *
     * @var \Trollweb\VismaNetApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\VismaNetApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\VismaNetApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://localhost/API');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\VismaNetApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\VismaNetApi\ApiClient $apiClient set the API client
     *
     * @return EmployeeApi
     */
    public function setApiClient(\Trollweb\VismaNetApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation employeeGetAllEmplyee
     *
     * Get a range of employees
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return \Trollweb\VismaNetApi\Model\EmployeeDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetAllEmplyee($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        list($response) = $this->employeeGetAllEmplyeeWithHttpInfo($greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
        return $response;
    }

    /**
     * Operation employeeGetAllEmplyeeWithHttpInfo
     *
     * Get a range of employees
     *
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\EmployeeDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetAllEmplyeeWithHttpInfo($greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/employee";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\EmployeeDto[]',
                '/controller/api/v1/employee'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\EmployeeDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\EmployeeDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation employeeGetEmployeeByemployeeCd
     *
     * Get a specific employee
     *
     * @param string $employee_cd Identifies the employee (required)
     * @return \Trollweb\VismaNetApi\Model\EmployeeDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeByemployeeCd($employee_cd)
    {
        list($response) = $this->employeeGetEmployeeByemployeeCdWithHttpInfo($employee_cd);
        return $response;
    }

    /**
     * Operation employeeGetEmployeeByemployeeCdWithHttpInfo
     *
     * Get a specific employee
     *
     * @param string $employee_cd Identifies the employee (required)
     * @return Array of \Trollweb\VismaNetApi\Model\EmployeeDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeByemployeeCdWithHttpInfo($employee_cd)
    {
        // verify the required parameter 'employee_cd' is set
        if ($employee_cd === null) {
            throw new \InvalidArgumentException('Missing the required parameter $employee_cd when calling employeeGetEmployeeByemployeeCd');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/employee/{employeeCd}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($employee_cd !== null) {
            $resourcePath = str_replace(
                "{" . "employeeCd" . "}",
                $this->apiClient->getSerializer()->toPathValue($employee_cd),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\EmployeeDto',
                '/controller/api/v1/employee/{employeeCd}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\EmployeeDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\EmployeeDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation employeeGetEmployeeExpenseClaimsByemployeeCd
     *
     * Get expense claims for a specific employee
     *
     * @param string $employee_cd Identifies the employee (required)
     * @param string $status  (optional)
     * @param \DateTime $date  (optional)
     * @param string $customer  (optional)
     * @param string $department_id  (optional)
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return \Trollweb\VismaNetApi\Model\ExpenseClaimDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeExpenseClaimsByemployeeCd($employee_cd, $status = null, $date = null, $customer = null, $department_id = null, $greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        list($response) = $this->employeeGetEmployeeExpenseClaimsByemployeeCdWithHttpInfo($employee_cd, $status, $date, $customer, $department_id, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
        return $response;
    }

    /**
     * Operation employeeGetEmployeeExpenseClaimsByemployeeCdWithHttpInfo
     *
     * Get expense claims for a specific employee
     *
     * @param string $employee_cd Identifies the employee (required)
     * @param string $status  (optional)
     * @param \DateTime $date  (optional)
     * @param string $customer  (optional)
     * @param string $department_id  (optional)
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\ExpenseClaimDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeExpenseClaimsByemployeeCdWithHttpInfo($employee_cd, $status = null, $date = null, $customer = null, $department_id = null, $greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        // verify the required parameter 'employee_cd' is set
        if ($employee_cd === null) {
            throw new \InvalidArgumentException('Missing the required parameter $employee_cd when calling employeeGetEmployeeExpenseClaimsByemployeeCd');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/employee/{employeeCd}/expenseClaim";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($status !== null) {
            $queryParams['status'] = $this->apiClient->getSerializer()->toQueryValue($status);
        }
        // query params
        if ($date !== null) {
            $queryParams['date'] = $this->apiClient->getSerializer()->toQueryValue($date);
        }
        // query params
        if ($customer !== null) {
            $queryParams['customer'] = $this->apiClient->getSerializer()->toQueryValue($customer);
        }
        // query params
        if ($department_id !== null) {
            $queryParams['departmentId'] = $this->apiClient->getSerializer()->toQueryValue($department_id);
        }
        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // path params
        if ($employee_cd !== null) {
            $resourcePath = str_replace(
                "{" . "employeeCd" . "}",
                $this->apiClient->getSerializer()->toPathValue($employee_cd),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\ExpenseClaimDto[]',
                '/controller/api/v1/employee/{employeeCd}/expenseClaim'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\ExpenseClaimDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\ExpenseClaimDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation employeeGetEmployeeExpenseReceiptsByemployeeCd
     *
     * Get expense receipts for a specific employee
     *
     * @param string $employee_cd Identifies the employee (required)
     * @param string $date  (optional)
     * @param string $date_condition  (optional)
     * @param string $inventory  (optional)
     * @param string $project  (optional)
     * @param string $claimed_by  (optional)
     * @param string $project_task  (optional)
     * @param bool $invoiceable  (optional)
     * @param string $status  (optional)
     * @param string $customer  (optional)
     * @return \Trollweb\VismaNetApi\Model\ExpenseReceiptDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeExpenseReceiptsByemployeeCd($employee_cd, $date = null, $date_condition = null, $inventory = null, $project = null, $claimed_by = null, $project_task = null, $invoiceable = null, $status = null, $customer = null)
    {
        list($response) = $this->employeeGetEmployeeExpenseReceiptsByemployeeCdWithHttpInfo($employee_cd, $date, $date_condition, $inventory, $project, $claimed_by, $project_task, $invoiceable, $status, $customer);
        return $response;
    }

    /**
     * Operation employeeGetEmployeeExpenseReceiptsByemployeeCdWithHttpInfo
     *
     * Get expense receipts for a specific employee
     *
     * @param string $employee_cd Identifies the employee (required)
     * @param string $date  (optional)
     * @param string $date_condition  (optional)
     * @param string $inventory  (optional)
     * @param string $project  (optional)
     * @param string $claimed_by  (optional)
     * @param string $project_task  (optional)
     * @param bool $invoiceable  (optional)
     * @param string $status  (optional)
     * @param string $customer  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\ExpenseReceiptDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeExpenseReceiptsByemployeeCdWithHttpInfo($employee_cd, $date = null, $date_condition = null, $inventory = null, $project = null, $claimed_by = null, $project_task = null, $invoiceable = null, $status = null, $customer = null)
    {
        // verify the required parameter 'employee_cd' is set
        if ($employee_cd === null) {
            throw new \InvalidArgumentException('Missing the required parameter $employee_cd when calling employeeGetEmployeeExpenseReceiptsByemployeeCd');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/employee/{employeeCd}/expenseReceipt";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($date !== null) {
            $queryParams['date'] = $this->apiClient->getSerializer()->toQueryValue($date);
        }
        // query params
        if ($date_condition !== null) {
            $queryParams['dateCondition'] = $this->apiClient->getSerializer()->toQueryValue($date_condition);
        }
        // query params
        if ($inventory !== null) {
            $queryParams['inventory'] = $this->apiClient->getSerializer()->toQueryValue($inventory);
        }
        // query params
        if ($project !== null) {
            $queryParams['project'] = $this->apiClient->getSerializer()->toQueryValue($project);
        }
        // query params
        if ($claimed_by !== null) {
            $queryParams['claimedBy'] = $this->apiClient->getSerializer()->toQueryValue($claimed_by);
        }
        // query params
        if ($project_task !== null) {
            $queryParams['projectTask'] = $this->apiClient->getSerializer()->toQueryValue($project_task);
        }
        // query params
        if ($invoiceable !== null) {
            $queryParams['invoiceable'] = $this->apiClient->getSerializer()->toQueryValue($invoiceable);
        }
        // query params
        if ($status !== null) {
            $queryParams['status'] = $this->apiClient->getSerializer()->toQueryValue($status);
        }
        // query params
        if ($customer !== null) {
            $queryParams['customer'] = $this->apiClient->getSerializer()->toQueryValue($customer);
        }
        // path params
        if ($employee_cd !== null) {
            $resourcePath = str_replace(
                "{" . "employeeCd" . "}",
                $this->apiClient->getSerializer()->toPathValue($employee_cd),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\ExpenseReceiptDto[]',
                '/controller/api/v1/employee/{employeeCd}/expenseReceipt'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\ExpenseReceiptDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\ExpenseReceiptDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation employeeGetEmployeeTimeCardsByemployeeCd
     *
     * Get a specific employee time cards
     *
     * @param string $employee_cd Identifies the employee (required)
     * @param string $status  (optional)
     * @param string $week  (optional)
     * @param string $type  (optional)
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return \Trollweb\VismaNetApi\Model\TimeCardDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeTimeCardsByemployeeCd($employee_cd, $status = null, $week = null, $type = null, $greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        list($response) = $this->employeeGetEmployeeTimeCardsByemployeeCdWithHttpInfo($employee_cd, $status, $week, $type, $greater_than_value, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition);
        return $response;
    }

    /**
     * Operation employeeGetEmployeeTimeCardsByemployeeCdWithHttpInfo
     *
     * Get a specific employee time cards
     *
     * @param string $employee_cd Identifies the employee (required)
     * @param string $status  (optional)
     * @param string $week  (optional)
     * @param string $type  (optional)
     * @param string $greater_than_value  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\TimeCardDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function employeeGetEmployeeTimeCardsByemployeeCdWithHttpInfo($employee_cd, $status = null, $week = null, $type = null, $greater_than_value = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null)
    {
        // verify the required parameter 'employee_cd' is set
        if ($employee_cd === null) {
            throw new \InvalidArgumentException('Missing the required parameter $employee_cd when calling employeeGetEmployeeTimeCardsByemployeeCd');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/employee/{employeeCd}/timecards";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($status !== null) {
            $queryParams['status'] = $this->apiClient->getSerializer()->toQueryValue($status);
        }
        // query params
        if ($week !== null) {
            $queryParams['week'] = $this->apiClient->getSerializer()->toQueryValue($week);
        }
        // query params
        if ($type !== null) {
            $queryParams['type'] = $this->apiClient->getSerializer()->toQueryValue($type);
        }
        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // path params
        if ($employee_cd !== null) {
            $resourcePath = str_replace(
                "{" . "employeeCd" . "}",
                $this->apiClient->getSerializer()->toPathValue($employee_cd),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\TimeCardDto[]',
                '/controller/api/v1/employee/{employeeCd}/timecards'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\TimeCardDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\TimeCardDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
