<?php
/**
 * SupplierInvoiceApi
 * PHP version 5
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Visma Net API
 *
 * No descripton provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 6.10.01.0003
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Trollweb\VismaNetApi\Api;

use \Trollweb\VismaNetApi\Configuration;
use \Trollweb\VismaNetApi\ApiClient;
use \Trollweb\VismaNetApi\ApiException;
use \Trollweb\VismaNetApi\ObjectSerializer;

/**
 * SupplierInvoiceApi Class Doc Comment
 *
 * @category Class
 * @package  Trollweb\VismaNetApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class SupplierInvoiceApi
{

    /**
     * API Client
     *
     * @var \Trollweb\VismaNetApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Trollweb\VismaNetApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Trollweb\VismaNetApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://localhost/API');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Trollweb\VismaNetApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Trollweb\VismaNetApi\ApiClient $apiClient set the API client
     *
     * @return SupplierInvoiceApi
     */
    public function setApiClient(\Trollweb\VismaNetApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation supplierInvoiceCreateHeaderAttachmentByinvoiceNumber
     *
     * Creates an attachment and associates it with a supplier invoice. If the file already exists, a new revision is created.
     *
     * @param string $invoice_number Identifies the supplier invoice (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceCreateHeaderAttachmentByinvoiceNumber($invoice_number)
    {
        list($response) = $this->supplierInvoiceCreateHeaderAttachmentByinvoiceNumberWithHttpInfo($invoice_number);
        return $response;
    }

    /**
     * Operation supplierInvoiceCreateHeaderAttachmentByinvoiceNumberWithHttpInfo
     *
     * Creates an attachment and associates it with a supplier invoice. If the file already exists, a new revision is created.
     *
     * @param string $invoice_number Identifies the supplier invoice (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceCreateHeaderAttachmentByinvoiceNumberWithHttpInfo($invoice_number)
    {
        // verify the required parameter 'invoice_number' is set
        if ($invoice_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $invoice_number when calling supplierInvoiceCreateHeaderAttachmentByinvoiceNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice/{invoiceNumber}/attachment";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($invoice_number !== null) {
            $resourcePath = str_replace(
                "{" . "invoiceNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($invoice_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/supplierInvoice/{invoiceNumber}/attachment'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber
     *
     * Creates an attachment and associates it with a certain supplier invoice line. If the file already exists, a new revision is created.
     *
     * @param string $invoice_number Identifies the supplier invoice (required)
     * @param int $line_number Specifies line number (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber($invoice_number, $line_number)
    {
        list($response) = $this->supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumberWithHttpInfo($invoice_number, $line_number);
        return $response;
    }

    /**
     * Operation supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumberWithHttpInfo
     *
     * Creates an attachment and associates it with a certain supplier invoice line. If the file already exists, a new revision is created.
     *
     * @param string $invoice_number Identifies the supplier invoice (required)
     * @param int $line_number Specifies line number (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumberWithHttpInfo($invoice_number, $line_number)
    {
        // verify the required parameter 'invoice_number' is set
        if ($invoice_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $invoice_number when calling supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber');
        }
        // verify the required parameter 'line_number' is set
        if ($line_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $line_number when calling supplierInvoiceCreateLineAttachmentByinvoiceNumberlineNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice/{invoiceNumber}/{lineNumber}/attachment";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($invoice_number !== null) {
            $resourcePath = str_replace(
                "{" . "invoiceNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($invoice_number),
                $resourcePath
            );
        }
        // path params
        if ($line_number !== null) {
            $resourcePath = str_replace(
                "{" . "lineNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($line_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/supplierInvoice/{invoiceNumber}/{lineNumber}/attachment'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation supplierInvoiceGetAllInvoices
     *
     * Get a range of Invoices
     *
     * @param string $document_type  (optional)
     * @param string $greater_than_value  (optional)
     * @param int $released  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @param int $dunning_level  (optional)
     * @param string $closed_financial_period  (optional)
     * @param string $dunning_letter_date_time  (optional)
     * @param string $dunning_letter_date_time_condition  (optional)
     * @return \Trollweb\VismaNetApi\Model\SupplierInvoiceDto[]
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceGetAllInvoices($document_type = null, $greater_than_value = null, $released = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null, $dunning_level = null, $closed_financial_period = null, $dunning_letter_date_time = null, $dunning_letter_date_time_condition = null)
    {
        list($response) = $this->supplierInvoiceGetAllInvoicesWithHttpInfo($document_type, $greater_than_value, $released, $number_to_read, $skip_records, $order_by, $last_modified_date_time, $last_modified_date_time_condition, $dunning_level, $closed_financial_period, $dunning_letter_date_time, $dunning_letter_date_time_condition);
        return $response;
    }

    /**
     * Operation supplierInvoiceGetAllInvoicesWithHttpInfo
     *
     * Get a range of Invoices
     *
     * @param string $document_type  (optional)
     * @param string $greater_than_value  (optional)
     * @param int $released  (optional)
     * @param int $number_to_read  (optional)
     * @param int $skip_records  (optional)
     * @param string $order_by  (optional)
     * @param string $last_modified_date_time  (optional)
     * @param string $last_modified_date_time_condition  (optional)
     * @param int $dunning_level  (optional)
     * @param string $closed_financial_period  (optional)
     * @param string $dunning_letter_date_time  (optional)
     * @param string $dunning_letter_date_time_condition  (optional)
     * @return Array of \Trollweb\VismaNetApi\Model\SupplierInvoiceDto[], HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceGetAllInvoicesWithHttpInfo($document_type = null, $greater_than_value = null, $released = null, $number_to_read = null, $skip_records = null, $order_by = null, $last_modified_date_time = null, $last_modified_date_time_condition = null, $dunning_level = null, $closed_financial_period = null, $dunning_letter_date_time = null, $dunning_letter_date_time_condition = null)
    {
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // query params
        if ($document_type !== null) {
            $queryParams['documentType'] = $this->apiClient->getSerializer()->toQueryValue($document_type);
        }
        // query params
        if ($greater_than_value !== null) {
            $queryParams['greaterThanValue'] = $this->apiClient->getSerializer()->toQueryValue($greater_than_value);
        }
        // query params
        if ($released !== null) {
            $queryParams['released'] = $this->apiClient->getSerializer()->toQueryValue($released);
        }
        // query params
        if ($number_to_read !== null) {
            $queryParams['numberToRead'] = $this->apiClient->getSerializer()->toQueryValue($number_to_read);
        }
        // query params
        if ($skip_records !== null) {
            $queryParams['skipRecords'] = $this->apiClient->getSerializer()->toQueryValue($skip_records);
        }
        // query params
        if ($order_by !== null) {
            $queryParams['orderBy'] = $this->apiClient->getSerializer()->toQueryValue($order_by);
        }
        // query params
        if ($last_modified_date_time !== null) {
            $queryParams['lastModifiedDateTime'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time);
        }
        // query params
        if ($last_modified_date_time_condition !== null) {
            $queryParams['lastModifiedDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($last_modified_date_time_condition);
        }
        // query params
        if ($dunning_level !== null) {
            $queryParams['dunningLevel'] = $this->apiClient->getSerializer()->toQueryValue($dunning_level);
        }
        // query params
        if ($closed_financial_period !== null) {
            $queryParams['closedFinancialPeriod'] = $this->apiClient->getSerializer()->toQueryValue($closed_financial_period);
        }
        // query params
        if ($dunning_letter_date_time !== null) {
            $queryParams['dunningLetterDateTime'] = $this->apiClient->getSerializer()->toQueryValue($dunning_letter_date_time);
        }
        // query params
        if ($dunning_letter_date_time_condition !== null) {
            $queryParams['dunningLetterDateTimeCondition'] = $this->apiClient->getSerializer()->toQueryValue($dunning_letter_date_time_condition);
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\SupplierInvoiceDto[]',
                '/controller/api/v1/supplierInvoice'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\SupplierInvoiceDto[]', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\SupplierInvoiceDto[]', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation supplierInvoiceGetByinvoiceNumber
     *
     * Get a specific Invoice
     *
     * @param string $invoice_number Identifies the Invoice (required)
     * @return \Trollweb\VismaNetApi\Model\SupplierInvoiceDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceGetByinvoiceNumber($invoice_number)
    {
        list($response) = $this->supplierInvoiceGetByinvoiceNumberWithHttpInfo($invoice_number);
        return $response;
    }

    /**
     * Operation supplierInvoiceGetByinvoiceNumberWithHttpInfo
     *
     * Get a specific Invoice
     *
     * @param string $invoice_number Identifies the Invoice (required)
     * @return Array of \Trollweb\VismaNetApi\Model\SupplierInvoiceDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceGetByinvoiceNumberWithHttpInfo($invoice_number)
    {
        // verify the required parameter 'invoice_number' is set
        if ($invoice_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $invoice_number when calling supplierInvoiceGetByinvoiceNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice/{invoiceNumber}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($invoice_number !== null) {
            $resourcePath = str_replace(
                "{" . "invoiceNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($invoice_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'GET',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\SupplierInvoiceDto',
                '/controller/api/v1/supplierInvoice/{invoiceNumber}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\SupplierInvoiceDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\SupplierInvoiceDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation supplierInvoicePost
     *
     * Create an SupplierInvoice
     *
     * @param \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto $supplier_invoice Defines the data for the SupplierInvoice to create (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoicePost($supplier_invoice)
    {
        list($response) = $this->supplierInvoicePostWithHttpInfo($supplier_invoice);
        return $response;
    }

    /**
     * Operation supplierInvoicePostWithHttpInfo
     *
     * Create an SupplierInvoice
     *
     * @param \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto $supplier_invoice Defines the data for the SupplierInvoice to create (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoicePostWithHttpInfo($supplier_invoice)
    {
        // verify the required parameter 'supplier_invoice' is set
        if ($supplier_invoice === null) {
            throw new \InvalidArgumentException('Missing the required parameter $supplier_invoice when calling supplierInvoicePost');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($supplier_invoice)) {
            $_tempBody = $supplier_invoice;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/supplierInvoice'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation supplierInvoicePutBysupplierInvoiceNumber
     *
     * Update a specific SupplierInvoice
     *
     * @param string $supplier_invoice_number Identifies the SupplierInvoice to update (required)
     * @param \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto $supplier_invoice Defines the data for the Invoice to update (required)
     * @return \Trollweb\VismaNetApi\Model\Object
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoicePutBysupplierInvoiceNumber($supplier_invoice_number, $supplier_invoice)
    {
        list($response) = $this->supplierInvoicePutBysupplierInvoiceNumberWithHttpInfo($supplier_invoice_number, $supplier_invoice);
        return $response;
    }

    /**
     * Operation supplierInvoicePutBysupplierInvoiceNumberWithHttpInfo
     *
     * Update a specific SupplierInvoice
     *
     * @param string $supplier_invoice_number Identifies the SupplierInvoice to update (required)
     * @param \Trollweb\VismaNetApi\Model\SupplierInvoiceUpdateDto $supplier_invoice Defines the data for the Invoice to update (required)
     * @return Array of \Trollweb\VismaNetApi\Model\Object, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoicePutBysupplierInvoiceNumberWithHttpInfo($supplier_invoice_number, $supplier_invoice)
    {
        // verify the required parameter 'supplier_invoice_number' is set
        if ($supplier_invoice_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $supplier_invoice_number when calling supplierInvoicePutBysupplierInvoiceNumber');
        }
        // verify the required parameter 'supplier_invoice' is set
        if ($supplier_invoice === null) {
            throw new \InvalidArgumentException('Missing the required parameter $supplier_invoice when calling supplierInvoicePutBysupplierInvoiceNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice/{supplierInvoiceNumber}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json','text/json','application/xml','text/xml','application/x-www-form-urlencoded'));

        // path params
        if ($supplier_invoice_number !== null) {
            $resourcePath = str_replace(
                "{" . "supplierInvoiceNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($supplier_invoice_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($supplier_invoice)) {
            $_tempBody = $supplier_invoice;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PUT',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\Object',
                '/controller/api/v1/supplierInvoice/{supplierInvoiceNumber}'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\Object', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 204:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\Object', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation supplierInvoiceReleaseInvoiceByinvoiceNumber
     *
     * Release invoice operation
     *
     * @param string $invoice_number Reference number of the released invoice to be reversed (required)
     * @return \Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceReleaseInvoiceByinvoiceNumber($invoice_number)
    {
        list($response) = $this->supplierInvoiceReleaseInvoiceByinvoiceNumberWithHttpInfo($invoice_number);
        return $response;
    }

    /**
     * Operation supplierInvoiceReleaseInvoiceByinvoiceNumberWithHttpInfo
     *
     * Release invoice operation
     *
     * @param string $invoice_number Reference number of the released invoice to be reversed (required)
     * @return Array of \Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto, HTTP status code, HTTP response headers (array of strings)
     * @throws \Trollweb\VismaNetApi\ApiException on non-2xx response
     */
    public function supplierInvoiceReleaseInvoiceByinvoiceNumberWithHttpInfo($invoice_number)
    {
        // verify the required parameter 'invoice_number' is set
        if ($invoice_number === null) {
            throw new \InvalidArgumentException('Missing the required parameter $invoice_number when calling supplierInvoiceReleaseInvoiceByinvoiceNumber');
        }
        // parse inputs
        $resourcePath = "/controller/api/v1/supplierInvoice/{invoiceNumber}/action/release";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json', 'text/json', 'application/xml', 'text/xml'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array());

        // path params
        if ($invoice_number !== null) {
            $resourcePath = str_replace(
                "{" . "invoiceNumber" . "}",
                $this->apiClient->getSerializer()->toPathValue($invoice_number),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto',
                '/controller/api/v1/supplierInvoice/{invoiceNumber}/action/release'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 200:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Trollweb\VismaNetApi\Model\ReleaseSupplierInvoiceActionResultDto', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
